var selectedNode;
// Set the following to "rsc/" string for local testing
var URI_local = "rsc/"
var URI_remote = "https://sanguinariojoe.gitlab.io/aquagpusph-simeditor/rsc/"
var URI = URI_local;
var LOADED_REPOS = [];
var REPO_DATA = undefined;
var PENDING = undefined;

/* Create a tiny extension to handle gitlab style inline equations:
 * 
 * $`V_i = m_i / \rho_i`$
 * 
 * and
 * 
 * ```latex
 * p_i = c_s^2 (\rho_i - \rho_0),
 * ```
 */
showdown.extension(`inline_formula`, {
    type: `lang`,
    filter: function (text) {
        // Look for a formula init
        var html = text;
        while (html.search("¨D`") !== -1) {
            var start = html.search("¨D`");
            var substr = html.slice(start, html.length);
            var end = substr.search("`¨D");
            if (end === -1) {
                console.log(`Unfinished inline equation, ${substr}`);
                end = substr.length - 3;
            }
            end += start + 3;

            // Render the equation
            var new_html = "";
            var elem = document.createElement("span");
            katex.render(html.slice(start + 3, end - 3).replace(/\n/g, " "),
                         elem,
                         {throwOnError: false});
            if (start > 0) {
                new_html = html.slice(0, start);
            }
            new_html = new_html + $(elem).html();
            if (end < html.length) {
                new_html = new_html + html.slice(end, html.length);
            }

            html = new_html;
        }

        while (html.search("```latex") !== -1) {
            var start = html.search("```latex");
            var substr = html.slice(start + 8, html.length);
            var end = substr.search("```");
            if (end === -1) {
                console.log(`Unfinished inline equation, ${substr}`);
                end = substr.length - 3;
            }
            end += start + 8 + 3;

            // Render the equation
            var new_html = "";
            var elem = document.createElement("span");
            katex.render(html.slice(start + 8, end - 3).replace(/\n/g, " "),
                         elem,
                         {throwOnError: false, displayMode: true});
            if (start > 0) {
                new_html = html.slice(0, start);
            }
            new_html = new_html + $(elem).html();
            if (end < html.length) {
                new_html = new_html + html.slice(end, html.length);
            }

            html = new_html;
        }

        return html;
    }
});


var md_parser = new showdown.Converter({
    extensions: [
        'inline_formula',
    ],
});

function onNodeSelected(event, node) {
    if ("md_desc" in node) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", URI + node.md_desc); 
        xhr.responseType = 'text';
        xhr.onload = function()  {
            if (xhr.status != 200) {
                console.log(`Error downloading ${URI + node.md_desc}`);
                console.log(`Error ${xhr.status}: ${xhr.statusText}`);
            }
            txt = xhr.responseText;
            html = md_parser.makeHtml(txt);
            $('#repoloader_desc').html(html);
        }
        xhr.onerror = function(event) {
            if (event.lengthComputable) {
                console.log(`Error downloading ${URI + node.md_desc}`);
                console.log(`Received ${event.loaded} of ${event.total} bytes`);
            } else {
                console.log(`Error downloading ${URI + node.md_desc}`);
                console.log(`Received ${event.loaded} bytes`); // no Content-Length
            }
        }
        xhr.send();
    } else if ("desc" in node) {
        $('#repoloader_desc').html(node.desc);
        // Render the equations
        $(".math", $('#repoloader_desc')).each(function(i, obj) {
            katex.render(obj.text(), obj.get());
        });
    }
    selectedNode = node;
    if ("path" in node) {
        $("#repoloader_loadbutton").prop("disabled", false);
    } else {
        $("#repoloader_loadbutton").prop("disabled", true);
    }
}

function onNodeExpanded(event, node) {
    $('#tree').treeview('selectNode', [node.nodeId, { silent: false}]);
}

function isRepoLoaded(path) {
    for (var i=0; i<LOADED_REPOS.length; i++) {
        if (path === LOADED_REPOS[i]) {
            return true;
        }
    }
    return false;
}

function getRepoData(name, subrepo=REPO_DATA) {
    if (typeof subrepo === undefined) {
        return undefined;
    }

    // Check if we already reached the queried node
    if ((typeof subrepo.text !== undefined) && (subrepo.text === name)) {
        return subrepo;
    }

    if (typeof subrepo.nodes === undefined) {
        return undefined;
    }
    var names = name.split("/");
    for(var i=0; i<subrepo.nodes.length; i++) {
        if (subrepo.nodes[i].text === names[0]) {
            if (names.length === 1) {
                // This is the node we are looking for
                return subrepo.nodes[i];
            }
            // Recursively go deeper
            var subname = name.slice(names[0].length + 1);
            return getRepoData(subname, subrepo.nodes[i]);
        }
    }

    // Not found
    return undefined;
}

function getRepoDepends(data, depends) {
    depends = typeof depends !== 'undefined' ? depends : [];

    if (data.depends === undefined) {
        return depends;
    }

    for (var i=0; i<data.depends.length; i++) {
        var subdata = getRepoData(data.depends[i]);
        if (subdata === undefined) {
            // We cannot find the dependency!?
            console.log(`Failure finding the dependency "${data.depends[i]}"`);
        }
        // Add the subdependencies
        depends = getRepoDepends(subdata, depends);
        // Add the dependency, if it was not added yet
        if ($.inArray(data.depends[i], depends) === -1) {
            depends.push(data.depends[i]);
        }
    }

    return depends;
}

function getRepoName(node) {
    var name = node.text;
    if (typeof node.parentId === undefined) {
        return name;
    }
    while (true) {
        node = $('#tree').treeview('getParent', node);
        name = node.text + '/' + name;
        if (typeof node.parentId === "undefined") {
            break;
        }
    }
    return name;
}

function onLoadRepo() {
    if ( (selectedNode === undefined) || (!("path" in selectedNode)) ) {
        return;
    }

    // Check the dependencies
    PENDING = [];
    if (typeof selectedNode.depends !== "undefined") {
        var depends = getRepoDepends(selectedNode);
        for (var i=0; i<depends.length; i++) {
            if (!isRepoLoaded(depends[i])) {
                PENDING.push(depends[i]);
            }
        }
    }

    $('#repoloader').hide();
    function loadAll() {
        if (typeof PENDING === "undefined") {
            // Special case when we already finished loading
            return;
        }

        if (PENDING.length > 1) {
            // We are still loading dependencies
            var pending = PENDING[0];
            LOADED_REPOS.push(pending);
            var path = getRepoData(pending).path;
            PENDING = PENDING.slice(1);
            if (typeof path === "undefined") {
                // The dependency has not a path!?
                console.log(`${pending} dependency has not a path to load`);
                loadAll();
                return;
            }
        } else {
            // Finally we have the selected node
            var path = PENDING[0].path;
            LOADED_REPOS.push(getRepoName(PENDING[0]));
            PENDING = undefined;
        }

        var xhr = new XMLHttpRequest();
        xhr.open("GET", URI + path); 
        xhr.responseType = "blob";
        xhr.onload = function()  {
            if (xhr.status != 200) {
                console.log(`Error downloading ${URI + path}`);
                console.log(`Error ${xhr.status}: ${xhr.statusText}`);
            }
            if (!(name in xhr.response)) {
                xhr.response.name = path;
            }
            loadFile(xhr.response, loadAll);
        }
        xhr.onerror = function(event) {
            if (event.lengthComputable) {
                console.log(`Error downloading ${URI + path}`);
                console.log(`Received ${event.loaded} of ${event.total} bytes`);
            } else {
                console.log(`Error downloading ${URI + path}`);
                console.log(`Received ${event.loaded} bytes`); // no Content-Length
            }
            // must we continue anyway?
            loadAll();
        }
        xhr.send();
    }

    if (PENDING.length > 0) {
        $('#repoloader_depends').show();
        // Prepare the new window
        $('#repoloader_depends_list').empty();
        var items = [];
        for (var i=0; i<PENDING.length; i++) {
            items.push('<li>' + PENDING[i] + '</li>');
        }
        $('#repoloader_depends_list').html(items.join(''));

        $('#repoloader_depends_load').unbind('click');
        $('#repoloader_depends_load').click(function() {
            $('#repoloader_depends').hide();
            PENDING = [selectedNode];
            loadAll();
        });

        $('#repoloader_depends_loadall').unbind('click');
        $('#repoloader_depends_loadall').click(function() {
            $('#repoloader_depends').hide();
            PENDING.push(selectedNode);
            loadAll();
        });

        $('#repoloader_depends_cancel').unbind('click');
        $('#repoloader_depends_cancel').click(function() {
            $('#repoloader_depends').hide();
            $('#repoloader').show();
        });

        return;
    }

    PENDING = [selectedNode];
    loadAll();
}

function showRepoLoader() {
    $('#repoloader').show();

    if (selectedNode !== undefined) {
        $('#tree').treeview('selectNode', [selectedNode.nodeId, { silent: false}]);
    } else {
        $("#repoloader_loadbutton").prop("disabled", true);
    }
}

function onCancelRepo() {
    $('#repoloader').hide();
}

// Try to parse the repository JSON file, first locally, then remotely.
//
// In local environment this would probably fail anyway due to CORS policy. In
// Firefox it can be fixed tweaking security.fileuri.strict_origin_policy in
// about:config
function parse_repojson(data) {
    REPO_DATA = {"nodes":data};
    $('#tree').treeview({
        data: data,
        levels: 0,
        backColor: 'black',
        onhoverColor: '#444444',
        color: 'white',
        checkedIcon: 'fa fa-check-square-o',
        collapseIcon: 'fa fa-minus-square-o',
        expandIcon: 'fa fa-plus-square-o',
        onNodeSelected: onNodeSelected,
        onNodeExpanded: onNodeExpanded,
    });
    $('#tree').on('nodeSelected', onNodeSelected);
    $('#tree').on('nodeExpanded', onNodeExpanded);
}

$.getJSON(URI_local + "repo.json", parse_repojson).fail(function() {
    console.log(`error downloading ${URI_local + "repo.json"}`);
    URI = URI_remote;
    $.getJSON(URI_remote + "repo.json", parse_repojson).fail(function() {
        console.log(`error downloading ${URI_remote + "repo.json"}`);
    });
});
