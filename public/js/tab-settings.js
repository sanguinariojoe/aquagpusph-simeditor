var SETTINGS = {
    Device:{
        type:"ALL",
        platform:0,
        device:0
    },
    RootPath:"/usr/share/aquagpusph"
};

var TIMING = {
    Options:{
        End:{
            Frames:{value:0, enabled:false},
            Steps:{value:0, enabled:false},
            Time:{value:0.0, enabled:false}
        },
        Output:{
            FPS:{value:0.0, enabled:false},
            IPF:{value:0, enabled:false}
        }
    },
};

function setDevice(platform, device, type) {
    $("#DeviceTypeSel").val(type);
    $("#DeviceTypeSel").trigger("change");
    $("#DevicePlatform").text(platform);
    $("#DevicePlatform").trigger("blur");
    $("#DeviceDevice").text(device);
    $("#DeviceDevice").trigger("blur");
}

function setRootPath(path) {
    $("#RootPath").text(path);
    $("#RootPath").trigger("blur");
}

function setTimingOption(name, type, value, enabled) {
    if ((TIMING.Options[name] === undefined) ||
        (TIMING.Options[name][type] === undefined)) {
        return;
    }
    var name = type + name + "Option";
    $("#Has" + name).prop('checked', enabled);
    $("#Has" + name).trigger("change");
    $("#" + name).text(value);
    $("#" + name).trigger("blur");
}


function initializeSettings() {
    var span = $('#DeviceTypeSel');
    span.on('change', function() {
        SETTINGS.Device.type = $(this).val();
    });
    span.trigger("change");

    var int_spans = ['#DevicePlatform', '#DeviceDevice'];
    for (var i=0; i<int_spans.length; i++) {
        var span = $(int_spans[i]);
        span.blur(function() {
            var val = parseInt($(this).text());
            if (isNaN(val)) {
                val = 0;
            }
            $(this).text(val);
            SETTINGS.Device[$(this).data('attribute')] = val;
        });
        span.trigger("blur");
    }

    var span = $('#RootPath');
    span.blur(function() {
        SETTINGS.RootPath = $(this).text();
    });
    span.trigger("blur");

    var check_spans = ['#HasFramesEndOption', '#HasStepsEndOption',
                       '#HasTimeEndOption', '#HasFPSOutputOption',
                       '#HasIPFOutputOption'];
    for (var i=0; i<check_spans.length; i++) {
        var span = $(check_spans[i]);
        span.change(function() {
            var checked = $(this).prop('checked');
            var name = $(this).data('name');
            var type = $(this).data('type');
            TIMING.Options[name][type].enabled = checked;
        });
        span.prop('checked', false);
        span.trigger("change");
    }

    var int_spans = ['#FramesEndOption', '#StepsEndOption', '#IPFOutputOption'];
    for (var i=0; i<int_spans.length; i++) {
        var span = $(int_spans[i]);
        span.blur(function() {
            var val = parseInt($(this).text());
            if (isNaN(val)) {
                val = 0;
            }
            $(this).text(val);
            var name = $(this).data('name');
            var type = $(this).data('type');
            TIMING.Options[name][type].value = val;
        });
        span.trigger("blur");
    }

    float_spans = ['#TimeEndOption', '#FPSOutputOption'];
    for (var i=0; i<float_spans.length; i++) {
        var span = $(float_spans[i]);
        span.blur(function() {
            var val = parseFloat($(this).text());
            if (isNaN(val)) {
                val = 0.0;
            }
            $(this).text(val);
            var name = $(this).data('name');
            var type = $(this).data('type');
            TIMING.Options[name][type].value = val;
        });
        span.trigger("blur");
    }
}

function updateSettings() {
}

// Register the table errors check as loading callback
registerTabCallback('Settings', updateSettings);
