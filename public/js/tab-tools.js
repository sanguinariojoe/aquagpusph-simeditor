var TOOLS = [];
var __ACTIVE_TOOL = undefined;

/** Get a tool by its name
 * @param name Tool name
 * @return The tool, undefined if such tool cannot be found
 */
function getTool(name) {
    for (var i=0; i<TOOLS.length; i++) {
        if (TOOLS[i].name === name) {
            return TOOLS[i];
        }
    }
    return undefined;
}

/** Check if the point is inside the object
 * @param x X coordinate of the point
 * @param y Y coordinate of the point
 * @param obj Object to be checked
 * @return true if the point is inside the object bounding box, false
 * otherwise.
 */
function isPointInObject(x, y, obj) {
    var bbox = obj.bbox();
    x -= obj.parent().transform().x;
    y -= obj.parent().transform().y;

    return ((bbox.x <= x) && (bbox.x2 >= x) && (bbox.y <= y) && (bbox.y2 >= y));
}

/** Get all the tools by a point
 * @param x X coordinate of the point
 * @param y Y coordinate of the point
 * @return The list of tools
 * @see isPointInObject()
 */
function getToolsByPoint(x, y) {
    var tools = [];
    for (var i=0; i<TOOLS.length; i++) {
        if (!TOOLS[i].visible()) {
            continue;
        }
        if (isPointInObject(x, y, TOOLS[i].svg)) {
            tools.push(TOOLS[i]);
        }
    }
    return tools;    
}

/** Get the tool that can be connected by a port in a specific point,
 * in case such tool exist
 * @param x X coordinate of the point
 * @param y Y coordinate of the point
 * @param filter List of types of ports to be considered. valid values in
 * the list are 'orig' for the origin port, 'in' for the input ports and
 * 'out' for the output ports. If undefined, ['orig', 'in', 'out'] will
 * be considered.
 * @return The connectable tool, or undefined if no valid tool is found. In
 * case that the origin is found as connectable, '0' will be returned
 */
function getConnectedTool(x, y, filter) {
    if (filter === undefined) {
        filter = ['orig', 'in', 'out'];
    }
    if (filter.indexOf('orig') !== -1) {
        if (isPointInObject(x, y, origPort)) {
            return '0'
        }
    }
    for (var i=0; i<TOOLS.length; i++) {
        if (!TOOLS[i].visible()) {
            continue;
        }
        if (filter.indexOf('in') !== -1) {
            if (isPointInObject(x, y, TOOLS[i].inPort)) {
                return TOOLS[i];
            }
        }
        if (filter.indexOf('out') !== -1) {
            if (isPointInObject(x, y, TOOLS[i].outPort)) {
                return TOOLS[i];
            }
        }
    }
    return undefined;
}

/** Get the list of selected tools
 * @return The list of selected tools
 */
function getSelectedTools() {
    var tools = [];
    for (var i=0; i<TOOLS.length; i++) {
        if (TOOLS[i].selected) {
            tools.push(TOOLS[i]);
        }
    }
    return tools;    
}

// Extension to can transport info regarding the tool that owns the port.
// Useful during the events handling, due to the namespace
SVG.extend(SVG.Shape, SVG.G, {
    owner: function(obj) {
        if (obj === undefined) {
            return this._owner;
        }
        this._owner = obj;
        return this;
    }
})

class Tool {
    constructor(name) {
        this._selected = false;
        this._type = "dummy";
        this._visible = true;
        this._once = false;
        this._ingroup = undefined;
        this.data = {
            "kernel":{
                "path":"kernel.cl",
                "entry_point":"entry",
                "enclosed_file":true,
            },
            "copy":{
                "in":undefined,
                "out":undefined,
            },
            "set":{
                "in":undefined,
                "value":"0",
            },
            "python":{
                "path":"script.py",
                "enclosed_file":true,
            },
            "set_scalar":{
                "in":undefined,
                "value":"0",
            },
            "reduction":{
                "in":undefined,
                "out":undefined,
                "null":"0",
                "text":"c = a + b",
            },
            "link-list":{
                "in":"r",
            },
            "radix-sort":{
                "in":undefined,
                "perm":undefined,
                "inv_perm":undefined,
            },
            "assert":{
                "condition":"true",
            },
            "if":{
                "condition":"true",
            },
            "while":{
                "condition":"true",
            },
            "end":{},
            "mpi-sync":{
                "mask":undefined,
                "fields":undefined,
                "processes":"",
            },
            "dummy":{},
            "report_screen":{
                "fields":undefined,
                "color":"white",
                "bold":"false",
            },
            "report_file":{
                "fields":undefined,
                "path":"report.out",
            },
            "report_particles":{
                "set":"0",
                "fields":undefined,
                "path":"particles.dat",
                "fps":"0.0",
                "ipf":"1",
            },
            "report_performance":{
                "color":"white",
                "bold":"false",
                "path":"",
            },
        };

        if(name === undefined) {
            var i = 0;
            while (getTool("Tool" + i.toString()) !== undefined) {
                i += 1;
            }
            name = "Tool" + i.toString();
        }
        // Build the empty object
        var canvas = getCanvas();
        var txt = canvas.text(name).build(false);
        txt.owner(this);
        var editBut = canvas.image("imgs/edit_black.svg");
        editBut.owner(this);
        var delBut = canvas.image("imgs/remove_black.svg");
        delBut.owner(this);
        var prevPort = canvas.circle(1).attr({fill: 'black',
                                              stroke: null});
        prevPort.owner(this);
        var nextPort = canvas.circle(1).attr({fill: 'black',
                                              stroke: null});
        nextPort.owner(this);
        var box = canvas.rect(10, 5).radius(1).attr({fill: 'white',
                                                     stroke: null});
        box.owner(this);
        var svg = canvas.group();
        svg.owner(this);
        svg.add(box);       // this._svg.get(0)
        svg.add(prevPort);  // this._svg.get(1)
        svg.add(nextPort);  // this._svg.get(2)
        svg.add(txt);       // this._svg.get(3)
        svg.add(editBut);   // this._svg.get(4)
        svg.add(delBut);    // this._svg.get(5)
        this._svg = svg;
        this._name = name;
        this.__updateSVG();

        // Declare the whole object as draggable
        svg.draggable();

        // List of connectors
        this.connections = {inPort:[], outPort:[]};
        // Now, to handle input and output ports, we are declaring them as
        // draggables as well. Then we are hacking the drag events to draw a
        // curve instead
        var __tmp_port;
        var __tmp_conn;
        prevPort.draggable();
        prevPort.off('dragstart.connect').on('dragstart.connect', function(e){
            e.preventDefault();
            // create a dummy connected port that we are dragging, instead of
            // the actual port
            __tmp_port = canvas.circle(1).attr({fill: 'black',
                                                stroke: null});
            __tmp_port.center(e.detail.p.x, e.detail.p.y);
            // Connect the actual port to the dummy one
            __tmp_conn = this.connectable({
                targetAttach: 'center',
                sourceAttach: 'center',
                type: 'curved-fromin'
            }, __tmp_port);
        });
        prevPort.off('dragmove.connect').on('dragmove.connect', function(e){
            e.preventDefault();
            var x = e.detail.p.x, y = e.detail.p.y;
            var x0 = this.parent().transform().x;
            var y0 = this.parent().transform().y;
            var tool = getConnectedTool(x + x0, y + y0, ['orig', 'out']);
            if ((tool === undefined) || (tool === this.owner())) {
                __tmp_port.move(x + x0, y + y0);
                __tmp_conn.connector.attr('stroke-width', 1);
                __tmp_conn.connector.attr('stroke-opacity', 0.5);
                __tmp_conn.setConnectorColor('black');
            }
            else if (tool === '0') {
                __tmp_port.move(origPort.cx(), origPort.cy());
                __tmp_conn.connector.attr('stroke-width', 3);
                __tmp_conn.connector.attr('stroke-opacity', 0.5);
                if (this.owner().isConnectedFrom(origPort)) {
                    __tmp_conn.setConnectorColor('red');
                }
                else {
                    __tmp_conn.setConnectorColor('black');
                }
            }
            else {
                x = tool.outPort.parent().transform().x + tool.outPort.cx();
                y = tool.outPort.parent().transform().y + tool.outPort.cy();
                __tmp_port.move(x, y);
                __tmp_conn.connector.attr('stroke-width', 3);
                __tmp_conn.connector.attr('stroke-opacity', 0.5);
                if (this.owner().isConnectedFrom(tool.outPort)) {
                    __tmp_conn.setConnectorColor('red');
                }
                else {
                    __tmp_conn.setConnectorColor('black');
                    var paths = this.owner().connectionPaths(tool.inPort);
                    if (paths.length > 0) {
                        // Recursive connection!
                        __tmp_conn.connector.attr('stroke-width', 1);
                        __tmp_conn.setConnectorColor('orange');
                    }
                }
            }
        });
        prevPort.off('dragend.connect').on('dragend.connect', function(e){
            e.preventDefault();
            var x = e.detail.p.x, y = e.detail.p.y;
            var x0 = this.parent().transform().x;
            var y0 = this.parent().transform().y;
            var tool = getConnectedTool(x + x0, y + y0, ['orig', 'out']);
            // Clear temporal stuff
            __tmp_conn.update();
            __tmp_conn.connector.remove();
            __tmp_port.remove();
            // Add/Remove the link
            if (tool === '0') {
                if (this.owner().delConnectionFrom(origPort)) {
                    return;
                }
                var con = this.connectable({
                    targetAttach: 'center',
                    sourceAttach: 'center',
                    type: 'curved-fromin'
                }, origPort);
                con.connector.attr('stroke-width', 3);
                con.connector.attr('stroke-opacity', 0.5);
                origPort.connections.push(con);
                this.owner().connections.inPort.push(con);
            }
            else if ((tool !== undefined) && (tool !== this.owner())) {
                if (this.owner().delConnectionFrom(tool.outPort)) {
                    return;
                }
                var paths = this.owner().connectionPaths(tool.inPort);
                if (paths.length > 0) {
                    // Recursive connection!
                    return;
                }

                var con = this.connectable({
                    targetAttach: 'center',
                    sourceAttach: 'center',
                    type: 'curved-fromin'
                }, tool.outPort);
                con.connector.attr('stroke-width', 3);
                con.connector.attr('stroke-opacity', 0.5);
                this.owner().connections.inPort.push(con);
                tool.outPort.owner().connections.outPort.push(con);
            }
        });
        nextPort.draggable();
        nextPort.off('dragstart.connect').on('dragstart.connect', function(e){
            e.preventDefault();
            // create a dummy connected port that we are dragging, instead of
            // the actual port
            __tmp_port = canvas.circle(1).attr({fill: 'black',
                                                stroke: null});
            __tmp_port.center(e.detail.p.x, e.detail.p.y);
            // Connect the actual port to the dummy one
            __tmp_conn = __tmp_port.connectable({
                targetAttach: 'center',
                sourceAttach: 'center',
                type: 'curved-fromin'
            }, this);
        });
        nextPort.off('dragmove.connect').on('dragmove.connect', function(e){
            e.preventDefault();
            var x = e.detail.p.x, y = e.detail.p.y;
            var x0 = this.parent().transform().x;
            var y0 = this.parent().transform().y;
            var tool = getConnectedTool(x + x0, y + y0, ['in']);
            if ((tool === undefined) || (tool === this.owner())) {
                __tmp_port.move(x + x0, y + y0);
                __tmp_conn.connector.attr('stroke-width', 1);
                __tmp_conn.connector.attr('stroke-opacity', 0.5);
                __tmp_conn.setConnectorColor('black');
            }
            else {
                x = tool.inPort.parent().transform().x + tool.inPort.cx();
                y = tool.inPort.parent().transform().y + tool.inPort.cy();
                __tmp_port.move(x, y);
                __tmp_conn.connector.attr('stroke-width', 3);
                __tmp_conn.connector.attr('stroke-opacity', 0.5);
                if (this.owner().isConnectedTo(tool.inPort)) {
                    __tmp_conn.setConnectorColor('red');
                }
                else {
                    __tmp_conn.setConnectorColor('black');
                    var paths = tool.connectionPaths(this.owner().inPort);
                    if (paths.length > 0) {
                        // Recursive connection!
                        __tmp_conn.connector.attr('stroke-width', 1);
                        __tmp_conn.setConnectorColor('orange');
                    }
                }
            }
        });
        nextPort.off('dragend.connect').on('dragend.connect', function(e){
            e.preventDefault();
            var x = e.detail.p.x, y = e.detail.p.y;
            var x0 = this.parent().transform().x;
            var y0 = this.parent().transform().y;
            var tool = getConnectedTool(x + x0, y + y0, ['in']);
            // Clear temporal stuff
            __tmp_conn.connector.remove();
            __tmp_port.remove();
            // Add/Remove the link
            if ((tool !== undefined) && (tool !== this.owner())) {
                if (this.owner().delConnectionTo(tool.inPort)) {
                    return;
                }
                var paths = tool.connectionPaths(this.owner().inPort);
                if (paths.length > 0) {
                    // Recursive connection!
                    return;
                }

                var con = tool.inPort.connectable({
                    targetAttach: 'center',
                    sourceAttach: 'center',
                    type: 'curved-fromin'
                }, this);
                con.connector.attr('stroke-width', 3);
                con.connector.attr('stroke-opacity', 0.5);
                tool.inPort.owner().connections.inPort.push(con);
                this.owner().connections.outPort.push(con);
            }
        });
        // We must track the whole object drag events to redirect it to the
        // already existing connections.
        svg.off('dragmove.connect').on('dragmove.connect', function(e){
            var keys = ["inPort", "outPort"];
            for (var k=0; k<keys.length; k++) {
                var conns = this.owner().connections[keys[k]];
                for (var i=0; i<conns.length; i++) {
                    conns[i].update();
                }
            }
        });

        // Buttons hightlight
        function highlight_filter(add) {
            add.componentTransfer({
                rgb: { type: 'table', tableValues: [1, 0] }
            }).colorMatrix('matrix', [ 1, 0, 0, 0, 0
                                     , 0, 0, 0, 0, 0
                                     , 0, 0, 0, 0, 0
                                     , 0, 0, 0, 1, 0 ]);
        }
        editBut.off('mouseenter.tabtools').on('mouseenter.tabtools',
            function() {this.filter(highlight_filter)});
        editBut.off('mouseleave.tabtools').on('mouseleave.tabtools',
            function() {this.unfilter(true)});
        delBut.off('mouseenter.tabtools').on('mouseenter.tabtools',
            function() {this.filter(highlight_filter)});
        delBut.off('mouseleave.tabtools').on('mouseleave.tabtools',
            function() {this.unfilter(true)});

        // Select tool
        var selMouseDown = function(e) {
            if(e.shiftKey) {
                // Add to the selection all tools in valid paths to the active
                // tool
                if (__ACTIVE_TOOL !== undefined) {
                    var oPaths = __ACTIVE_TOOL.connectionPaths(
                        this.owner().inPort);
                    var iPaths = this.owner().connectionPaths(
                        __ACTIVE_TOOL.inPort);
                    var paths = oPaths.concat(iPaths);
                    for (var i=0; i<paths.length; i++) {
                        for (var j=0; j<paths[i].length; j++) {
                            paths[i][j].selected = true;
                        }
                    }
                }
                this.owner().selected = true;
            }
            else if(e.ctrlKey) {
                // Just add the element to the selection
                this.owner().selected = true;
            }
            else {
                // unselect all
                for (var i=0; i<TOOLS.length; i++) {
                    TOOLS[i].selected = false;
                }
                // And select the element
                this.owner().selected = true;
            }

            // Set the active tool
            if (__ACTIVE_TOOL !== undefined) {
                __ACTIVE_TOOL.selected = __ACTIVE_TOOL._selected;
            }
            __ACTIVE_TOOL = this.owner();
            this.owner().box.attr({stroke:'#FF0000'});
        }
        box.off('mousedown.select').on('mousedown.select', selMouseDown);
        txt.off('mousedown.select').on('mousedown.select', selMouseDown);
        
        // Edit tool
        editBut.off('mousedown.action').on('mousedown.action', function(e) {
            e.preventDefault();
            var tab = createToolTab(this.owner());
            tab.click();
        });

        // Remove tool
        delBut.off('mousedown.action').on('mousedown.action', function(e) {
            e.preventDefault();
            this.owner().remove();
        });
    }

    /** Tool SVG representation getter
     * @return SVG representation (A group of SVG elements)
     */
    get svg() {
        return this._svg;
    }
    /** Holding/background box SVG representation getter
     * @return Holding box SVG representation
     */
    get box() {
        return this._svg.get(0);
    }
    /** Input port SVG representation getter
     * @return Input port SVG representation
     */
    get inPort() {
        return this._svg.get(1);
    }
    /** Output port SVG representation getter
     * @return Output port SVG representation
     */
    get outPort() {
        return this._svg.get(2);
    }
    /** Tool name SVG representation getter
     * @return Tool name SVG representation
     */
    get txt() {
        return this._svg.get(3);
    }
    /** Tool edit button SVG representation getter
     * @return Tool edit button SVG representation
     */
    get editButton() {
        return this._svg.get(4);
    }
    /** Tool remove button SVG representation getter
     * @return Tool remove button SVG representation
     */
    get removeButton() {
        return this._svg.get(5);
    }

    /** Tool type getter
     * @return Tool type
     */
    get type() {
        return this._type;
    }

    /** Tool type setter
     * @param val New tool type
     */
    set type(val) {
        this._type = val;
        if (this._tab !== undefined) {
            this._tab.updateType();
        }
    }

    /** Tool name getter
     * @return Tool name
     */
    get name() {
        return this._name;
    }

    /** Tool name setter
     * @param val New tool name
     */
    set name(val) {
        if (val === "") {
            return;
        }
        // Rename the eventually existing tool with the same name
        var tool = getTool(val);
        if (tool !== undefined) {
            tool.name = val + '.bak';
        }
        this._name = val;
        this.txt.plain(val);
        this.__updateSVG();
    }

    /** Shall be the tool run just once?
     * @return true if the tool shall be run just once, false otherwise
     */
    get once() {
        return this._once;
    }

    /** Shall be the tool run just once?
     * @param val true if the tool shall be run just once, false otherwise
     */
    set once(val) {
        this._once = val;
    }

    /** Get the group this tool belongs to
     * @return The owner group, undefined if this tool is not grouped at all
     */
    get group() {
        return this._group;
    }

    /** @brief Set the group this tool belongs to
     *
     * This function is also hiding/showing the tool
     *
     * @param g The owner group, undefined to set this tool as ungrouped
     */
    set group(g) {
        this._group = g;
        if (g !== undefined) {
            this.hide();
        } else {
            this.show();
        }
    }

    /** Is the tool selected?
     * @return true if the tool is selected, false otherwise
     */
    get selected() {
        return this._selected;
    }

    /** Set the tool as selected/unselected
     * @param val true if the tool is selected, false otherwise
     */
    set selected(val) {
        this._selected = Boolean(val);
        if (this._selected) {
            this.box.attr({stroke:'#BB0000',
                           'stroke-width':2});
        }
        else {
            this.box.attr({stroke:null});
            if (__ACTIVE_TOOL === this) {
                __ACTIVE_TOOL = undefined;
            }
        }
        // Enable the group making button if possible
        if (canMakeGroup()) {
            $('#ToolsGroup').show();
        } else {
            $('#ToolsGroup').hide();            
        }
    }

    /** Is this tool connected to a specific visitor port?
     * @param port Visitor port
     * @return true if this tool is connected by the output port with the
     * visitor, false otherwise.
     */
    isConnectedTo(port) {
        for (var i=0; i<this.connections.outPort.length; i++) {
            var conn = this.connections.outPort[i];
            if (conn.source === port) {
                return true;
            }
        }
        return false;
    }

    /** Is this tool connected from a specific visitor port?
     * @param port Visitor port
     * @return true if this tool is connected by the input port with the
     * visitor, false otherwise.
     */
    isConnectedFrom(port) {
        for (var i=0; i<this.connections.inPort.length; i++) {
            var conn = this.connections.inPort[i];
            if (conn.target === port) {
                return true;
            }
        }
        return false;
    }

    /** Get the available paths from the output port to an specific input port,
     * through the already set connections.
     * @param port The remote input port
     * @return List of available paths. Each path is a list of the tools
     * involved.
     */
    connectionPaths(port) {
        var paths = [];
        for (var i=0; i<this.connections.outPort.length; i++) {
            var conn = this.connections.outPort[i];
            if (conn.source === port) {
                paths.push([this, port.owner()]);
                continue;
            }
            var subpaths = conn.source.owner().connectionPaths(port);
            for (var j=0; j<subpaths.length; j++) {
                paths.push([this].concat(subpaths[j]));
            }
        }
        return paths;
    }

    /** Remove a connection to a remote input port
     * @param port Visitor port
     * @return true if this tool was connected by the input port with the
     * visitor, false otherwise.
     */
    delConnectionTo(port) {
        for (var i=0; i<this.connections.outPort.length; i++) {
            var conn = this.connections.outPort[i];
            if (conn.source === port) {
                // Remove the reference on the visitor
                port.owner().connections.inPort.splice(
                    port.owner().connections.inPort.indexOf(conn), 1);
                // Remove the connector and the local reference
                conn.connector.remove();
                this.connections.outPort.splice(i, 1);
                return true;
            }
        }
        return false;
    }

    /** Remove a connection from a remote output port
     * @param port Visitor port
     * @return true if this tool was connected by the output port with the
     * visitor, false otherwise.
     */
    delConnectionFrom(port) {
        for (var i=0; i<this.connections.inPort.length; i++) {
            var conn = this.connections.inPort[i];
            if (conn.target === port) {
                // Remove the reference on the visitor
                if (port === origPort) {
                    origPort.connections.splice(
                        origPort.connections.indexOf(conn), 1);
                }
                else {
                    port.owner().connections.outPort.splice(
                        port.owner().connections.outPort.indexOf(conn), 1);
                }
                // Remove the connector and the local reference
                conn.connector.remove();
                this.connections.inPort.splice(i, 1);
                return true;
            }
        }
        return false;
    }

    /** Remove the whole tool
     */
    remove() {
        // Unselect the tool
        this.selected = false;
        // Remove all the connections
        for (var i=this.connections.inPort.length - 1; i>=0; i--) {
            var port = this.connections.inPort[i].target;
            this.delConnectionFrom(port);
        }
        for (var i=this.connections.outPort.length - 1; i>=0; i--) {
            var port = this.connections.outPort[i].source;
            this.delConnectionTo(port);
        }
        // Remove the element itself
        this.svg.remove();
        TOOLS.splice(TOOLS.indexOf(this), 1);
        closeToolTab(this);
    }

    __updateSVG() {
        // Get the resulting txt bbox
        var txt_bbox = this.txt.bbox();
        var innerHeight = txt_bbox.h;
        var r = txt_bbox.h / 2;
        // Compute the main dimensions
        var innerWidth = txt_bbox.w + 11 * r;
        var outerHeight = innerHeight + r;
        var outerWidth = innerWidth + r;
        // Set the radius of the ports
        this.inPort.radius(0.75 * r);
        this.outPort.radius(0.75 * r);
        // Set the box dimensions
        this.box.size(outerWidth, outerHeight);
        this.box.radius(outerHeight / 2);
        var cx = this.box.cx(), cy = this.box.cy();
        // Set the buttons size
        this.editButton.size(2 * r, 2 * r);
        this.removeButton.size(2 * r, 2 * r);
        // Place the elements
        this.inPort.center(cx - innerWidth / 2 + r, cy);
        this.outPort.center(cx + innerWidth / 2 - r, cy);
        this.txt.center(cx - 2.0 * r, cy);
        this.editButton.center(cx + txt_bbox.w / 2 - r, cy);
        this.removeButton.center(cx + txt_bbox.w / 2 + r, cy);
    }

    /** Hide the tool, and its connections
     */
    hide() {
        this._visible = false;
        // Unselect the tool
        this.selected = false;
        // Hide all the connections
        var keys = ["inPort", "outPort"];
        for (var k=0; k<keys.length; k++) {
            for (var i=0; i<this.connections[keys[k]].length; i++) {
                this.connections[keys[k]][i].connector.hide();
            }
        }
        // Hide the element itself
        this.svg.hide();
        // Eventually close the tab
        closeToolTab(this);
        return this;
    }

    /** Show the tool, and its connections (updating them)
     */
    show() {
        this._visible = true;
        // Show all the connections
        var keys = ["inPort", "outPort"];
        for (var k=0; k<keys.length; k++) {
            for (var i=0; i<this.connections[keys[k]].length; i++) {
                this.connections[keys[k]][i].connector.show();
                this.connections[keys[k]][i].update();
            }
        }
        // Hide the element itself
        this.svg.show();
        return this;
    }

    visible(state) {
        if (state === undefined) {
            return this._visible;
        }
        if (state) {
            this.show();
        } else {
            this.hide();
        }
        return this;
    }
};

/** @brief Add a new tool
 * 
 * The new tool will be a dummy tool, with an unique name
 * 
 * @return The new tool
 */
function addTool() {
    var tool = new Tool();
    TOOLS.push(tool);
    return tool;
}

/** @brief Get the input and output ports of an eventual group of tools
 *
 * This function returns, for the provided set of tools, all the input
 * and output port candidates to become the group ports.
 *
 * A port is candidate if it is unconnected, or connected just with external
 * tools, i.e. tools don't belonging to the group.
 *
 * If the tool detects a tool with bulk connections, i.e. connections within
 * the group itself, as well as extern connections, then 2 empty lists will be
 * returned, indicating that a group cannot be set on top of this tools list.
 *
 * @tools List of tools that will eventually conform the group.
 * @return map with 2 lists of ports for the "inPort" and "outPort".
 */
function getGroupPorts(tools) {
    var ports = {"inPort":[], "outPort":[]};
    for (var i=0; i<tools.length; i++) {
        var tool = tools[i];
        var keys = ["inPort", "outPort"];
        var targetKeys = ["target", "source"];
        for (var k=0; k<keys.length; k++) {
            var isEdge = undefined;
            var conns = tool.connections[keys[k]];
            for (var j=0; j<conns.length; j++) {
                var visitor = conns[j][targetKeys[k]];
                if ((visitor !== origPort) &&
                    (tools.indexOf(visitor.owner()) !== -1)) {
                    if (isEdge === true) {
                        // This guy has some bulk and some edge connections,
                        // so it's impossible to define the group
                        return {"inPort":[], "outPort":[]};
                    }
                    isEdge = false;
                }
                else {
                    if (isEdge === false) {
                        // This guy has some bulk and some edge connections,
                        // so it's impossible to define the group
                        return {"inPort":[], "outPort":[]};
                    }
                    isEdge = true;
                }
            }
            if ((isEdge === undefined) || (isEdge === true)) {
                ports[keys[k]].push(tool[keys[k]]);
            }
        }
    }
    return ports;
}

/** Check if a list of tools can be grouped.
 * @tools List of tools that will eventually conform the group.
 * @return map with 2 lists of ports for the "inPort" and "outPort".
 * @see getGroupPorts()
 */
function canMakeGroup(tools) {
    if (tools === undefined) {
        tools = getSelectedTools();
    }
    var ports = getGroupPorts(tools);
    if ((ports["inPort"].length == 1) &&
        (ports["outPort"].length == 1)) {
        // The selection has just an input port and an output port. Just check
        // they don't belong to the same tool
        if(ports["inPort"][0].owner() !== ports["outPort"][0].owner()){
            return true;
        }
    }
    return false;
}

class Group extends Tool {
    constructor(name, tools) {
        if(name === undefined) {
            var i = 0;
            while (getTool("Group" + i.toString()) !== undefined) {
                i += 1;
            }
            name = "Group" + i.toString();
        }
        if(tools === undefined) {
            tools = getSelectedTools();
        }
        super(name);
        this._tools = tools;
        this.removeButton.load("imgs/unpack_black.svg");

        // Place the SVG at the barycenter of the tools
        var cx = tools[0].svg.transform().x + tools[0].box.cx();
        var cy = tools[0].svg.transform().y + tools[0].box.cy();
        for (var i=1; i<tools.length; i++) {
            cx += tools[i].svg.transform().x + tools[i].box.cx();
            cy += tools[i].svg.transform().y + tools[i].box.cy();
        }
        cx /= tools.length;
        cy /= tools.length;
        this.box.center(cx, cy);
        this.__updateSVG();
        // Store the center coordinates to know (at the time of ungrouping) the
        // new position of each element
        this._cx = cx;
        this._cy = cy;

        // Transfer all the connections to the group ports
        var ports = getGroupPorts(tools);
        var keys = ["inPort", "outPort"];
        var sourceKeys = ["source", "target"];
        for (var k=0; k<keys.length; k++) {
            var tool = ports[keys[k]][0].owner();
            var conns = tool.connections[keys[k]];
            for (var j=0; j<conns.length; j++) {
                conns[j][sourceKeys[k]] = this[keys[k]];
                conns[j].update();
                this.connections[keys[k]].push(conns[j]);
            }
            tool.connections[keys[k]] = [];
        }
        // Store also the ports to can restore later the connectors
        this._ports = ports;

        // Hide all the tools
        for (var i=0; i<tools.length; i++) {
            tools[i].group = this;
        }

        // Show the hidden edge connectors
        var keys = ["inPort", "outPort"];
        for (var k=0; k<keys.length; k++) {
            var conns = this.connections[keys[k]];
            for (var j=0; j<conns.length; j++) {
                conns[j].connector.show();
            }
        }
    }

    /** Ungroup
     */
    remove() {
        var tools = this._tools;
        // Show again the grouped tools
        var cx = this.svg.transform().x + this.box.cx();
        var cy = this.svg.transform().y + this.box.cy();
        var dx = cx - this._cx, dy = cy - this._cy;
        for (var i=0; i<tools.length; i++) {
            tools[i].box.dmove(dx, dy);
            tools[i].__updateSVG();
            tools[i].group = undefined;
        }
        // Transfer the connectors back to their legitime owners
        var ports = this._ports;
        var keys = ["inPort", "outPort"];
        var sourceKeys = ["source", "target"];
        for (var k=0; k<keys.length; k++) {
            var tool = ports[keys[k]][0].owner();
            var conns = this.connections[keys[k]];
            for (var j=0; j<conns.length; j++) {
                conns[j][sourceKeys[k]] = ports[keys[k]][0];
                conns[j].update();
                tool.connections[keys[k]].push(conns[j]);
            }
            this.connections[keys[k]] = [];
        }

        super.remove();

        rearrangeElementsZ();
    }

    /** Get the names of the grouped tools
     * @return array of names of the grouped tools
     */
    grouped_names() {
        var names = [];
        for (var i = 0; i < this._tools.length; i++) {
            names.push(this._tools[i].name);
        }
        return names;
    }
};

/** Add a new group of tools, with an unique name
 * @tools List of tools to be grouped
 * @return The new group, undefined if the tools cannot be grouped
 * @see canMakeGroup()
 */
function addGroup(tools) {
    if (tools === undefined) {
        tools = getSelectedTools();
    }
    var tool;
    if (canMakeGroup(tools)) {
        tool = new Group(undefined, tools);
    } else {
        return undefined;
    }
    TOOLS.push(tool);
    rearrangeElementsZ();
    return tool;
}

var __canvas = SVG('ToolsCanvas').size("100%", "100%").panZoom();
function getCanvas() {
    return __canvas;
}
// Generate the origin port
var origPort = __canvas.circle(20).attr({fill: 'red',
                                         stroke: 'black',
                                         'stroke-width': 3});
origPort.center(0, 0);
origPort.connections = [];
var __TO_CENTER__ = true;  // We must center the image on the first load

/** Set the Z position of all elements
 * @see rearrangeElementsXY()
 */
function rearrangeElementsZ() {
    origPort.front();
    var conns = [];
    for (var i=0; i<TOOLS.length; i++) {
        TOOLS[i].svg.front();
        TOOLS[i].__updateSVG();
        var keys = ["inPort", "outPort"];
        for (var k=0; k<keys.length; k++) {
            conns = conns.concat(TOOLS[i].connections[keys[k]]);
        }
    }
    for (var j=0; j<conns.length; j++) {
        conns[j].connector.front();
        // Take advantage to update
        conns[j].update();
    }
}

/** Arrange the positions of the tools to get as best as possible connections
 * visualization
 * @see rearrangeElementsZ()
 */
function rearrangeElementsXY() {
    // First, let's setup a map of tools and dependencies
    var grid_dx = 0;
    var grid_dy = 0;
    var tools = {};
    for (var i=0; i<TOOLS.length; i++) {
        if(!TOOLS[i].visible()) {
            continue;
        }
        var bbox = TOOLS[i].svg.bbox();
        grid_dx = Math.max(grid_dx, bbox.w);
        grid_dy = Math.max(grid_dy, bbox.h);

        var conns = TOOLS[i].connections.inPort;
        var deps = [];
        for (var j=0; j<conns.length; j++) {
            var conn = conns[j];
            if (conn.target === origPort) {
                continue
            } else {
                deps.push(conn.target.owner().name);
            }
        }
        tools[TOOLS[i].name] = {added:false, deps:deps, slot_x:0, slot_y:0,
                                obj:TOOLS[i]};
    }
    grid_dx += 10;
    grid_dy += 5;
    var grid_x0 = origPort.cx() + 0.5 * grid_dx;
    var grid_y0 = origPort.cy();
    // Now distribute the tools in the grid slots
    grid = {};
    while (true) {
        var remaining = 0;
        var keys = Object.keys(tools);
        for (var k=0; k<keys.length; k++) {
            var i = keys[k];
            // Skip already added tools
            if(tools[i].added) {
                continue;
            }
            remaining += 1;
            // Don't parse the tools with pending dependencies
            var can_add = true;
            var slot_x = 0;
            var slot_y_min = 0, slot_y_max = 0;
            for (var j=0; j<tools[i].deps.length; j++) {
                var dep = tools[i].deps[j];
                if (!tools[dep].added) {
                    can_add = false;
                    break;
                }
                slot_x = Math.max(slot_x, tools[dep].slot_x + 1);
                slot_y_max = Math.max(slot_y_max, tools[dep].slot_y);
                slot_y_min = Math.min(slot_y_min, tools[dep].slot_y);
            }
            if(!can_add) {
                continue;
            }
            var slot_y = Math.round(0.5 * (slot_y_min + slot_y_max));

            // Allocate the tool in the grid
            if (grid[slot_x] === undefined) {
                grid[slot_x] = {};
            }
            while (grid[slot_x][slot_y] !== undefined) {
                slot_y = (Math.sign(slot_y) < 0) ? -slot_y : -(slot_y + 1);
            }
            grid[slot_x][slot_y] = tools[i].obj;
            tools[i].slot_x = slot_x;
            tools[i].slot_y = slot_y;

            // Place the tool
            var tool = tools[i].obj;
            tool.svg.center(0.5 * tool.box.width(),
                            0.5 * tool.box.height());
            tool.box.center(grid_x0 + slot_x * grid_dx,
                            grid_y0 + slot_y * grid_dy);
            tool.__updateSVG();
            tools[i].added = true;
        }

        if(remaining === 0) {
            break;
        }
    }
    rearrangeElementsZ();
}

function updateTools() {
    if (__TO_CENTER__) {
        __TO_CENTER__ = false;
        var vbox = __canvas.viewbox();
        var x = origPort.cx() - Math.max(3 * origPort.attr('r'),
                                         vbox.width / 10);
        var y = origPort.cy() - vbox.height / 2;
        __canvas.viewbox({x:x, y:y, width:vbox.width, height:vbox.height});
    }

    rearrangeElementsZ();
}

// We don't want to call rearrangeElementsZ on every single zoom event, since it
// excessively delays it. In order to call rearrangeElementsZ just once, we are
// storing the delayed queue (0.25 seconds), cancelling the already queried
// executions when a new one arrives
TOOLTAB_REARRANGEELEMENTSZ_QUERY = undefined;
function rearrangeElementsZQuery() {
    TOOLTAB_REARRANGEELEMENTSZ_QUERY = undefined;
    rearrangeElementsZ();
}
__canvas.on('zoom', function(ev) {
    if (TOOLTAB_REARRANGEELEMENTSZ_QUERY !== undefined) {
        clearTimeout(TOOLTAB_REARRANGEELEMENTSZ_QUERY);
    }
    TOOLTAB_REARRANGEELEMENTSZ_QUERY = setTimeout(rearrangeElementsZQuery, 250); 
});
__canvas.on('panEnd', function(ev) {
    rearrangeElementsZ();
});
__canvas.on('pinchZoomEnd', function(ev) {
    rearrangeElementsZ();
});

// Register the table errors check as loading callback
registerTabCallback('Tools', updateTools);
