__TABS_CALLBACKS = {}
__HIDDEN_TAB_BUTTONS = [];

function showAlltabs() {
    var dropdownMenu = $('#tabsDropdownMenu');
    for (var i=__HIDDEN_TAB_BUTTONS.length - 1; i>=0; i--) {
        $(__HIDDEN_TAB_BUTTONS[i]).css('display', '');
        if (dropdownMenu.children('a')[i].classList.contains('active')) {
            $(__HIDDEN_TAB_BUTTONS[i]).addClass('active');
        }
        __HIDDEN_TAB_BUTTONS.splice(i, 1);
        // We should remove the link from the dropdown menu
        $(dropdownMenu.children('a')[i]).remove();
    }
}

function resizeTabContent() {
    var h = $(window).height();
    $('.tabcontent').css('height', (h - 64).toString() + 'px');

    // Move overflown tab bar buttons to the dropdown menu
    var navbar = $('#navbar');
    var maxW = navbar.innerWidth();
    maxW -= navbar.children('.navbar-brand').outerWidth(true);
    var dropdown = $('#tabsDropdown');
    if (__HIDDEN_TAB_BUTTONS.length > 0) {
        maxW -= dropdown.outerWidth(true);
    }
    var dropdownMenu = $('#tabsDropdownMenu');
    var bar = $('#tabsBar');
    var buttons = bar.children('button');
    var W = $('#loadButton').parent().outerWidth(true);
    var n = buttons.length - __HIDDEN_TAB_BUTTONS.length
    for (var i=0; i<n; i++) {
        var w = $(buttons[i]).outerWidth(true);
        if (W + w > maxW) {
            if (__HIDDEN_TAB_BUTTONS.length === 0) {
                // We shall shown the dropdown toggler
                dropdown.parent().css('display', 'inline');
                maxW -= dropdown.outerWidth(true);
            }
            // We should hide the button
            __HIDDEN_TAB_BUTTONS.push(buttons[i]);
            $(buttons[i]).css('display', 'none');
            // We should add a link to the dropdown menu
            var a = $('<a href="#">');
            a.addClass('dropdown-item');
            a.addClass('tablinks');
            a.data('tabid', $(buttons[i]).data('tabid'));
            a.text($(buttons[i]).text());
            a.on("click", function(evt) {
                openTab(evt, $(this).data('tabid'));
            });
            if (buttons[i].classList.contains('active')) {
                a.addClass('active');
            }
            dropdownMenu.append(a);
            continue;
        }
        W += w;
    }
    // Show again tab buttons if we have room enough for that. We should use
    // FILO filosofy to get back the buttons
    for (var i=__HIDDEN_TAB_BUTTONS.length - 1; i>=0; i--) {
        var w = $(__HIDDEN_TAB_BUTTONS[i]).outerWidth(true);
        if (__HIDDEN_TAB_BUTTONS.length == 1) {
            // If we show this tab button, then the dropdown toggler will be
            // hidden, making 16px extra room
            maxW += 16;
        }
        if (W + w >= maxW) {
            // No more buttons can be fit in the bar
            break;
        }
        // We can show back the button
        $(__HIDDEN_TAB_BUTTONS[i]).css('display', '');
        if (dropdownMenu.children('a')[i].classList.contains('active')) {
            $(__HIDDEN_TAB_BUTTONS[i]).addClass('active');
        }
        __HIDDEN_TAB_BUTTONS.splice(i, 1);
        // We should remove the link from the dropdown menu
        $(dropdownMenu.children('a')[i]).remove();
        W += w;
    }
    // Hide the dropdown toggler if there are not hidden tab buttons
    if (!__HIDDEN_TAB_BUTTONS.length) {
        dropdown.parent().css('display', 'none');
    }
}

function openTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    if (evt !== undefined) {
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
    }

    // Show the current tab, and add an "active" class to the button that opened
    // the tab
    document.getElementById(tabName).style.display = "block";
    if (evt !== undefined) {
        evt.currentTarget.className += " active";
    }

    // Call the callback
    if (__TABS_CALLBACKS[tabName] !== undefined) {
        (__TABS_CALLBACKS[tabName])(evt);
    }
}

function restoreTab() {
    var tablinks = document.getElementsByClassName("tablinks active");
    openTab(undefined, $(tablinks[0]).data('tabid'));
}

function registerTabCallback(tabName, callback) {
    __TABS_CALLBACKS[tabName] = callback;
}
