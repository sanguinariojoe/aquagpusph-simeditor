function scapeFromXML(txt) {
    if (typeof txt === "undefined") {
        return undefined;
    }
    return txt;
}
function scapeToXML(txt) {
    if (typeof txt === "undefined") {
        return "";
    }
    var edit = txt.replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(
                           /'/g, "&apos;").replace(/</g, "&lt;").replace(
                           />/g, "&gt;");
    // The DOM parser is already replacing & inside, so we need a specific code
    // that the DOM parser is not editing, and so we can restore afterwards
    edit = edit.replace(/&/g, "@@AMP_SYMBOL@@");

    return edit
}

//                                   Load
// =============================================================================

function parseXMLVariables(root) {
    openTab(undefined, 'Variables');
    var VariablesNodes = root.children('Variables');
    VariablesNodes.each(function (index) {
        var VariableNodes = $(this).children('Variable');
        VariableNodes.each(function (index) {
            var node = $(this);
            var symbol = node.attr('symbol');
            if (typeof symbol !== "undefined") {
                symbol = scapeFromXML(symbol);
            }
            addVariable(node.attr('name'),
                        node.attr('type'),
                        node.attr('length'),
                        node.attr('value'),
                        symbol);
        });
    });
}

function parseXMLDefinitions(root) {
    openTab(undefined, 'Definitions');
    var DefinitionsNodes = root.children('Definitions');
    DefinitionsNodes.each(function (index) {
        var DefineNodes = $(this).children('Define');
        DefineNodes.each(function (index) {
            var node = $(this);
            var evaluated = node.attr('evaluate') === 'true';
            addDefinition(node.attr('name'),
                          node.attr('value'),
                          evaluated);
        });
    });
}

function parseXMLSets(root) {
    openTab(undefined, 'Sets');
    var SetNodes = root.children('ParticlesSet');
    SetNodes.each(function (index) {
        var set = addParticlesSet();
        set.n = $(this).attr('n');
        var loadNode = $($(this).children('Load')[0]);
        set.inFile = loadNode.attr("file");
        set.inFields = loadNode.attr("fields");
        var saveNode = $($(this).children('Save')[0]);
        var ext = ".vtu";
        if(saveNode.attr("format") !== "VTK") {
            ext = ".dat";
        }
        set.outFile = saveNode.attr("file") + ext;
        set.outFields = saveNode.attr("fields");
        var ScalarNodes = $(this).children('Scalar');
        ScalarNodes.each(function (index) {
            var node = $(this);
            var name = "var_" + node.attr("name");
            if (typeof set[name] === "undefined") {
                console.log("ParticlesSet Scalar variable " + name + " is undefined!");
                return;
            }
            set[name].set(node.attr("value"));
        });
    });
}

function parseXMLTools(root) {
    openTab(undefined, 'Tools');
    var ATTRS = {
        "kernel":["path", "entry_point"],
        "copy":["in", "out"],
        "set":["in", "value"],
        "python":["path"],
        "set_scalar":["in", "value"],
        "reduction":["in", "out", "null"],
        "link-list":["in"],
        "radix-sort":["in", "perm", "inv_perm"],
        "assert":["condition"],
        "if":["condition"],
        "while":["condition"],
        "end":[],
        "mpi-sync":["mask", "fields", "processes"],
        "dummy":[],
        "report_screen":["fields", "color", "bold"],
        "report_file":["fields", "path"],
        "report_particles":["set", "fields", "path", "fps", "ipf"],
        "report_performance":["color", "bold", "path"],
    };
    var SPACE_X = 100, SPACE_Y = 50;
    var ToolsNodes = root.children('Tools');
    ToolsNodes.each(function (index) {
    var ToolNodes = $(this).children('Tool');
    var restore_groups = [];
    ToolNodes.each(function (index) {
        var tool = addTool();
        // Common attributes
        tool.name = $(this).attr('name');
        tool.type = $(this).attr('type');
        if (typeof $(this).attr('once') !== "undefined") {
            tool.once = $(this).attr('once').toLowerCase() === 'true';
        }
        for (var i=0; i<ATTRS[tool.type].length; i++) {
            var attr = ATTRS[tool.type][i];
            if (typeof $(this).attr(attr) !== "undefined") {
                tool.data[tool.type][attr] = scapeFromXML($(this).attr(attr));
            }
        }
        if (typeof tool.data[tool.type]["enclosed_file"] !== "undefined") {
            // Check if the file already exists
            var filesmap = {};
            if (tool.type == "kernel") {
                filesmap = CL_FILES;
            } else if (tool.type == "python") {
                filesmap = PY_FILES;
            }

            tool.data[tool.type]["enclosed_file"] = false;
            if (tool.data[tool.type]["path"] in filesmap) {
                filesmap[tool.data[tool.type]["path"]].refs += 1;
                tool.data[tool.type]["enclosed_file"] = true;
            }
        }
        if (typeof tool.data[tool.type]["text"] !== "undefined") {
            tool.data[tool.type]["text"] = scapeFromXML($(this).text());
        }

        var action = $(this).attr('action');
        switch(action) {
            case "add":
                // Get the joint
                var outPort = origPort;
                var x = outPort.cx(), y = outPort.cy();
                var connections = origPort.connections;
                if (typeof __ACTIVE_TOOL !== "undefined") {
                    outPort = __ACTIVE_TOOL.outPort;
                    x = outPort.parent().transform().x + outPort.cx();
                    y = outPort.parent().transform().y + outPort.cy();
                    connections = __ACTIVE_TOOL.connections.outPort;
                }
                // Get the best position
                tool.box.center(-1000, -1000);
                tool.__updateSVG();
                x += SPACE_X;
                var sign = -1, dist_mult = 0;
                while (getToolsByPoint(x, y).length > 0) {
                    sign *= -1;
                    dist_mult++;
                    y += sign * dist_mult * SPACE_Y;
                }
                var bbox = tool.svg.bbox();
                tool.box.center(x + bbox.w / 2, y);
                tool.__updateSVG();

                // Connect the tool
                var con = tool.inPort.connectable({
                    targetAttach: 'center',
                    sourceAttach: 'center',
                    type: 'curved-fromin'
                }, outPort);
                con.connector.attr('stroke-width', 3);
                con.connector.attr('stroke-opacity', 0.5);
                connections.push(con);
                tool.connections.inPort.push(con);

                break;
            case "insert":
            case "try_insert":
                if (typeof $(this).attr('after') === "undefined") {
                    console.log('Only insert "after" action is allowed in the non-linear editor');
                    return;
                }
                var after = $(this).attr('after').split(",");
                var x = undefined, y = undefined, conns = [];
                for (var i=0; i<after.length; i++) {
                    var prev = getTool(after[i]);
                    if (typeof prev === "undefined") {
                        continue;
                    }

                    // If the tool is grouped, we need to ungroup it. To this
                    // end, we need to setup a grouping chain (since the owner
                    // group can be grouped itself, and so on), reversely
                    // unpacking it
                    var group = prev.group;
                    var group_chain = [];
                    while (typeof group !== "undefined") {
                        group_chain.push(group);
                        group = group.group;
                    }
                    for (var j = group_chain.length - 1; j >= 0; j--) {
                        restore_groups.push({
                            "name" : group_chain[j].name,
                            "tools" : group_chain[j].grouped_names()
                        });
                        group_chain[j].remove();
                    }
                    
                    var outPort = prev.outPort;
                    if ((typeof x === "undefined") ||
                        (x < outPort.parent().transform().x + outPort.cx())) {
                        x = outPort.parent().transform().x + outPort.cx();
                        y = outPort.parent().transform().y + outPort.cy();
                    }
                    // Connect the tool
                    var con = tool.inPort.connectable({
                        targetAttach: 'center',
                        sourceAttach: 'center',
                        type: 'curved-fromin'
                    }, outPort);
                    con.connector.attr('stroke-width', 3);
                    con.connector.attr('stroke-opacity', 0.5);
                    prev.connections.outPort.push(con);
                    tool.connections.inPort.push(con);
                    conns.push(con);
                }
                // Get the best position
                tool.box.center(-1000, -1000);
                tool.__updateSVG();
                x += SPACE_X;
                var sign = -1, dist_mult = 0;
                while (getToolsByPoint(x, y).length > 0) {
                    sign *= -1;
                    dist_mult++;
                    y += sign * dist_mult * SPACE_Y;
                }
                var bbox = tool.svg.bbox();
                tool.box.center(x + bbox.w / 2, y);
                tool.__updateSVG();
                for (var i=0; i<conns.length; i++) {
                    conns[i].update();
                }
                break;
            case "remove":
            case "try_remove":
                var name = tool.name;
                if(typeof getTool(name + '.bak') !== "undefined") {
                    getTool(name + '.bak').remove();
                }
                // Actually we did not wanted to add a new tool
                tool.remove();
                return;
            case "replace":
            case "try_replace":
                var name = tool.name;
                // Actually we did not wanted to add a new tool
                tool.remove();
                var replaced = getTool(name + '.bak')
                if(typeof replaced !== "undefined") {
                    // We shall recover the former name
                    replaced.name = name;
                    replaced.type = tool.type;
                    replaced.data = tool.data;
                }
                return;
            default:
                console.log("Unhandled action:", action);
                return;
        }

        var before = $(this).attr('also_before');
        if (typeof before === "undefined") {
            before = "";
        }
        var before = before.split(",");
        for (var i=0; i<before.length; i++) {
            var next = getTool(before[i]);
            if (typeof next === "undefined") {
                continue;
            }

            // If the tool is grouped, we need to ungroup it. To this
            // end, we need to setup a grouping chain (since the owner
            // group can be grouped itself, and so on), reversely
            // unpacking it
            var group = next.group;
            var group_chain = [];
            while (typeof group !== "undefined") {
                group_chain.push(group);
                group = group.group;
            }
            for (var j = group_chain.length - 1; j >= 0; j--) {
                restore_groups.push({
                    "name" : group_chain[j].name,
                    "tools" : group_chain[j].grouped_names()
                });
                group_chain[j].remove();
            }
                    
            var outPort = tool.outPort;
            // Connect the tool
            var con = next.inPort.connectable({
                targetAttach: 'center',
                sourceAttach: 'center',
                type: 'curved-fromin'
            }, outPort);
            con.connector.attr('stroke-width', 3);
            con.connector.attr('stroke-opacity', 0.5);
            tool.connections.outPort.push(con);
            next.connections.inPort.push(con);
            conns.push(con);
        }

        // Select the new tool as the active one
        var old_active_tool = __ACTIVE_TOOL;
        tool.selected = true;
        tool.box.attr({stroke:'#FF0000'});
        __ACTIVE_TOOL = tool;
        if(typeof old_active_tool !== "undefined") {
            old_active_tool.selected = true;
        }
    });  // Tool

    // Try to restore the removed groups
    for (var i=0; i<restore_groups.length; i++) {
        var tool_names = restore_groups[i]["tools"];
        var valid = true;
        var tools = [];
        for (var j=0; j<tool_names.length; j++) {
            var t = getTool(tool_names[j]);
            if (typeof t === "undefined") {
                valid = false;
                break;
            }
            tools.push(t);
        }
        if (!valid) {
            break;
        }
        var tool = addGroup(tools);
        if (typeof tool !== "undefined") {
            tool.name = restore_groups[i]["name"];
        }
    }

    // Create the asked new groups
    var GroupNodes = $(this).children('Group');
    GroupNodes.each(function (index) {
        var tool_names = $(this).attr('tools').split(",");
        var tools = [];
        for (var i=0; i<tool_names.length; i++) {
            var t = getTool(tool_names[i]);
            if (typeof t === "undefined") {
                console.log("Failure creating group '" + $(this).attr('name') + "'");
                console.log("    Can't find tool '" + tool_names[i] + "'");
            }
            tools.push(t);
        }
        var tool = addGroup(tools);
        if (typeof tool === "undefined") {
            // Can't group the tools
            console.log("Failure creating group '" + $(this).attr('name') + "'");
        }
        tool.name = $(this).attr('name');

        var old_active_tool = __ACTIVE_TOOL;
        tool.selected = true;
        tool.box.attr({stroke:'#FF0000'});
        __ACTIVE_TOOL = tool;
        if(typeof old_active_tool !== "undefined") {
            old_active_tool.selected = true;
        }
    });  // Group
    });  // Tools
}

function parseXMLSettings(root) {
    openTab(undefined, 'Settings');
    var SettingsNodes = root.children('Settings');
    SettingsNodes.each(function (index) {
        var DeviceNodes = $(this).children('Device');
        DeviceNodes.each(function (index) {
            var node = $(this);
            setDevice(node.attr('platform'),
                      node.attr('device'),
                      node.attr('type'));
        });
        var RootPathNodes = $(this).children('RootPath');
        RootPathNodes.each(function (index) {
            var node = $(this);
            setRootPath(node.attr('path'));
        });
    });
}

function parseXMLTiming(root) {
    openTab(undefined, 'Settings');
    var TimingNodes = root.children('Timing');
    TimingNodes.each(function (index) {
        var OptionNodes = $(this).children('Option');
        OptionNodes.each(function (index) {
            var node = $(this);
            setTimingOption(node.attr('name'),
                            node.attr('type'),
                            node.attr('value'),
                            true);
        });
    });
}

function loadXML(txt) {
    parser = new DOMParser();
    xml = $(parser.parseFromString(txt, "text/xml"));
    root = $(xml.children('sphInput')[0]);
    parseXMLVariables(root);
    parseXMLDefinitions(root);
    parseXMLSets(root);
    parseXMLTools(root);
    parseXMLSettings(root);
    parseXMLTiming(root);
    restoreTab();
}

function loadScript(filesmap, filename, txt) {
    if (!(filename in filesmap)) {
        filesmap[filename] = {
            refs: 0,
            txt: "",
            session: undefined,
        };
    }
    filesmap[filename].txt = txt;
}

function loadZIP(data) {
    var zip = new JSZip();
    zip.loadAsync(data).then(function(contents) {
        // Make a first loop loading just Python and OpenCL files
        Object.keys(contents.files).forEach(function(filename) {
            if (!filename.endsWith(".cl") && !filename.endsWith(".py")) {
                return;
            }
            zip.file(filename).async('string').then(function(txt) {
                var files_map;
                if (filename.endsWith(".cl")) {
                    loadScript(CL_FILES, filename, txt)
                } else if (filename.endsWith(".py")) {
                    loadScript(PY_FILES, filename, txt)
                }
            });
        });
        // Make a second loop loading just XML files
        Object.keys(contents.files).forEach(function(filename) {
            if (!filename.endsWith(".xml")) {
                return;
            }
            zip.file(filename).async('string').then(function(txt) {
                loadXML(txt)
            });
        });
    });
}

function loadFile(fileObj, onFinish) {
    var reader = new FileReader();
    if (fileObj.name.endsWith(".xml")) {
        reader.onload = function() {
            loadXML(reader.result);
            if (typeof onFinish !== "undefined") {
                onFinish();
            }
        };
        reader.readAsText(fileObj);
    } else {
        reader.onload = function(){
            loadZIP(reader.result);
            if (typeof onFinish !== "undefined") {
                onFinish();
            }
        };
        reader.readAsBinaryString(fileObj);
    }
}

function load(evt) {
    // Open a file selection dialog
    var fileDiag = $('<input type="file" style="display:none;" accept=".xml, .zip">');
    var reader;
    fileDiag.trigger('click');
    // Read the selected file
    fileDiag.on('change', function(e) {
        loadFile(e.target.files[0]);
    });
}

//                                   Save
// =============================================================================

function saveXMLVariables(root) {
    var variables = $.parseXML("<Variables />").documentElement;
    for (var i=__FIXED_VARIABLES; i<VARIABLES.length; i++) {
        var variable = $.parseXML("<Variable />").documentElement;
        $(variable).attr('name', VARIABLES[i].name);
        $(variable).attr('type', VARIABLES[i].type);
        $(variable).attr('length', VARIABLES[i].length);
        $(variable).attr('value', VARIABLES[i].value);
        $(variable).attr('symbol', scapeToXML(VARIABLES[i].symbol));
        $(variables).append(variable);
    }
    $(root).append(variables);
}

function saveXMLDefinitions(root) {
    var definitions = $.parseXML("<Definitions />").documentElement;
    for (var i=0; i<DEFINITIONS.length; i++) {
        var definition = $.parseXML("<Define />").documentElement;
        $(definition).attr('name', DEFINITIONS[i].name);
        $(definition).attr('value', DEFINITIONS[i].value);
        $(definition).attr('evaluate',
                           DEFINITIONS[i].evaluated ? 'true' : 'false');
        $(definitions).append(definition);
    }
    $(root).append(definitions);
}

function saveXMLSets(root) {
    for (var i=0; i<SETS.length; i++) {
        var set = $.parseXML("<ParticlesSet />").documentElement;
        $(set).attr('n', SETS[i].n);

        var keys = Object.keys(SETS[i]);
        for (var k=0; k<keys.length; k++) {
            var key = keys[k];
            if (key.startsWith("var_")) {
                var scalar = $.parseXML("<Scalar />").documentElement;
                $(scalar).attr("name", key.substring(4));
                $(scalar).attr("value", SETS[i][key].get());
                $(set).append(scalar);
            }
        }

        var load = $.parseXML("<Load />").documentElement;
        var infile = SETS[i].inFile;
        $(load).attr("format", infile.endsWith(".dat") ? 'FastASCII' : 'VTK');
        $(load).attr("file", infile);
        $(load).attr("fields", SETS[i].inFields);
        $(set).append(load);

        var save = $.parseXML("<Save />").documentElement;
        var outfile = SETS[i].outFile;
        $(save).attr("format", outfile.endsWith(".dat") ? 'FastASCII' : 'VTK');
        if (outfile.endsWith(".vtk") ||
            outfile.endsWith(".vtu") ||
            outfile.endsWith(".dat")) {
            outfile = outfile.substring(0, outfile.length - 4);
        }
        $(save).attr("file", outfile);
        $(save).attr("fields", SETS[i].outFields);
        $(set).append(save);

        $(root).append(set);
    }
}


function saveXMLTools(root) {
    /** Create a map of tools with their partners (tools that shall be executed
     * before) and their children (tools that are executed after)
     *
     * @return tools map, indexed by their names
     */
    function toolsMap() {
        var tools = {};
        for (var i=0; i<TOOLS.length; i++) {
            var conns = TOOLS[i].connections.inPort;
            var partners = [];
            for (var j=0; j<conns.length; j++) {
                var conn = conns[j];
                if (conn.target === origPort) {
                    continue
                } else {
                    partners.push(conn.target.owner().name);
                }
            }
            conns = TOOLS[i].connections.outPort;
            var children = [];
            for (var j=0; j<conns.length; j++) {
                children.push(conns[j].source.owner().name);
            }
            tools[TOOLS[i].name] = {added:false, partners:partners, children:children};
        }
        return tools;
    }

    /** Check if a tool has all its dependencies fulfilled, i.e. all its
     * partners has been added
     *
     * @param tool Tool to check
     * @param tools Tools map
     * @return true if the dependencies are fulfilled, false otherwise.
     */
    function canAddTool(tool, tools) {
        for (var i=0; i<tools[tool.name].partners.length; i++) {
            var dep = tools[tool.name].partners[i];
            if (!tools[dep].added) {
                return false;
            }
        }
        return true;
    }

    /** Select a tool which is ready to be added
     *
     * @param tools Tools map
     * @return The next tool ready to become added, null if there are not more tools
     * ready.
     */
    function selectTool(tools) {
        for (var i=0; i<TOOLS.length; i++) {
            // Ignore the groups, which are parsed at the end
            if (TOOLS[i] instanceof Group) {
                continue;
            }
            // Skip already added tools
            if(tools[TOOLS[i].name].added) {
                continue;
            }
            if(canAddTool(TOOLS[i], tools)) {
                return TOOLS[i];
            }
        }
        return null;
    }

    /** Add a tool to the XML node
     *
     * @param tool Tool to become added
     * @param tools Tools map
     * @param xmlnode XML node where the tool shall be added
     */
    function addTool(tool, tools, xmlnode) {
        var partners_str = '';
        for (var i=0; i<tools[tool.name].partners.length; i++) {
            var dep = tools[tool.name].partners[i];
            partners_str = partners_str + dep + ',';
        }
        if(partners_str.endsWith(',')) {
            partners_str = partners_str.substring(0, partners_str.length - 1);
        }

        var xmltool = $.parseXML("<Tool />").documentElement;
        $(xmltool).attr('name', tool.name);
        $(xmltool).attr('type', tool.type);
        $(xmltool).attr('once', tool.once);
        if (partners_str === '') {
            $(xmltool).attr('action', 'add');
        } else {
            $(xmltool).attr('action', 'insert');
            $(xmltool).attr('after', partners_str);
        }
        for (var i=0; i<ATTRS[tool.type].length; i++) {
            var attr = ATTRS[tool.type][i];
            $(xmltool).attr(attr, scapeToXML(tool.data[tool.type][attr]));
        }
        if (typeof tool.data[tool.type]["text"] !== "undefined") {
            $(xmltool).text(scapeToXML(tool.data[tool.type]["text"]));
        }

        $(xmlnode).append(xmltool);            
        tools[tool.name].added = true;
    }
    
    /** Parse and add as many tools as possible  following the lines for a
     * common partner
     *
     * @param tool Main partner tool
     * @param tools Tools map
     * @param xmlnode XML node where the tool shall be added
     */
    function addToolsLine(tool, tools, xmlnode) {
        addTool(tool, tools, xmlnode);
        for (var i=0; i<tools[tool.name].children.length; i++) {
            var child = getTool(tools[tool.name].children[i]);
            if (!tools[child.name].added && canAddTool(child, tools)) {
                addToolsLine(child, tools, xmlnode);
            }
        }
    }

    var ATTRS = {
        "kernel":["path", "entry_point"],
        "copy":["in", "out"],
        "set":["in", "value"],
        "python":["path"],
        "set_scalar":["in", "value"],
        "reduction":["in", "out", "null"],
        "link-list":["in"],
        "radix-sort":["in", "perm", "inv_perm"],
        "assert":["condition"],
        "if":["condition"],
        "while":["condition"],
        "end":[],
        "mpi-sync":["mask", "fields", "processes"],
        "dummy":[],
        "report_screen":["fields", "color", "bold"],
        "report_file":["fields", "path"],
        "report_particles":["set", "fields", "path", "fps", "ipf"],
        "report_performance":["color", "bold", "path"],
    };
    var tools_node = $.parseXML("<Tools />").documentElement;

    // We must store and remove all the groups, because they are perniciously
    // manipulating the connections data
    var groups = [];
    for (var i=TOOLS.length - 1; i>=0; i--) {
        if (!(TOOLS[i] instanceof Group)) {
            continue;
        }
        groups.push({name:TOOLS[i].name, tools:TOOLS[i]._tools});
        TOOLS[i].remove();
    }

    // First, let's setup a map of tools and dependencies
    tools = toolsMap();
    // Now we parse the list of tools adding the ones with all the dependencies
    // already fulfilled. This process will be repeated until all the tools
    // have been added.
    var tool;
    while ((tool = selectTool(tools)) !== null) {
        addToolsLine(tool, tools, tools_node);
    }

    // Finally, we can restore and save the groups, in a FILO way, to can handle
    // groups of groups
    for (var i=groups.length - 1; i>=0; i--) {
        // The stored groups are not valid anymore, so we must replace them by
        // the new instances
        for (var j=0; j<groups[i].tools.length; j++) {
            if (groups[i].tools[j] instanceof Group) {
                groups[i].tools[j] = getTool(groups[i].tools[j].name);
            }
        }

        var tool = addGroup(groups[i].tools);
        tool.name = groups[i].name;

        var gtools = '';
        for (var j=0; j<groups[i].tools.length; j++) {
            var gtools = gtools + groups[i].tools[j].name + ',';
        }
        if(gtools.endsWith(',')) {
            gtools = gtools.substring(0, gtools.length - 1);
        }

        tool = $.parseXML("<Group />").documentElement;
        $(tool).attr('name', groups[i].name);
        $(tool).attr('tools', gtools);

        $(tools_node).append(tool);
    }

    $(root).append(tools_node);
}

function saveXMLSettings(root) {
    var settings = $.parseXML("<Settings />").documentElement;

    var device = $.parseXML("<Device />").documentElement;
    $(device).attr('platform', SETTINGS.Device.platform);
    $(device).attr('device', SETTINGS.Device.device);
    $(device).attr('type', SETTINGS.Device.type);
    $(settings).append(device);

    var rootpath = $.parseXML("<RootPath />").documentElement;
    $(rootpath).attr('path', SETTINGS.RootPath);
    $(settings).append(rootpath);

    $(root).append(settings);
}

function saveXMLTiming(root) {
    var timing = $.parseXML("<Timing />").documentElement;

    for (var k1=0; k1<Object.keys(TIMING.Options).length; k1++) {
        var name = Object.keys(TIMING.Options)[k1];
        for (var k2=0; k2<Object.keys(TIMING.Options[name]).length; k2++) {
            var type = Object.keys(TIMING.Options[name])[k2];
            if (!TIMING.Options[name][type].enabled) {
                continue;
            }
            var option = $.parseXML("<Option />").documentElement;
            $(option).attr('name', name);
            $(option).attr('type', type);
            $(option).attr('value', TIMING.Options[name][type].value);
            $(timing).append(option);
        }
    }

    $(root).append(timing);
}


function saveXML() {
    var xml = $("<ROOT>");
    var root = $.parseXML("<sphInput />").documentElement;
    saveXMLVariables(root);
    saveXMLDefinitions(root);
    saveXMLSets(root);
    saveXMLTools(root);
    saveXMLSettings(root);
    saveXMLTiming(root);
    xml.append(root);
    var txt = '<?xml version="1.0" ?>\n\n' + xml.html();
    return txt.replace(/@@AMP_SYMBOL@@/g, "&");
}

function isFileActuallyReferenced(fpath) {
    for (var i=0; i<TOOLS.length; i++) {
        if ((TOOLS[i].type !== "kernel") && (TOOLS[i].type !== "python")) {
            continue;
        }
        if (TOOLS[i].data[TOOLS[i].type]["path"] === fpath) {
            return true;
        }
    }
    return false;
}

function save (evt) {
    var zip = new JSZip();
    zip.file("Main.xml", saveXML());
    var keys = Object.keys(CL_FILES);
    for (var k=0; k<keys.length; k++) {
        if (!isFileActuallyReferenced(keys[k])) {
            continue;
        }
        zip.file(keys[k], CL_FILES[keys[k]].txt);
    }
    var keys = Object.keys(PY_FILES);
    for (var k=0; k<keys.length; k++) {
        if (!isFileActuallyReferenced(keys[k])) {
            continue;
        }
        zip.file(keys[k], PY_FILES[keys[k]].txt);
    }
    zip.generateAsync({type:"blob"}).then(function(content) {
        saveAs(content, "simulation.zip");
    });
}
