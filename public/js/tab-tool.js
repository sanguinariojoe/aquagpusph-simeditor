var __TOOLTAB = undefined;
var __TABTOOL = undefined;
var CL_FILES = {"kernel.cl":{
    refs:Math.floor(Number.MAX_SAFE_INTEGER / 2),
    txt: `#include "resources/Scripts/types/types.h"

// You can use here all the variables defined in the Variables tab
__kernel void entry(__global vec* r,
                    unsigned int N)
{
    unsigned int i = get_global_id(0);
    if(i >= N)
        return;

    // Now you can access and modify the ith particle attributes
    // r[i] = ...;
}`,
    session: undefined,  // editor session
}};

var PY_FILES = {"script.py":{
    refs:Math.floor(Number.MAX_SAFE_INTEGER / 2),
    txt: `import numpy as np
import aquagpusph

def main():
    # You can get all the variables defined in the Variables tab
    # dims = aqua.get("dims")
    # And set them
    # aqua.set("dims", dims)

    # Return True to continue the AQUAgpusph execution, False to stop it
    return True`,
    session: undefined,  // editor session
}};

function parseBool(str) {
    return str.toLowerCase() === 'true';
}

function updateKernelFiles() {
    var input = $("#kernel_path");
    input.editableSelect('clear');
    var fnames = Object.keys(CL_FILES);
    for (var i=0; i<fnames.length; i++) {
        input.editableSelect('add', fnames[i]);
    }
}

function resizeCLEditor() {
    var editor = $('#cleditor');
    var p = editor.prev();
    var div = editor.parent();
    var y = div.offset().top - 64 + p.outerHeight(true);
    editor.css('top', y.toString() + 'px');
}

function setupKernelContent(tool) {
    // Kernel path
    // ===========
    var input = $("#kernel_path");
    if (input.prop('nodeName') === "SELECT") {
        input.editableSelect({ filter:false });
        input = $("#kernel_path");
    }

    updateKernelFiles();
    input.val(tool.data["kernel"]["path"]);

    function kernelPathChange() {
        var input = $(this);
        var fname = $(this).val();
        var tool = __TABTOOL;
        if (fname === "") {
            // Bad name, restore the previous one
            input.val(tool.data["kernel"]["path"]);
            return;
        }

        if (tool.data["kernel"]["enclosed_file"]) {
            // Avoid routes
            if (fname.lastIndexOf("/") !== -1) {
                fname = fname.substring(fname.lastIndexOf("/") + 1);
            }
            // Remove special chars
            fname = encodeURIComponent(fname);
            if (fname === "") {
                // Unhandeable, restore previous name
                input.val(tool.data["kernel"]["path"]);
                return;
            }
            input.val(fname);

            if (CL_FILES[tool.data["kernel"]["path"]] !== undefined) {
                // We are migrating, decrease its reference
                CL_FILES[tool.data["kernel"]["path"]].refs--;
            }
            if (CL_FILES[fname] === undefined) {
                // Duplicate the file if possible, start a new empty file
                // otherwise
                var txt = "";
                if (CL_FILES[tool.data["kernel"]["path"]] !== undefined) {
                    txt = CL_FILES[tool.data["kernel"]["path"]].txt;
                }
                CL_FILES[fname] = {refs:0, txt:txt};
                // Add it to the options
                input.editableSelect('add', fname);
            }
            CL_FILES[fname].refs++;
            if ((CL_FILES[tool.data["kernel"]["path"]] !== undefined) &&
                (CL_FILES[tool.data["kernel"]["path"]].refs === 0)) {
                // Unused element, time to remove it!
                delete CL_FILES[tool.data["kernel"]["path"]];
            }
            updateKernelFiles();

            // Show the editor (loading the appropiate session)
            var editor = $('#cleditor');
            editor.show();
            // At this point, the file should be ready
            var session = CL_FILES[fname].session;
            if (session === undefined) {
                session = new ace.EditSession(
                    CL_FILES[fname].txt);
                session.setMode("ace/mode/c_cpp");
                CL_FILES[fname].session = session;
            }
            __CLEDITOR.setSession(session);
            resizeCLEditor();
        }
        
        tool.data["kernel"]["path"] = fname;
    }
    input.off('select.editable-select').off('change.tabtool');
    input.on('select.editable-select', kernelPathChange);
    input.on('change.tabtool', kernelPathChange);

    // Kernel entry point
    // ==================
    input = $("#kernel_entry_point");
    input.val(tool.data["kernel"]["entry_point"]);
    input.off('change.tabtool').on('change.tabtool', function(e) {
        var txt = $(this).val();
        var tool = __TABTOOL;
        var c_regex = RegExp("^[a-zA-Z_]\\w*$");
        if (c_regex.test(txt)) {
            tool.data["kernel"]["entry_point"] = txt;
        }
        else {
            $(this).val(tool.data["kernel"]["entry_point"]);
        }
    });

    // Enclosed file
    // =============
    input = $("#kernel_enclosed_file");
    input.off('change.tabtool').on('change.tabtool', function() {
        var checked = $(this).prop('checked');
        var tool = __TABTOOL;
        tool.data["kernel"]["enclosed_file"] = checked;

        // Create/remove enclosed files
        if ((!checked) &&
            (CL_FILES[tool.data["kernel"]["path"]] !== undefined)) {
            // Decrease the file reference (and eventually remove it)
            CL_FILES[tool.data["kernel"]["path"]].refs--;
            if (CL_FILES[tool.data["kernel"]["path"]].refs === 0) {
                delete CL_FILES[tool.data["kernel"]["path"]];
                updateKernelFiles();
            }
        }
        $("#kernel_path").trigger('change.tabtool');

        // Show/hide the editor
        var editor = $('#cleditor');
        if (checked) {
            editor.show();
            // At this point, the file should be ready
            var session = CL_FILES[tool.data["kernel"]["path"]].session;
            if (session === undefined) {
                session = new ace.EditSession(
                    CL_FILES[tool.data["kernel"]["path"]].txt);
                session.setMode("ace/mode/c_cpp");
                CL_FILES[tool.data["kernel"]["path"]].session = session;
            }
            __CLEDITOR.setSession(session);
            resizeCLEditor();
        } else {
            editor.hide();
        }
    });
    input.prop('checked', tool.data["kernel"]["enclosed_file"]);
    input.trigger('change.tabtool');
}


function setupCopyContent(tool) {
    var keys = ["in", "out"];
    var arrays = getArrayVariables();
    for (var i=0; i<keys.length; i++) {
        var key = keys[i];
        var select = $("#copy_" + key);
        var span = select.next();
        // Add the options
        var selectedOpt = undefined;
        select.empty();
        for (var j=0; j<arrays.length; j++) {
            var option = $('<option>');
            option.val(arrays[j].name);
            option.text(arrays[j].name);
            select.append(option);

            // Eventually mark the option as selected
            if (arrays[j].name === tool.data["copy"][key]) {
                selectedOpt = arrays[j];
            }
        }
        // Select the appropiate option
        if (selectedOpt !== undefined) {
            select.val(selectedOpt.name);
            try {
                katex.render(selectedOpt.symbol, span.get(0));
                span.html("(" + span.html() + ")");
                span.css("color", "white");
            } catch (e) { // (e if e instanceof katex.ParseError) {
                // Do nothing
            }
        }
        else {
            select.val("");
            span.css("color", "white");
            span.text("");
            tool.data["copy"][key] = undefined;
        }

        select.off('change.tabtool').on('change.tabtool', function(event) {
            var select = $(this);
            var span = select.next();
            var tool = __TABTOOL;
            // Store the option on the tool data structure
            var key = select.attr('id').replace("copy_", "");
            tool.data["copy"][key] = select.val();
            // Re-render
            setupCopyContent(tool);
        });
    }
}


function __toolMathRenderer(elem, txt, variable) {
    // Start adding a tooltip. We are disabling it later if errors are not found
    var root = elem.parent();
    root.css("color", "red");
    root.data('toggle', 'tooltip');
    root.data('placement', 'right');
    root.tooltip('enable');

    var eq;
    try {
        // Convert to Katex expression
        eq = tokenizer.toKatex(txt);
        katex.render(eq, elem.get(0));
        // Check the dependencies
        var deps = tokenizer.dependencies(txt);
        for (var i=0; i<deps.length; i++) {
            if (typeof deps[i] === "string") {
                root.prop('title',
                          '"' + deps[i] + '" variable is not defined');
                return;
            }
        }
    } catch (e) {
        console.log(e);
        root.prop('title', 'Invalid expression');
        return;
    }

    // If a variable has been provided, check the number of components
    if (variable !== undefined) {
        var eqs = tokenizer.split(txt);
        var n = typeToN(variable.type);
        if (eqs.length < n) {
            root.prop('title', 'Type "' + variable.type + '" requires at least ' + n.toString() + ' values');
            return;            
        }
    }

    // Everything OK!
    root.css("color", "white");
    root.prop('title', '');
    root.tooltip('disable');
}


function setupSetContent(tool) {
    // In array
    // ========
    var select = $("#set_in");
    var span = select.next();
    // Add the options
    var arrays = getArrayVariables();
    var selectedOpt = undefined;
    select.empty();
    for (var i=0; i<arrays.length; i++) {
        var option = $('<option>');
        option.val(arrays[i].name);
        option.text(arrays[i].name);
        select.append(option);

        // Eventually mark the option as selected
        if (arrays[i].name === tool.data["set"]["in"]) {
            selectedOpt = arrays[i];
        }
    }
    // Select the appropiate option
    if (selectedOpt !== undefined) {
        select.val(selectedOpt.name);
        try {
            katex.render(selectedOpt.symbol, span.get(0));
            span.html("(" + span.html() + ")");
            span.css("color", "white");
        } catch (e) { // (e if e instanceof katex.ParseError) {
            // Do nothing
        }
    }
    else {
        select.val("");
        span.css("color", "white");
        span.text("");
        tool.data["set"]["in"] = undefined;
    }

    select.off('change.tabtool').on('change.tabtool', function(event) {
        var select = $(this);
        var span = select.next();
        var tool = __TABTOOL;
        // Store the option on the tool data structure
        tool.data["set"]["in"] = select.val();
        // Re-render
        setupSetContent(tool);
    });

    // Output value
    // ============
    span = $('#set_value');
    span.text(tool.data["set"]["value"]);
    var updateSetValue = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        v = v.replace(';', ',');
        tool.data["set"]["value"] = v;

        // These tools are written as OpenCL code, so better not try to render
        // as math
        // __toolMathRenderer(elem, v, getVariable(tool.data["set"]["in"]));
        elem.text(v);
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["set"]["value"]);
    }).on('blur.tabtool', function() { 
        updateSetValue($(this)) });
    updateSetValue(span);
}

function updateScriptFiles() {
    var input = $("#python_path");
    input.editableSelect('clear');
    var fnames = Object.keys(PY_FILES);
    for (var i=0; i<fnames.length; i++) {
        input.editableSelect('add', fnames[i]);
    }
}

function resizePYEditor() {
    var editor = $('#pyeditor');
    var p = editor.prev();
    var div = editor.parent();
    var y = div.offset().top - 64 + p.outerHeight(true);
    editor.css('top', y.toString() + 'px');
}

function setupPythonContent(tool) {
    // Script path
    // ===========
    var input = $("#python_path");
    if (input.prop('nodeName') === "SELECT") {
        input.editableSelect({ filter:false });
        input = $("#python_path");
    }

    updateScriptFiles();
    input.val(tool.data["python"]["path"]);

    function pythonPathChange() {
        var input = $(this);
        var fname = $(this).val();
        var tool = __TABTOOL;
        if (fname === "") {
            // Bad name, restore the previous one
            input.val(tool.data["python"]["path"]);
            return;
        }

        if (tool.data["python"]["enclosed_file"]) {
            // Avoid routes
            if (fname.lastIndexOf("/") !== -1) {
                fname = fname.substring(fname.lastIndexOf("/") + 1);
            }
            // Remove special chars
            fname = encodeURIComponent(fname);
            if (fname === "") {
                // Unhandeable, restore previous name
                input.val(tool.data["python"]["path"]);
                return;
            }
            input.val(fname);

            if (PY_FILES[tool.data["python"]["path"]] !== undefined) {
                // We are migrating, decrease its reference
                PY_FILES[tool.data["python"]["path"]].refs--;
            }
            if (PY_FILES[fname] === undefined) {
                // Duplicate the file if possible, start a new empty file
                // otherwise
                var txt = "";
                if (PY_FILES[tool.data["python"]["path"]] !== undefined) {
                    txt = PY_FILES[tool.data["python"]["path"]].txt;
                }
                PY_FILES[fname] = {refs:0, txt:txt};
                // Add it to the options
                input.editableSelect('add', fname);
            }
            PY_FILES[fname].refs++;
            if ((PY_FILES[tool.data["python"]["path"]] !== undefined) &&
                (PY_FILES[tool.data["python"]["path"]].refs === 0)) {
                // Unused element, time to remove it!
                delete PY_FILES[tool.data["python"]["path"]];
            }
            updateScriptFiles();

            // Show the editor (loading the appropiate session)
            var editor = $('#pyeditor');
            editor.show();
            // At this point, the file should be ready
            var session = PY_FILES[fname].session;
            if (session === undefined) {
                session = new ace.EditSession(
                    PY_FILES[fname].txt);
                session.setMode("ace/mode/python");
                PY_FILES[fname].session = session;
            }
            __PYEDITOR.setSession(session);
            resizePYEditor();
        }
        
        tool.data["python"]["path"] = fname;
    }
    input.off('select.editable-select').off('change.tabtool');
    input.on('select.editable-select', pythonPathChange);
    input.on('change.tabtool', pythonPathChange);

    // Enclosed file
    // =============
    input = $("#python_enclosed_file");
    input.off('change.tabtool').on('change.tabtool', function() {
        var checked = $(this).prop('checked');
        var tool = __TABTOOL;
        tool.data["python"]["enclosed_file"] = checked;

        // Create/remove enclosed files
        if ((!checked) &&
            (PY_FILES[tool.data["python"]["path"]] !== undefined)) {
            // Decrease the file reference (and eventually remove it)
            PY_FILES[tool.data["python"]["path"]].refs--;
            if (PY_FILES[tool.data["python"]["path"]].refs === 0) {
                delete PY_FILES[tool.data["python"]["path"]];
                updateScriptFiles();
            }
        }
        $("#python_path").trigger('change.tabtool');

        // Show/hide the editor
        var editor = $('#pyeditor');
        if (checked) {
            editor.show();
            // At this point, the file should be ready
            var session = PY_FILES[tool.data["python"]["path"]].session;
            if (session === undefined) {
                session = new ace.EditSession(
                    PY_FILES[tool.data["python"]["path"]].txt);
                session.setMode("ace/mode/python");
                PY_FILES[tool.data["python"]["path"]].session = session;
            }
            __PYEDITOR.setSession(session);
            resizePYEditor();
        } else {
            editor.hide();
        }
    });
    input.prop('checked', tool.data["python"]["enclosed_file"]);
    input.trigger('change.tabtool');
}


function setupSetScalarContent(tool) {
    // In array
    // ========
    var select = $("#set_scalar_in");
    var span = select.next();
    // Add the options
    var arrays = getScalarVariables();
    var selectedOpt = undefined;
    select.empty();
    for (var i=0; i<arrays.length; i++) {
        var option = $('<option>');
        option.val(arrays[i].name);
        option.text(arrays[i].name);
        select.append(option);

        // Eventually mark the option as selected
        if (arrays[i].name === tool.data["set_scalar"]["in"]) {
            selectedOpt = arrays[i];
        }
    }
    // Select the appropiate option
    if (selectedOpt !== undefined) {
        select.val(selectedOpt.name);
        try {
            katex.render(selectedOpt.symbol, span.get(0));
            span.html("(" + span.html() + ")");
            span.css("color", "white");
        } catch (e) { // (e if e instanceof katex.ParseError) {
            // Do nothing
        }
    }
    else {
        select.val("");
        span.css("color", "white");
        span.text("");
        tool.data["set_scalar"]["in"] = undefined;
    }

    select.off('change.tabtool').on('change.tabtool', function(event) {
        var select = $(this);
        var span = select.next();
        var tool = __TABTOOL;
        // Store the option on the tool data structure
        tool.data["set_scalar"]["in"] = select.val();
        // Re-render
        setupSetScalarContent(tool);
    });

    // Output value
    // ============
    span = $('#set_scalar_value');
    span.text(tool.data["set_scalar"]["value"]);
    var updateSetScalarValue = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        v = v.replace(';', ',');
        tool.data["set_scalar"]["value"] = v;

        __toolMathRenderer(elem, v, getVariable(tool.data["set_scalar"]["in"]));
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["set_scalar"]["value"]);
    }).on('blur.tabtool', function() { 
        updateSetScalarValue($(this)) });
    updateSetScalarValue(span);
}


function setupReductionContent(tool) {
    // In array
    // ========
    var select = $("#reduction_in");
    var span = select.next();
    // Add the options
    var arrays = getArrayVariables();
    var selectedOpt = undefined;
    select.empty();
    for (var i=0; i<arrays.length; i++) {
        var option = $('<option>');
        option.val(arrays[i].name);
        option.text(arrays[i].name);
        select.append(option);

        // Eventually mark the option as selected
        if (arrays[i].name === tool.data["reduction"]["in"]) {
            selectedOpt = arrays[i];
        }
    }
    // Select the appropiate option
    if (selectedOpt !== undefined) {
        select.val(selectedOpt.name);
        try {
            katex.render(selectedOpt.symbol, span.get(0));
            span.html("(" + span.html() + ")");
            span.css("color", "white");
        } catch (e) { // (e if e instanceof katex.ParseError) {
            // Do nothing
        }
    }
    else {
        select.val("");
        span.css("color", "white");
        span.text("");
        tool.data["reduction"]["in"] = undefined;
    }

    select.off('change.tabtool').on('change.tabtool', function(event) {
        var select = $(this);
        var span = select.next();
        var tool = __TABTOOL;
        // Store the option on the tool data structure
        tool.data["reduction"]["in"] = select.val();
        // Re-render
        setupReductionContent(tool);
    });

    // Out array
    // ========
    var select = $("#reduction_out");
    var span = select.next();
    // Add the options
    var arrays = getScalarVariables();
    var selectedOpt = undefined;
    select.empty();
    for (var i=0; i<arrays.length; i++) {
        var option = $('<option>');
        option.val(arrays[i].name);
        option.text(arrays[i].name);
        select.append(option);

        // Eventually mark the option as selected
        if (arrays[i].name === tool.data["reduction"]["out"]) {
            selectedOpt = arrays[i];
        }
    }
    // Select the appropiate option
    if (selectedOpt !== undefined) {
        select.val(selectedOpt.name);
        try {
            katex.render(selectedOpt.symbol, span.get(0));
            span.html("(" + span.html() + ")");
            span.css("color", "white");
        } catch (e) { // (e if e instanceof katex.ParseError) {
            // Do nothing
        }
    }
    else {
        select.val("");
        span.css("color", "white");
        span.text("");
        tool.data["reduction"]["out"] = undefined;
    }

    select.off('change.tabtool').on('change.tabtool', function(event) {
        var select = $(this);
        var span = select.next();
        var tool = __TABTOOL;
        // Store the option on the tool data structure
        tool.data["reduction"]["out"] = select.val();
        // Re-render
        setupReductionContent(tool);
    });

    // Null value
    // ============
    span = $('#reduction_null');
    span.text(tool.data["reduction"]["null"]);
    var updateReductionNull = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        v = v.replace(';', ',');
        tool.data["reduction"]["null"] = v;
        elem.text(v);
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["reduction"]["null"]);
    }).on('blur.tabtool', function() { 
        updateReductionNull($(this)) });
    updateReductionNull(span);

    // Text value
    // ==========
    span = $('#reduction_text');
    span.text(tool.data["reduction"]["text"]);
    var updateReductionText = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        tool.data["reduction"]["text"] = v;
        elem.text(v);
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["reduction"]["text"]);
    }).on('blur.tabtool', function() { 
        updateReductionText($(this)) });
    updateReductionText(span);
}

function setupLinkListContent(tool) {
    // In array
    // ========
    var select = $("#link-list_in");
    var span = select.next();
    // Add the options
    var arrays = getArrayVariables();
    var selectedOpt = undefined;
    select.empty();
    for (var i=0; i<arrays.length; i++) {
        var option = $('<option>');
        option.val(arrays[i].name);
        option.text(arrays[i].name);
        select.append(option);

        // Eventually mark the option as selected
        if (arrays[i].name === tool.data["link-list"]["in"]) {
            selectedOpt = arrays[i];
        }
    }
    // Select the appropiate option
    if (selectedOpt !== undefined) {
        select.val(selectedOpt.name);
        try {
            katex.render(selectedOpt.symbol, span.get(0));
            span.html("(" + span.html() + ")");
            span.css("color", "white");
        } catch (e) { // (e if e instanceof katex.ParseError) {
            // Do nothing
        }
    }
    else {
        select.val("");
        span.css("color", "white");
        span.text("");
        tool.data["link-list"]["in"] = undefined;
    }

    select.off('change.tabtool').on('change.tabtool', function(event) {
        var select = $(this);
        var span = select.next();
        var tool = __TABTOOL;
        // Store the option on the tool data structure
        tool.data["link-list"]["in"] = select.val();
        // Re-render
        setupLinkListContent(tool);
    });
}


function setupRadixSortContent(tool) {
    var keys = ["in", "perm", "inv_perm"];
    for (k=0; k<keys.length; k++) {
        var key = keys[k];
        var select = $("#radix-sort_" + key);
        var span = select.next();
        // Add the options
        var arrays = getArrayVariables();
        var selectedOpt = undefined;
        select.empty();
        for (var i=0; i<arrays.length; i++) {
            var option = $('<option>');
            option.val(arrays[i].name);
            option.text(arrays[i].name);
            select.append(option);

            // Eventually mark the option as selected
            if (arrays[i].name === tool.data["radix-sort"][key]) {
                selectedOpt = arrays[i];
            }
        }
        // Select the appropiate option
        if (selectedOpt !== undefined) {
            select.val(selectedOpt.name);
            try {
                katex.render(selectedOpt.symbol, span.get(0));
                span.html("(" + span.html() + ")");
                span.css("color", "white");
            } catch (e) { // (e if e instanceof katex.ParseError) {
                // Do nothing
            }
        }
        else {
            select.val("");
            span.css("color", "white");
            span.text("");
            tool.data["radix-sort"][key] = undefined;
        }

        select.off('change.tabtool').on('change.tabtool', function(event) {
            var select = $(this);
            var span = select.next();
            var tool = __TABTOOL;
            // Store the option on the tool data structure
            tool.data["radix-sort"][key] = select.val();
            // Re-render
            setupRadixSortContent(tool);
        });
    }
}


function setupAssertContent(tool) {
    var span = $('#assert_condition');
    span.text(tool.data["assert"]["condition"]);
    var updateAssertCondition = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        tool.data["assert"]["condition"] = v;
        elem.text(v);
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["assert"]["condition"]);
    }).on('blur.tabtool', function() { 
        updateAssertCondition($(this)) });
    updateAssertCondition(span);
}


function setupIfContent(tool) {
    var span = $('#if_condition');
    span.text(tool.data["if"]["condition"]);
    var updateIfCondition = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        tool.data["if"]["condition"] = v;
        elem.text(v);
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["if"]["condition"]);
    }).on('blur.tabtool', function() { 
        updateIfCondition($(this)) });
    updateIfCondition(span);
}


function setupWhileContent(tool) {
    var span = $('#while_condition');
    span.text(tool.data["while"]["condition"]);
    var updateWhileCondition = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        tool.data["while"]["condition"] = v;
        elem.text(v);
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["while"]["condition"]);
    }).on('blur.tabtool', function() { 
        updateWhileCondition($(this)) });
    updateWhileCondition(span);
}


function setupEndContent(tool) {
}


function setupMpiSyncContent(tool) {
    // Mask var
    // ========
    var select = $("#mpi-sync_mask");
    var span = select.next();
    // Add the options
    var arrays = getArrayVariables();
    var selectedOpt = undefined;
    select.empty();
    for (var i=0; i<arrays.length; i++) {
        if(arrays[i].type !== "unsigned int*") {
            continue;
        }
        var option = $('<option>');
        option.val(arrays[i].name);
        option.text(arrays[i].name);
        select.append(option);

        // Eventually mark the option as selected
        if (arrays[i].name === tool.data["mpi-sync"]["mask"]) {
            selectedOpt = arrays[i];
        }
    }
    // Select the appropiate option
    if (selectedOpt !== undefined) {
        select.val(selectedOpt.name);
        try {
            katex.render(selectedOpt.symbol, span.get(0));
            span.html("(" + span.html() + ")");
            span.css("color", "white");
        } catch (e) { // (e if e instanceof katex.ParseError) {
            // Do nothing
        }
    }
    else {
        select.val("");
        span.css("color", "white");
        span.text("");
        tool.data["mpi-sync"]["mask"] = undefined;
    }

    select.off('change.tabtool').on('change.tabtool', function(event) {
        var select = $(this);
        var span = select.next();
        var tool = __TABTOOL;
        // Store the option on the tool data structure
        tool.data["mpi-sync"]["mask"] = select.val();
        // Re-render
        setupMpiSyncContent(tool);
    });
    
    var span = $('#mpi-sync_fields');
    span.text(tool.data["mpi-sync"]["fields"]);
    var updateFields = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        tool.data["mpi-sync"]["fields"] = v;
        elem.text(v);
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["mpi-sync"]["fields"]);
    }).on('blur.tabtool', function() { 
        updateFields($(this)) });

    var span = $('#mpi-sync_processes');
    span.text(tool.data["mpi-sync"]["processes"]);
    var updateProcs = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        tool.data["mpi-sync"]["processes"] = v;
        elem.text(v);
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["mpi-sync"]["processes"]);
    }).on('blur.tabtool', function() { 
        updateProcs($(this)) });
}


function setupReportScreenContent(tool) {
    var span = $('#report_screen_fields');
    span.text(tool.data["report_screen"]["fields"]);
    var updateFields = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        tool.data["report_screen"]["fields"] = v;
        elem.text(v);
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["report_screen"]["fields"]);
    }).on('blur.tabtool', function() { 
        updateFields($(this)) });

    var select = $("#report_screen_color");
    var colors = ["white", "green", "blue", "yellow", "red", "magenta", "cyan"];
    var selectedOpt = undefined;
    select.empty();
    for (var i=0; i<colors.length; i++) {
        var option = $('<option>');
        option.val(colors[i]);
        option.text(colors[i]);
        select.append(option);

        if (colors[i] === tool.data["report_screen"]["color"]) {
            selectedOpt = colors[i];
        }
    }
    if (selectedOpt !== undefined) {
        select.val(selectedOpt);
    }
    select.off('change.tabtool').on('change.tabtool', function(event) {
        var select = $(this);
        var tool = __TABTOOL;
        // Store the option on the tool data structure
        tool.data["report_screen"]["color"] = select.val();
    });

    var input = $("#report_screen_bold");
    input.off('change.tabtool').on('change.tabtool', function() {
        var checked = $(this).prop('checked');
        var tool = __TABTOOL;
        tool.data["report_screen"]["bold"] = checked.toString();
    });
    input.prop('checked', parseBool(tool.data["report_screen"]["bold"]));
    input.trigger('change.tabtool');
}


function setupReportFileContent(tool) {
    var span = $('#report_file_fields');
    span.text(tool.data["report_file"]["fields"]);
    var updateFields = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        tool.data["report_file"]["fields"] = v;
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["report_file"]["fields"]);
    }).on('blur.tabtool', function() { 
        updateFields($(this)) });

    var span = $('#report_file_path');
    span.text(tool.data["report_file"]["path"]);
    var updatePath = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        tool.data["report_file"]["path"] = v;
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["report_file"]["path"]);
    }).on('blur.tabtool', function() { 
        updatePath($(this)) });
}


function setupReportParticlesContent(tool) {
    var select = $("#report_particles_set");
    var selectedOpt = undefined;
    select.empty();
    for (var i=0; i<SETS.length; i++) {
        var option = $('<option>');
        option.val(i.toString());
        option.text(i.toString());
        select.append(option);

        if (i === tool.data["report_particles"]["set"]) {
            selectedOpt = i.toString();
        }
    }
    if (selectedOpt !== undefined) {
        select.val(selectedOpt);
    }
    select.off('change.tabtool').on('change.tabtool', function(event) {
        var select = $(this);
        var tool = __TABTOOL;
        // Store the option on the tool data structure
        var set = parseInt(select.val());
        if (isNaN(set) || (set < 0) || (set >= SETS.length)){
            set = 0;
        }
        tool.data["report_particles"]["set"] = set.toString();
        select.val(set.toString());
    });
    select.trigger('change.tabtool');

    var span = $('#report_particles_fields');
    span.text(tool.data["report_particles"]["fields"]);
    var updateFields = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        tool.data["report_particles"]["fields"] = v;
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["report_particles"]["fields"]);
    }).on('blur.tabtool', function() { 
        updateFields($(this)) });

    var span = $('#report_particles_path');
    span.text(tool.data["report_particles"]["path"]);
    var updatePath = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        tool.data["report_particles"]["path"] = v;
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["report_particles"]["path"]);
    }).on('blur.tabtool', function() { 
        updatePath($(this)) });

    var span = $('#report_particles_fps');
    span.text(tool.data["report_particles"]["fps"]);
    var updateFPS = function(elem) {
        var tool = __TABTOOL;
        var v = parseFloat(elem.text());
        if (isNaN(v) || v < 0.0) {
            v = 0.0;
        }
        tool.data["report_particles"]["fps"] = v.toString();
        elem.text(tool.data["report_particles"]["fps"]);
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["report_particles"]["fps"]);
    }).on('blur.tabtool', function() { 
        updateFPS($(this)) });

    var span = $('#report_particles_ipf');
    span.text(tool.data["report_particles"]["ipf"]);
    var updateIPF = function(elem) {
        var tool = __TABTOOL;
        var v = parseInt(elem.text());
        if (isNaN(v) || v < 0) {
            v = 0;
        }
        tool.data["report_particles"]["ipf"] = v.toString();
        elem.text(tool.data["report_particles"]["ipf"]);
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["report_particles"]["ipf"]);
    }).on('blur.tabtool', function() { 
        updateIPF($(this)) });
}


function setupReportPerformanceContent(tool) {
    var select = $("#report_performance_color");
    var colors = ["white", "green", "blue", "yellow", "red", "magenta", "cyan"];
    var selectedOpt = undefined;
    select.empty();
    for (var i=0; i<colors.length; i++) {
        var option = $('<option>');
        option.val(colors[i]);
        option.text(colors[i]);
        select.append(option);

        if (colors[i] === tool.data["report_performance"]["color"]) {
            selectedOpt = colors[i];
        }
    }
    if (selectedOpt !== undefined) {
        select.val(selectedOpt);
    }
    select.off('change.tabtool').on('change.tabtool', function(event) {
        var select = $(this);
        var tool = __TABTOOL;
        // Store the option on the tool data structure
        tool.data["report_performance"]["color"] = select.val();
    });

    var input = $("#report_performance_bold");
    input.off('change.tabtool').on('change.tabtool', function() {
        var checked = $(this).prop('checked');
        var tool = __TABTOOL;
        tool.data["report_performance"]["bold"] = checked.toString();
    });
    input.prop('checked', parseBool(tool.data["report_performance"]["bold"]));
    input.trigger('change.tabtool');

    var span = $('#report_performance_path');
    span.text(tool.data["report_performance"]["path"]);
    var updatePath = function(elem) {
        var tool = __TABTOOL;
        var v = elem.text();
        tool.data["report_performance"]["path"] = v;
    }
    span.off('focus.tabtool').off('blur.tabtool');
    span.on('focus.tabtool', function() {
        var tool = __TABTOOL;
        $(this).text(tool.data["report_performance"]["path"]);
    }).on('blur.tabtool', function() { 
        updatePath($(this)) });
}


/** Create the tool edition tab.
 * @param tool Tool associated to the tab.
 * @return The tab button, to can easily open it
 * @note It the tool already has a tab, this function is returning its id,
 * without generating a new one.
 */
function createToolTab(tool) {
    $("#cleditor").ready(resizeCLEditor);
    $("#pyeditor").ready(resizePYEditor);

    // Set the button
    var button = __TOOLTAB;
    if (button === undefined) {
        showAlltabs();  // Better we show all the tabs before manipulating them
        var tabsbar = $("#tabsBar");
        var button = $('<button class="tablinks">');
        button.attr("id", "ToolTab");
        button.data('tabid', "Tool");
        button.on('click', function (event) {
            openTab(event, $(this).data('tabid'));
        });
        tabsbar.append(button);
    }
    button.text(tool.name);
    resizeTabContent();

    __TOOLTAB = button;
    __TABTOOL = tool;

    // Tool name
    var title = $("#ToolName");
    title.blur(function() {
        var txt = $(this).text();
        __TABTOOL.name = txt;
        __TOOLTAB.text(txt);
    });
    title.text(tool.name);

    // Tool type
    var type = $("#ToolTypeSel");
    type.off('change.tabtool');
    type.on('change.tabtool', function(e) {
        if (__TABTOOL instanceof Group) {
            // Groups type cannot be changed!
            $(this).val(__TABTOOL.type);
        }
        else {
            __TABTOOL.type = $(this).val();
        }
        // Hide all the subcontents
        $('#Tool').children('div').hide();
        // Show the selected one
        $('#' + $(this).val()).show();
        // Resize the code editors
        resizeCLEditor();
        resizePYEditor();
    });
    type.val(tool.type);
    type.trigger('change.tabtool');  // Update the content

    // Run once
    input = $("#ToolOnce");
    input.off('change.tabtool').on('change.tabtool', function() {
        if (__TABTOOL instanceof Group) {
            // Groups type cannot be changed!
            $(this).prop('checked', __TABTOOL.once);
            return;
        }
        var checked = $(this).prop('checked');
        __TABTOOL.once = checked;
    });
    input.prop('checked', __TABTOOL.once);
    input.trigger('change.tabtool');

    // subcontent sections
    setupKernelContent(tool);
    setupCopyContent(tool);
    setupSetContent(tool);
    setupPythonContent(tool);
    setupSetScalarContent(tool);
    setupReductionContent(tool);
    setupLinkListContent(tool);
    setupRadixSortContent(tool);
    setupAssertContent(tool);
    setupIfContent(tool);
    setupWhileContent(tool);
    setupEndContent(tool);
    setupMpiSyncContent(tool);
    setupReportScreenContent(tool);
    setupReportFileContent(tool);
    setupReportParticlesContent(tool);
    setupReportPerformanceContent(tool);

    return button;
}

/** Close the tool edition tab.
 * @param tool Tool associated to the tab.
 * @return true if the tab has been closed, false otherwise
 * @note The tab is only closed if the tool match with the currently becoming
 * open.
 */
function closeToolTab(tool) {
    if (__TABTOOL !== tool) {
        return false;
    }

    showAlltabs();  // Better we show all the tabs before manipulating them
    __TOOLTAB.remove();
    __TOOLTAB = undefined;
    __TABTOOL = undefined;
    resizeTabContent();
    return true;
}
