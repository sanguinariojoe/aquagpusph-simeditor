var SETS = [];

function getSetVariables() {
    var variables = [];
    for (var i=0; i<VARIABLES.length; i++) {
        if (VARIABLES[i].type.endsWith('*') && (VARIABLES[i].length === "n_sets")) {
            variables.push(VARIABLES[i]);
        }
    }
    return variables;
}

function getParticleSetByElement(element) {
    for (var i=0; i<SETS.length; i++) {
        if (SETS[i]._entry.get(0) === element.get(0)) {
            return SETS[i];
        }
    }
    return undefined;
}


function __setSpanInt(editable, txt, attrName) {
    /* Create a span tag for integer values, which can be eventually edited
     * @param editable true if the user may edit the text that by clicking on
     * it, false otherwise.
     * @param txt Text inside the span section
     * @param attrName Reference to the attribute
     */
    var span = $('<span contenteditable=' + editable.toString() + '>');
    span.data('attribute', attrName);
    var val = parseInt(txt);
    span.text(parseInt(txt).toString());

    span.blur(function() {
        var i;
        var vars = getSetVariables();
        var val = parseInt($(this).text());
        // Update
        var set = getParticleSetByElement($(this).parent().parent());
        set[$(this).data('attribute')] = val.toString();
    });

    return span;
}

function __setSpanText(editable, txt, attrName) {
    /* Create a span tag for text, which can be eventually edited
     * @param editable true if the user may edit the text that by clicking on
     * it, false otherwise.
     * @param txt Text inside the span section
     * @param attrName Reference to the attribute
     */
    var span = $('<span contenteditable=' + editable.toString() + '>');
    span.data('attribute', attrName);
    span.text(txt);

    span.blur(function() {
        var txt = $(this).text();
        // Update
        var set = getParticleSetByElement($(this).parent().parent());
        set[$(this).data('attribute')] = txt;
    });

    return span;
}

function __setSpanMath(editable, txt, attrName) {
    /* Create a span tag for math equation, which can be eventually edited
     * @param editable true if the user may edit the text that by clicking on
     * it, false otherwise.
     * @param txt Text inside the span section
     */
    var span = $('<span contenteditable=' + editable.toString() + '>');
    span.text(txt);
    span.data('attribute', attrName);

    span.focus(function() {
        // Replace the Katex equation by the text
        var set = getParticleSetByElement($(this).parent().parent());
        // Set variables are quite complex objects, because setter and getter
        // cannot be created as object properties (since they have not a
        // constant name), requiring a closure
        // see https://stackoverflow.com/questions/19531845/can-js-have-getters-and-setters-methods-named-same-as-property?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
        // Such closure is enveloped in an object (see updateSetVariables).
        // So, depending on wether it is a getter/setter, or a complex closure,
        // different methods shall be applied
        var txt;
        if (typeof set[$(this).data('attribute')] === "object") {
            txt = set[$(this).data('attribute')].get();
        } else {
            txt = set[$(this).data('attribute')];
        }
        $(this).text(txt);
    }).blur(function() {
        var txt = $(this).text();
        var set = getParticleSetByElement($(this).parent().parent());
        if (typeof set[$(this).data('attribute')] === "object") {
            set[$(this).data('attribute')].set(txt);
        } else {
            set[$(this).data('attribute')] = txt;
        }
    });

    return span;
}

function addParticlesSet() {
    var tr = $('<tr>');
    tr.css('border-bottom', '1px dashed white')
    var set = {_n:"1",
               _inFile:'input.vtk',
               _inFields:'r',
               _outFile:'output.vtk',
               _outFields:'r',
               _variables:{},
               _entry:tr};
    SETS.push(set);

    if($('#setsTable').children().length == 0) {
        $('#setsTable tr:last').append(tr);
    }
    else {
        $('#setsTable tr:last').after(tr);
    }

    var td;
    td = $('<td>'); tr.append(td);
    td.append(__setSpanInt(true, set._n, "n"));
    td = $('<td>'); tr.append(td);
    td.append(__setSpanText(true, set._inFile, "inFile"));
    td.append($('<br>'));
    td.append(__setSpanMath(true, set._inFields, "inFields"));
    td = $('<td>'); tr.append(td);
    td.append(__setSpanText(true, set._outFile, "outFile"));
    td.append($('<br>'));
    td.append(__setSpanMath(true, set._outFields, "outFields"));
    td = $('<td>'); tr.append(td);
    td = $('<td>'); tr.append(td);
    var remove = $('<img src="imgs/remove.svg" width="16px">');
    td.append(remove);
    remove.on('click', function() {
        // Get the table row and the set
        var tr = $(this).parent().parent();
        var set = getParticleSetByElement(tr);
        // Remove the set and the table row
        var i = SETS.indexOf(set);
        SETS.splice(i, 1);
        tr.remove();
        // The variables would be affected, so call to update everything
        updateSetsTable();
    });

    Object.defineProperty(set, 'n', {
        get: function() {
            return this._n;
        },
        set: function(n) {
            this._n = parseInt(n).toString();
            // Update the representation
            $(this._entry.children()[0]).children(":first").text(this._n);
            // Check for errors
            checkSetsN();
        }
    });
    Object.defineProperty(set, 'inFile', {
        get: function() {
            return this._inFile;
        },
        set: function(f) {
            this._inFile = f.trim();
            // Update the representation
            var element = $(set._entry.children()[1]);
            var span = element.children(':first');
            span.text(this._inFile);
            // Check for errors
            checkSetsFiles();
        }
    });
    Object.defineProperty(set, 'inFields', {
        get: function() {
            return this._inFields;
        },
        set: function(v) {
            v = v.replace(';', ',');

            this._inFields = v;
            var element = $(this._entry.children()[1]);
            var span = element.children(':last');

            // Convert to Katex expression
            var eq;
            try {
                eq = tokenizer.toKatex(v);
                katex.render(eq, span.get(0));
            } catch (e) {
                console.log(e);
                span.css("color", "red");
                var txt = span.text();
                if (txt !== v) {
                    span.text(v);
                }
            }
            checkSetsFields();
        }
    });
    Object.defineProperty(set, 'outFile', {
        get: function() {
            return this._outFile;
        },
        set: function(f) {
            this._outFile = f.trim();
            // Update the representation
            var element = $(set._entry.children()[2]);
            var span = element.children(':first');
            span.text(this._outFile);
            // Check for errors
            checkSetsFiles();
        }
    });
    Object.defineProperty(set, 'outFields', {
        get: function() {
            return this._outFields;
        },
        set: function(v) {
            v = v.replace(';', ',');

            this._outFields = v;
            var element = $(this._entry.children()[2]);
            var span = element.children(':last');

            // Convert to Katex expression
            var eq;
            try {
                eq = tokenizer.toKatex(v);
                katex.render(eq, span.get(0));
            } catch (e) {
                console.log(e);
                span.css("color", "red");
                var txt = span.text();
                if (txt !== v) {
                    span.text(v);
                }
            }
            checkSetsFields();
        }
    });

    // Enforce rendering length and value
    set.inFields = set._inFields;
    set.outFields = set._outFields;
    updateSetsTable();

    if (__TABTOOL !== undefined) {
        var tool = __TABTOOL;
        closeToolTab(tool);
        createToolTab(tool);
    }
    return set;
}

function updateNSets() {
    var variable = getVariable('n_sets');
    variable.value = SETS.length.toString();
}

function checkSetsN() {
    /** Check for errors in the sets names.
     */
    var N = 0;
    for (var i=0; i<SETS.length; i++) {
        var set = SETS[i];
        var element = $(set._entry.children()[0]);
        var span = element.children(':first');
        var n = parseInt(set.n);
        if (isNaN(n)) {
            element.css("color", "red");
            span.data('toggle', 'tooltip');
            span.data('placement', 'right');
            span.prop('title', 'Invalid number');
            span.tooltip('enable');
            continue;
        }
        if (n <= 0) {
            element.css("color", "red");
            span.data('toggle', 'tooltip');
            span.data('placement', 'right');
            span.prop('title', 'Positive number is required');
            span.tooltip('enable');
            continue;
        }
        span.prop('title', '');
        span.tooltip('disable');
        element.css("color", "white");
        N += n;
    }
    var variable = getVariable('N');
    variable.value = N.toString();
}

function checkSetsFiles() {
    for (var i=0; i<SETS.length; i++) {
        var set = SETS[i];
        var attrs = ['inFile', 'outFile'];
        for (var j=0; j<attrs.length; j++) {
            var element = $(set._entry.children()[j + 1]);
            var span = element.children(':first');
            if (set[attrs[j]].endsWith('.dat') || set[attrs[j]].endsWith('.vtk') || set[attrs[j]].endsWith('.vtu')) {
                span.prop('title', '');
                span.tooltip('disable');
                span.css("color", "white");
                continue;
            }
            span.css("color", "red");
            span.data('toggle', 'tooltip');
            span.data('placement', 'right');
            span.prop('title', 'Just .dat|.vtu files are accepted');
            span.tooltip('enable');            
        }
    }
}

function checkSetFieldsExpression(expr) {
    /** Check for errors in a fields list
     * @param expr Expression to traverse
     * @return undefined if no errors are detected, or the error description
     * otherwise.
     */
    var title = undefined;
    try {
        if (!tokenizer.isVariable(expr)) {
            return 'Just a list of variables is accepted';
        }
        var deps = tokenizer.dependencies(expr);
        for (var i=0; i<deps.length; i++) {
            var dep = deps[i];
            if (typeof dep === "string") {
                title = '"' + dep + '" variable is not defined';
                break;
            }
            var variable = VARIABLES[dep];
            if (!variable.type.endsWith('*')) {
                title = '"' + variable.name + '" is a scalar variable';
                break;                
            }
            if (variable.length !== 'N') {
                title = '"' + variable.name + '" has not "N" length';
                break;                
            }
        }
    } catch(e) {
        title = 'Invalid expression';
    }
    return title;
}


function checkSetsFields() {
    for (var i=0; i<SETS.length; i++) {
        var set = SETS[i];
        var attrs = ['inFields', 'outFields'];
        for (var j=0; j<attrs.length; j++) {
            var element = $(set._entry.children()[j + 1]);
            var span = element.children(':last');
            var title = checkSetFieldsExpression(set[attrs[j]]);
            if (title !== undefined) {
                span.css("color", "red");
                span.data('toggle', 'tooltip');
                span.data('placement', 'right');
                span.prop('title', title);
                span.tooltip('enable');
                continue;
            }

            span.prop('title', '');
            span.tooltip('disable');
            span.css("color", "white");
        }
    }
}

function updateSetVariables() {
    var setVars = getSetVariables();
    var setVarNames = [];
    for (var i=0; i<setVars.length; i++) {
        setVarNames.push(setVars[i].name);
    }
    for (var i=0; i<SETS.length; i++) {
        var set = SETS[i];
        var element = $(set._entry.children()[3]);
        // Remove the variables not existing anymore
        var varNames;
        try {
            varNames = Object.keys(set._variables);
        } catch(e) {
            varNames = [];
        }
        for (var j=0; j<varNames.length; j++) {
            if (setVarNames.indexOf(varNames[j]) === -1) {
                // Get the span and remove it
                var spans = element.children('span');
                for (var k=0; k<spans.length; k++) {
                    if ($(spans[k]).data('attribute') === 'var_' + varNames[j]) {
                        $(spans[k]).next().remove();  // <br>
                        $(spans[k]).remove();
                    }
                }
                // Remove the variable from the list
                delete set._variables[varNames[j]];
            }
        }
        // Append the new variables
        var varNames;
        try {
            varNames = Object.keys(set._variables);
        } catch(e) {
            varNames = [];
        }
        for (var j=0; j<setVarNames.length; j++) {
            if (varNames.indexOf(setVarNames[j]) === -1) {
                // Append the variable to the list
                set._variables[setVarNames[j]] = "0";
                // Create a span object to handle it
                var span = __setSpanMath(true, "0", 'var_' + setVarNames[j]);
                element.append(span);
                element.append($('<br>'));
                set['var_' + setVarNames[j]] = {
                    span:span,
                    attrName:setVarNames[j],
                    obj: set,
                    get: function () {
                        return this.obj._variables[this.attrName];
                    },
                    set: function (v) {
                        v = v.replace(';', ',');
                        this.obj._variables[this.attrName] = v;

                        // Convert to Katex expression
                        var eq;
                        var variable = getVariable(this.attrName);
                        try {
                            eq = tokenizer.toKatex(variable.name + ' = ' + v);
                            katex.render(eq, this.span.get(0));
                        } catch (e) {
                            console.log(e);
                            this.span.css("color", "red");
                            var txt = this.span.text();
                            if (txt !== variable.name + ' = ' + v) {
                                this.span.text(variable.name + ' = ' + v);
                            }
                        }
                        checkSetVariables();
                    }
                }
            }
            // Enforce rendering
            set['var_' + setVarNames[j]].set(set._variables[setVarNames[j]]);
        }
    }
}

function checkSetVariableExpression(setIndex, varName) {
    /** Check for errors in a particular variable expression
     * @param setIndex Index of the particles set at the SETS array
     * @param varName The particles set constant variable name
     * @return undefined if no errors are detected, or the error description
     * otherwise.
     */
    var set = SETS[setIndex];

    var title = undefined;
    try {
        // It may happens that the variable is not ready yet, so better get
        // protected by the try/catch
        var eq = set['var_' + varName].get();
        var eqs = tokenizer.split(eq);
        var variable = getVariable(varName);
        if (typeToN(variable.type) > eqs.length) {
            title = variable.name + ' is of type ' + variable.type +
                    ', but just ' + eqs.length.toString() +
                    ' components have been provided';
        }
        var deps = tokenizer.dependencies(eq);
        for (var i=0; i<deps.length; i++) {
            var dep = deps[i];
            if (typeof dep === "string") {
                title = dep + ' variable is not defined';
                break;
            }
        }
    } catch(e) {
        title = 'Invalid expression';
    }
    return title;
}

function checkSetVariables() {
    var setVars = getSetVariables();
    for (var i=0; i<SETS.length; i++) {
        var set = SETS[i];
        var element = $(set._entry.children()[3]);
        for (var j=0; j<setVars.length; j++) {
            var varName = setVars[j].name;
            var title = checkSetVariableExpression(i, varName);
            var span;
            var spans = element.children('span');
            for (var k=0; k<spans.length; k++) {
                if ($(spans[k]).data('attribute') === 'var_' + varName) {
                    span = $(spans[k]);
                }
            }
            if(title !== undefined) {
                span.css("color", "red");
                span.data('toggle', 'tooltip');
                span.data('placement', 'right');
                span.prop('title', title);
                span.tooltip('enable');
                continue;
            }

            span.prop('title', '');
            span.tooltip('disable');
            span.css("color", "white");
        }
        
    }
}

function updateSetsTable() {
    /** Look for common errors, highlighting them and adding tooltips
     */
    updateNSets();
    checkSetsN();
    checkSetsFiles();
    checkSetsFields();
    updateSetVariables();
    checkSetVariables();
}

// Register the table errors check as loading callback
registerTabCallback('Sets', updateSetsTable);
