String.prototype.replaceAt=function(index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}

var varsSymbols = function (node, options) {
    if ((node.type === 'SymbolNode')) {
        var val = undefined;
        for (var i=0; i < VARIABLES.length; i++) {
            if (node.name === VARIABLES[i].name) {
                val = '{' + VARIABLES[i].symbol + '}';
            }
            else if (node.name.startsWith(VARIABLES[i].name)) {
                var n = typeToN(VARIABLES[i].type);
                var suffixes = ['x', 'y', 'z', 'w'];
                for (var j=0; j<n; j++) {
                    if (node.name === VARIABLES[i].name + '_' + suffixes[j]) {
                        val = '{' + VARIABLES[i].symbol + '}_' + suffixes[j];
                    }
                }
            }
        }
        return val;
    }
};

var tokenizer = {

split : function(eq) {
    /** Split a equation in components, using commas as separators. This
     * function already takes into account commas enclosed in parentheses.
     * @param eq Equation string to split.
     * @return Equation components.
     * @note Semicolons are directly considered as separators.
     * @note Empty components are allowed.
     */
    var parenthesis_counter = 0;
    for (var i=0; i < eq.length; i++){
        // We does not care about unbalanced parenthesis, muparser will do it
        if(eq[i] === '(')
            parenthesis_counter++;
        else if(eq[i] === ')')
            parenthesis_counter--;
        else if(eq[i] === ','){
            if(parenthesis_counter > 0){
                // It is enclosed in parentheses, skip it
                continue;
            }
            eq = eq.replaceAt(i,';');
        }
    }

    return eq.split(';');
},

toKatex : function(eq) {
    /** Convert a equation from muParser to Katex string to be compiled in a
     * fancy way.
     * @param eq Equation to be converted.
     * @return Equation in Katex compilable format.
     * @note Exceptions might be rised if math.js is unable to parse the
     * expression.
     */
    var eqs = this.split(eq);
    var katex = [];
    for (var i=0; i<eqs.length; i++){
        katex.push(math.parse(eqs[i]).toTex({handler: varsSymbols}));
    }
    katex = katex.toString();
    return katex;
},

isVariable : function(eq) {
    /** Analyze if a math expression is just a list of variables (or even a
     * single variable).
     * @param eq Math expression to traverse
     * @return true if the expression conatins just a list of variables, false
     * otherwise.
     */
    var eqs = this.split(eq);
    if (eqs.length > 1) {
        for (var i=0; i<eqs.length; i++) {
            if (!this.isVariable(eqs[i])) {
                return false;
            }
        }
        return true;
    }

    var complexExpr = false;
    var root = math.parse(eq);
    root.traverse(function (node, path, parent) {
        if (node.type !== 'SymbolNode') {
            complexExpr = true;
        }
    });
    return !complexExpr;
},

dependencies : function(eq) {
    /** Analyze an equation, and return the considered variables positions, or
     * the referenced name if the variable cannot be found.
     * @param eq Math expression to traverse
     * @return List of considered variable indexes, or names if they cannot be
     * found.
     */
    var deps = [];
    var eqs = this.split(eq);
    if (eqs.length > 1) {
        for (var i=0; i<eqs.length; i++) {
            deps = deps.concat(this.dependencies(eqs[i]));
        }
        return deps;
    }

    var names = [];
    var root = math.parse(eq);
    root.traverse(function (node, path, parent) {
        if (node.type !== 'SymbolNode') {
            return;
        }
        if (names.indexOf(node.name) != -1) {
            // Already parsed symbol
            return;
        }
        names.push(node.name);
        // Locate the variable, if possible
        var dep = node.name;
        for (var i=0; i < VARIABLES.length; i++) {
            if (node.name === VARIABLES[i].name) {
                dep = i;
                break;
            }
            else if (node.name.startsWith(VARIABLES[i].name)) {
                var n = typeToN(VARIABLES[i].type);
                var suffixes = ['x', 'y', 'z', 'w'];
                for (var j=0; j<n; j++) {
                    if (node.name === VARIABLES[i].name + '_' + suffixes[j]) {
                        dep = i;
                        break;
                    }
                }
                if (dep !== node.name) {
                    break;
                }
            }
        }
        deps.push(dep);
    });
    return deps;
},

}
