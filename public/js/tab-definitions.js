var DEFINITIONS = [];

function getDefinition(name) {
    for (var i=0; i<DEFINITIONS.length; i++) {
        if (DEFINITIONS[i].name === name) {
            return DEFINITIONS[i];
        }
    }
    return undefined;
}

function getDefinitionByElement(element) {
    for (var i=0; i<DEFINITIONS.length; i++) {
        if (DEFINITIONS[i]._entry.get(0) === element.get(0)) {
            return DEFINITIONS[i];
        }
    }
    return undefined;
}

function __defSpanText(editable, txt, attrName) {
    /* Create a span tag for text, which can be eventually edited
     * @param editable true if the user may edit the text that by clicking on
     * it, false otherwise.
     * @param txt Text inside the span section
     * @param attrName Reference to the attribute
     */
    var span = $('<span contenteditable=' + editable.toString() + '>');
    span.data('attribute', attrName);
    span.text(txt);

    span.blur(function() {
        var txt = $(this).text();
        var def = getDefinitionByElement($(this).parent().parent());
        def[$(this).data('attribute')] = txt;
    });

    return span;
}

function __defSpanMath(editable, txt, attrName) {
    /* Create a span tag for math equation, which can be eventually edited
     * @param editable true if the user may edit the text that by clicking on
     * it, false otherwise.
     * @param txt Text inside the span section
     * @param attrName Reference to the attribute
     */
    var span = $('<span contenteditable=' + editable.toString() + '>');
    span.data('attribute', attrName);
    span.text(txt);

    span.focus(function() {
        var def = getDefinitionByElement($(this).parent().parent());
        $(this).text(def[$(this).data('attribute')]);
    }).blur(function() {
        var txt = $(this).text();
        var def = getDefinitionByElement($(this).parent().parent());
        def[$(this).data('attribute')] = txt;
    });

    return span;
}

var defSpanCheckIndex = 0;

function __defSpanCheck(editable, checked, attrName) {
    /* Create a span tag for a checkbox
     * @param editable true if the user may check/uncheck, false otherwise.
     * @param txt Text inside the span section
     * @param attrName Reference to the attribute
     */
    var div = $('<div>');
    var span = $('<input type="checkbox">');
    span.prop('checked', checked);
    span.prop('disabled', !editable);
    span.prop('id', 'defSpanCheck' + defSpanCheckIndex.toString());
    span.data('attribute', attrName);
    span.addClass('radio-button');

    span.on('change', function() {
        var checked = $(this).prop('checked');
        var def = getDefinitionByElement($(this).parent().parent().parent());
        def[$(this).data('attribute')] = checked;
    });

    var label = $('<label for="' +
                  'defSpanCheck' + defSpanCheckIndex.toString() +
                  '">');
    label.append($('<span>'));
    label.css('line-height', '1');
    label.css('margin-bottom', '0');

    div.append(span);
    div.append(label);

    defSpanCheckIndex++;

    return div;
}

function addDefinition(name, value, evaluated) {
    if (name === undefined) {
        i = 0;
        while (getDefinition("DEF" + i.toString()) !== undefined) {
            i += 1;
        }
        name = "DEF" + i.toString();
    }
    if (value === undefined) {
        value = "0";
    }
    if (evaluated === undefined) {
        evaluated = false;
    }

    var tr = $('<tr>');
    var definition = {_name:name,
                      _value:value,
                      _evaluated:evaluated,
                      _entry:tr,
                      _removable:true};
    DEFINITIONS.push(definition);

    var td;
    td = $('<td>'); tr.append(td);
    td.append(__defSpanText(true, definition._name, "name"));
    td = $('<td>'); tr.append(td);
    td.append(__defSpanMath(true, definition._value, "value"));
    td = $('<td>'); tr.append(td);
    td.append(__defSpanCheck(true, definition._evaluated, "evaluated"));
    if($('#defsTable').children().length == 0) {
        $('#defsTable tr:last').append(tr);
    }
    else {
        $('#defsTable tr:last').after(tr);
    }
    td = $('<td>'); tr.append(td);
    var remove = $('<img src="imgs/remove.svg" width="16px">');
    td.append(remove);
    remove.on('click', function() {
        // Get the table row and the definition
        var tr = $(this).parent().parent();
        var definition = getDefinitionByElement(tr);
        // Remove the definition and the table row
        var i = DEFINITIONS.indexOf(definition);
        DEFINITIONS.splice(i, 1);
        tr.remove();
    });
    
    Object.defineProperty(definition, 'name', {
        get: function() {
            return this._name;
        },
        set: function(name) {
            this._name = name.trim().replace(' ', '_');
            // Update the representation
            $(this._entry.children()[0]).children(":first").text(this._name);
            // Check for errors
            checkDefinitionNames();
        }
    });
    Object.defineProperty(definition, 'value', {
        get: function() {
            return this._value;
        },
        set: function(v) {
            v = v.replace(';', ',');

            this._value = v;
            $(this._entry.children()[3]).css("color", "white");

            // Convert to Katex expression
            var eq;
            if (this._evaluated) {
                try {
                    eq = tokenizer.toKatex(v);
                    var element = $(this._entry.children()[1]).children(':first');
                    katex.render(eq, element.get(0));
                } catch (e) {
                    console.log(e);
                    $(this._entry.children()[1]).css("color", "red");
                    var txt = $(this._entry.children()[1]).children(":first").text();
                    if (txt !== v) {
                        $(this._entry.children()[1]).children(":first").text(v);
                    }
                }
            }
            else {
                var txt = $(this._entry.children()[1]).children(":first").text();
                if (txt !== v) {
                    $(this._entry.children()[1]).children(":first").text(v);
                }
            }
            checkDefinitionValues();
        }
    });
    Object.defineProperty(definition, 'evaluated', {
        get: function() {
            return this._evaluated;
        },
        set: function(checked) {
            this._evaluated = checked;
            // Re-render equations
            this.value = this._value;
        }
    });
    Object.defineProperty(definition, 'removable', {
        get: function() {
            return this._removable;
        },
        set: function(r) {
            this._removable = r;
            var element = $(this._entry.children()[3]).children(':first');
            if(r) {
                element.css('display', '');
            }
            else {
                element.css('display', 'none');
            }
        }              
    });

    // Enforce rendering length and value
    definition.value = definition._value;
    updateDefinitionsTable();

    return definition;
}

function definitionNameNumber(name) {
    var count = 0;
    for (var i=0; i<DEFINITIONS.length; i++) {
        if (DEFINITIONS[i].name === name) {
            count++;
        }
    }
    return count;
}

function checkDefinitionNames() {
    /** Check for errors in the definition names.
     */
    for (var i=0; i<DEFINITIONS.length; i++) {
        var definition = DEFINITIONS[i];
        var element = $(definition._entry.children()[0]);
        var span = element.children(':first');
        var valid = true;
        for (var j=0; j<i; j++) {
            if (DEFINITIONS[j].name === definition.name) {
                valid = false;
                break;
            }
        }
        if (valid) {
            element.css("color", "white");
            span.prop('title', '');
            span.tooltip('disable');
            continue;
        }
        element.css("color", "red");
        span.data('toggle', 'tooltip');
        span.data('placement', 'right');
        span.prop('title', 'Definition name already taken');
        span.tooltip('enable');
    }
}

function checkDefinitionExpression(defIndex, defAttr) {
    /** Check for errors in a particular definition expression
     * @param defIndex Index of the definition at the DEFINITIONS array
     * @param defAttr The definition attribute expression to be checked
     * @return undefined if no errors are detected, or the error description
     * otherwise.
     */
    var definition = DEFINITIONS[defIndex];
    var title = undefined;
    try {
        var deps = tokenizer.dependencies(definition[defAttr]);
        for (var i=0; i<deps.length; i++) {
            var dep = deps[i];
            if (typeof dep === "string") {
                title = '"' + dep + '" variable is not defined';
                break;
            }
        }
    } catch(e) {
        title = 'Invalid expression';
    }
    return title;
}

function checkDefinitionValues() {
    /** Check for errors in the definition value
     */
    for (var i=0; i<DEFINITIONS.length; i++) {
        var definition = DEFINITIONS[i];
        var element = $(definition._entry.children()[1]);
        var span = element.children(':first');

        if (definition.evaluated) {
            var title = checkDefinitionExpression(i, "value");
            if(title !== undefined) {
                element.css("color", "red");
                span.data('toggle', 'tooltip');
                span.data('placement', 'right');
                span.prop('title', title);
                span.tooltip('enable');
                continue;
            }
        }

        span.prop('title', '');
        span.tooltip('disable');
        element.css("color", "white");
    }
    
}

function updateDefinitionsTable() {
    /** Look for common errors, highlighting them and adding tooltips
     */
    checkDefinitionNames();
    checkDefinitionValues();
}

// Register the table errors check as loading callback
registerTabCallback('Definitions', updateDefinitionsTable);
