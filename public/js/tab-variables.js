var DIMS = 3;
var VARIABLES = [];
var __VAR_TYPES = ["unsigned int",
                   "uivec2",
                   "uivec3",
                   "uivec4",
                   "uivec",
                   "int",
                   "ivec2",
                   "ivec3",
                   "ivec4",
                   "ivec",
                   "float",
                   "vec2",
                   "vec3",
                   "vec4",
                   "vec",
                   "unsigned int*",
                   "uivec2*",
                   "uivec3*",
                   "uivec4*",
                   "uivec*",
                   "int*",
                   "ivec2*",
                   "ivec3*",
                   "ivec4*",
                   "ivec*",
                   "float*",
                   "vec2*",
                   "vec3*",
                   "vec4*",
                   "vec*",
                   "matrix*"];
var __FIXED_VARIABLES = 0;

Array.prototype.swap = function (x,y) {
  var b = this[x];
  this[x] = this[y];
  this[y] = b;
  return this;
}

function typeToN(t) {
    if(t.indexOf("vec2") != -1) {
        return 2;
    }
    else if(t.indexOf("vec3") != -1) {
        return 3;
    }
    else if(t.indexOf("vec4") != -1) {
        return 4;
    }
    else if(t.indexOf("vec") != -1) {
        return (DIMS - 1) * 2;  // 2 -> 2; 3 -> 4
    }
    else if(t.indexOf("matrix") != -1) {
        return Math.pow((DIMS - 1) * 2, 2);  // 2 -> 2; 3 -> 4
    }
    return 1;
}

function getVariable(name) {
    for (var i=0; i<VARIABLES.length; i++) {
        if (VARIABLES[i].name === name) {
            return VARIABLES[i];
        }
    }
    return undefined;
}

function getVariableByElement(element) {
    for (var i=0; i<VARIABLES.length; i++) {
        if (VARIABLES[i]._entry.get(0) === element.get(0)) {
            return VARIABLES[i];
        }
    }
    return undefined;
}

function getArrayVariables() {
    var variables = [];
    for (var i=0; i<VARIABLES.length; i++) {
        if (VARIABLES[i].type.endsWith('*')) {
            variables.push(VARIABLES[i]);
        }
    }
    return variables;
}

function getScalarVariables() {
    var variables = [];
    for (var i=0; i<VARIABLES.length; i++) {
        if (!VARIABLES[i].type.endsWith('*')) {
            variables.push(VARIABLES[i]);
        }
    }
    return variables;
}

function __varSpanText(editable, txt) {
    /* Create a span tag for text, which can be eventually edited
     * @param editable true if the user may edit the text that by clicking on
     * it, false otherwise.
     * @param txt Text inside the span section
     */
    var span = $('<span contenteditable=' + editable.toString() + '>');
    span.addClass("contenteditable");
    span.text(txt);

    span.blur(function() {
        var txt = $(this).text();
        // Get the changed attribute
        keys = ["name", "type", "length", "value", "symbol"];
        var i = $(this).parent().index();
        var attr = keys[i];
        // Update
        var variable = getVariableByElement($(this).parent().parent());
        variable[attr] = txt;
    });

    return span;
}

function __varSpanSelector(editable, options, option) {
    /* Create a span tag for math equation, which can be eventually edited
     * @param editable true if the user may edit the text that by clicking on
     * it, false otherwise.
     * @param txt Text inside the span section
     */
    var span = $('<select>');
    span.prop('disabled', !editable);
    for (var i=0; i<options.length; i++){
        var opt = $('<option>');
        opt.val(options[i]);
        opt.text(options[i]);
        span.append(opt);
    }
    span.val(option);

    span.on('change', function() {
        var txt = $(this).val();
        // Get the changed attribute
        keys = ["name", "type", "length", "value", "symbol"];
        var i = $(this).parent().index();
        var attr = keys[i];
        // Update
        var variable = getVariableByElement($(this).parent().parent());
        variable[attr] = txt;
        // We should check that length and value are still correct.
        updateVariablesTable();
    });

    return span;
}


function __varSpanMath(editable, txt) {
    /* Create a span tag for math equation, which can be eventually edited
     * @param editable true if the user may edit the text that by clicking on
     * it, false otherwise.
     * @param txt Text inside the span section
     */
    var span = $('<span contenteditable=' + editable.toString() + '>');
    span.addClass("contenteditable");
    span.text(txt);

    span.focus(function() {
        // Get the associated attribute
        keys = ["name", "type", "length", "value", "symbol"];
        var i = $(this).parent().index();
        var attr = keys[i];
        // And the variable
        var variable = getVariableByElement($(this).parent().parent());
        // Replace the Katex equation by the text
        $(this).text(variable[attr]);
    }).blur(function() {
        var txt = $(this).text();
        // Get the changed attribute
        keys = ["name", "type", "length", "value", "symbol"];
        var i = $(this).parent().index();
        var attr = keys[i];
        // Update
        var variable = getVariableByElement($(this).parent().parent());
        variable[attr] = txt;
    });

    return span;
}

function addVariable(name, type, length, value, symbol, editables) {
    if (name === undefined) {
        i = 0;
        while (getVariable("var" + i.toString()) !== undefined) {
            i += 1;
        }
        name = "var" + i.toString();
    }
    if (type === undefined) {
        type = __VAR_TYPES[0];
    }
    if (length === undefined) {
        length = "1";
    }
    if (value === undefined) {
        value = "0";
    }
    if (symbol === undefined) {
        symbol = name;
    }
    if (editables === undefined) {
        editables = {name:true, type:true, length:true, value:true, symbol:true};
    }

    var tr = $('<tr>');
    var variable = {_name:name,
                    _type:type,
                    _length:length,
                    _value:value,
                    _symbol:symbol,
                    _editables: editables,
                    _entry:tr,
                    _removable:true};
    VARIABLES.push(variable);

    var td;
    td = $('<td>'); tr.append(td);
    td.append(__varSpanText(variable._editables.name,
                            variable._name));
    td = $('<td>'); tr.append(td);
    td.append(__varSpanSelector(variable._editables.type,
                                __VAR_TYPES,
                                variable._type));
    td = $('<td>'); tr.append(td);
    td.append(__varSpanMath(variable._editables.length,
                            variable._length));
    td = $('<td>'); tr.append(td);
    td.append(__varSpanMath(variable._editables.value,
                            variable._value));
    td = $('<td>'); tr.append(td);
    td.append(__varSpanMath(variable._editables.symbol,
                            variable._symbol));
    if($('#varsTable').children().length == 0) {
        $('#varsTable').append(tr);
    }
    else {
        $('#varsTable tr:last').after(tr);
    }

    var math_element = tr.children(':last').children(':last');
    try {
        katex.render(variable._symbol, math_element.get(0));
    } catch (e) {
        console.log(e);
    }
    td = $('<td>'); tr.append(td);
    var remove = $('<img src="imgs/remove.svg" width="16px">');
    td.append(remove);
    remove.on('click', function() {
        // Get the table row and the variable
        var tr = $(this).parent().parent();
        var variable = getVariableByElement(tr);
        // Remove the variable and the table row
        var i = VARIABLES.indexOf(variable);
        VARIABLES.splice(i, 1);
        tr.remove();
    });
    var moveUp = $('<img src="imgs/moveUp.svg" width="16px">');
    td.append(moveUp);
    moveUp.on('click', function() {
        // Get the table row and the variable
        var tr = $(this).parent().parent();
        var variable = getVariableByElement(tr);
        var i = VARIABLES.indexOf(variable);
        // Move the tr
        var prev = tr.prev();
        tr.detach();
        prev.before(tr);
        // Swap the variables in the list
        VARIABLES.swap(i - 1, i);

        setMovableVariables();
        // The dependencies might be broken, recheck expressions
        checkVariableLengths();
        checkVariableValues();
    });
    var moveDown = $('<img src="imgs/moveDown.svg" width="16px">');
    td.append(moveDown);
    moveDown.on('click', function() {
        // Get the table row and the variable
        var tr = $(this).parent().parent();
        var variable = getVariableByElement(tr);
        var i = VARIABLES.indexOf(variable);
        // Move the tr
        var next = tr.next();
        tr.detach();
        next.after(tr);
        // Swap the variables in the list
        VARIABLES.swap(i, i + 1);

        setMovableVariables();
        // The dependencies might be broken, recheck expressions
        checkVariableLengths();
        checkVariableValues();
    });
    
    Object.defineProperty(variable, 'name', {
        get: function() {
            return this._name;
        },
        set: function(name) {
            this._name = name.trim().replace(' ', '_');
            // Update the representation
            $(this._entry.children()[0]).children(":first").text(this._name);
            // Check for errors
            checkVariableNames();
        }
    });
    Object.defineProperty(variable, 'type', {
        get: function() {
            return this._type;
        },
        set: function(t) {
            this._type = t;
            var editables = this.editables;
            if (t.endsWith('*')) {
                // Array. Lock the value field
                editables.length = true;
                editables.value = false;
                this.editables = editables;
                this.length = this.length;  // Recheck length
            }
            else {
                // Scalar. Lock the legnth field
                editables.length = false;
                editables.value = true;
                this.editables = editables;
                this.value = this.value.toString();  // Recheck value
            }
            // Update the representation if needed
            var txt = $(this._entry.children()[1]).children(":first").val();
            if (txt !== t) {
                $(this._entry.children()[1]).children(":first").val(t);
            }
        }
    });
    Object.defineProperty(variable, 'length', {
        get: function() {
            return this._length;
        },
        set: function(l) {
            l = tokenizer.split(l)[0];
            this._length = l;
            $(this._entry.children()[2]).css("color", "white");

            // Convert to Katex expression
            var eq;
            try {
                eq = tokenizer.toKatex(l);
                var element = $(this._entry.children()[2]).children(':first');
                katex.render(eq, element.get(0));
            } catch (e) {
                console.log(e);
                $(this._entry.children()[3]).css("color", "red");
                var txt = $(this._entry.children()[2]).children(":first").text();
                if (txt !== l) {
                    $(this._entry.children()[2]).children(":first").text(l);
                }
            }
            checkVariableLengths();
        }
    });
    Object.defineProperty(variable, 'value', {
        get: function() {
            return this._value;
        },
        set: function(v) {
            v = v.replace(';', ',');

            this._value = v;
            $(this._entry.children()[3]).css("color", "white");

            // Convert to Katex expression
            var eq;
            try {
                eq = tokenizer.toKatex(v);
                var element = $(this._entry.children()[3]).children(':first');
                katex.render(eq, element.get(0));
            } catch (e) {
                console.log(e);
                $(this._entry.children()[3]).css("color", "red");
                var txt = $(this._entry.children()[3]).children(":first").text();
                if (txt !== v) {
                    $(this._entry.children()[3]).children(":first").text(v);
                }
            }
            checkVariableValues();
        }
    });
    Object.defineProperty(variable, 'symbol', {
        get: function() {
            return this._symbol;
        },
        set: function(s) {
            this._symbol = s;
            try {
                var element = $(this._entry.children()[4]).children(':last');
                katex.render(s, element.get(0));
                $(this._entry.children()[4]).css("color", "white");
            } catch (e) { // (e if e instanceof katex.ParseError) {
                $(this._entry.children()[4]).css("color", "red");
                $(this._entry.children()[4]).children(":first").text(s);
            }
            // Re-render all the equations
            for (var i=0; i<VARIABLES.length; i++) {
                VARIABLES[i].length = VARIABLES[i]._length;
                VARIABLES[i].value = VARIABLES[i]._value;
            }
        }
    });
    Object.defineProperty(variable, 'editables', {
        get: function() {
            return this._editables;
        },
        set: function(e) {
            this._editables = e;
            var keys = ["name", "type", "length", "value", "symbol"];
            for (var i=0; i<keys.length; i++) {
                var span = $(this._entry.children()[i]).children(":first");
                if (span.is("select")) {
                    span.prop("disabled", !e[keys[i]]);
                }
                else if (span.is("span")) {                
                    span.attr("contenteditable", e[keys[i]]);
                }
            } 
        }              
    });
    Object.defineProperty(variable, 'removable', {
        get: function() {
            return this._removable;
        },
        set: function(r) {
            this._removable = r;
            var element = $(this._entry.children()[5]).children(':first');
            if(r) {
                element.css('display', '');
            }
            else {
                element.css('display', 'none');
            }
        }              
    });

    // Enforce rendering length and value
    variable.length = variable._length;
    variable.value = variable._value;
    variable.editables = variable._editables;
    updateVariablesTable();

    return variable;
}

function hideVariable(variable) {
    variable._entry.css("display", "none");
}

function showVariable(variable) {
    variable._entry.css("display", "");
}

function initializeVariables() {
    var v
    // Fixed variables internally handled by AQUAgpusph
    v = addVariable("end_t", "float", "1", "3.402823e+38", "t_{max}");
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("end_iter", "unsigned int", "1", "2147483647", "iter_{max}");
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("end_frame", "unsigned int", "1", "2147483647", "frame_{max}");
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("N", "unsigned int", "1", "0", "n");  // This guy depends on the particles sets
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("n_sets", "unsigned int", "1", "1", "n_{sets}")
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("n_radix", "unsigned int", "1", "0", "n_{radix}");  // This guy is the next power of 2 of N
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("n_cells", "uivec4", "1", "0,0,0,0", "n_{cells}");
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("id", "unsigned int*", "N", "0", "id");
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("r", "vec*", "N", "0,0,0,0", "\\boldsymbol{r}");
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("iset", "unsigned int*", "N", "0", "set");
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("id_sorted", "unsigned int*", "N", "0", "id^{sorted}");
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("id_unsorted", "unsigned int*", "N", "0", "id^{unsorted}");
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("icell", "unsigned int*", "N", "0", "cell");
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("ihoc", "unsigned int*", "n_cells_w", "0", "hoc");
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("mpi_rank", "unsigned int", "1", "0", "i_{mpi}");
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    v = addVariable("mpi_size", "unsigned int", "1", "1", "n_{mpi}");
    v.editables = {name:false, type:false, length:false, value:false, symbol:true};
    hideVariable(v);
    __FIXED_VARIABLES = 16;
    // Automatically added varaibles, that would be edited by the user
    v = addVariable("dims", "unsigned int", "1", "3", "d");
    v.editables = {name:false, type:false, length:false, value:true, symbol:true};
    v = addVariable("t", "float", "1", "0.0", "t");
    v.editables = {name:false, type:false, length:false, value:true, symbol:true};
    v = addVariable("dt", "float", "1", "0.0", "\\Delta t");
    v.editables = {name:false, type:false, length:false, value:true, symbol:true};
    v = addVariable("iter", "unsigned int", "1", "0", "iter");
    v.editables = {name:false, type:false, length:false, value:true, symbol:true};
    v = addVariable("frame", "unsigned int", "1", "0", "frame");
    v.editables = {name:false, type:false, length:false, value:true, symbol:true};
    v = addVariable("support", "float", "1", "2.0", "s");
    v.editables = {name:false, type:false, length:false, value:true, symbol:true};
    for (var i=0; i<VARIABLES.length; i++) {
        VARIABLES[i].removable = false;
    }
}

function variableNameNumber(name) {
    var count = 0;
    for (var i=0; i<VARIABLES.length; i++) {
        if (VARIABLES[i].name === name) {
            count++;
        }
    }
    return count;
}

function checkVariableNames() {
    /** Check for errors in the variable names.
     */
    for (var i=0; i<VARIABLES.length; i++) {
        var variable = VARIABLES[i];
        var element = $(variable._entry.children()[0]);
        var span = element.children(':first');
        var valid = true;
        for (var j=0; j<i; j++) {
            if (VARIABLES[j].name === variable.name) {
                valid = false;
                break;
            }
        }
        if (valid) {
            span.prop('title', '');
            span.tooltip('disable');
            // Let's choose the color depending wether it's an editable field or
            // not
            if (variable.editables.name) {
                element.css("color", "white");
            } else {
                element.css("color", "#CCC");
            }
            continue;
        }
        element.css("color", "red");
        span.data('toggle', 'tooltip');
        span.data('placement', 'right');
        span.prop('title', 'Variable name already taken');
        span.tooltip('enable');
    }
}

function checkVariableTypes() {
    /** This function is actually just decorating unmodificable vars in gray
     */
    for (var i=0; i<VARIABLES.length; i++) {
        var variable = VARIABLES[i];
        var element = $(variable._entry.children()[1]);
        if (variable.editables.type) {
            element.children(':first').css("color", "white");
        } else {
            element.children(':first').css("color", "#CCC");
        }
    }
}

function checkVariableExpression(varIndex, varAttr) {
    /** Check for errors in a particular variable expression
     * @param varIndex Index of the variable at the VARIABLES array
     * @param varAttr The variable attribute expression to be checked
     * @return undefined if no errors are detected, or the error description
     * otherwise.
     */
    var variable = VARIABLES[varIndex];
    var title = undefined;
    try {
        var deps = tokenizer.dependencies(variable[varAttr]);
        for (var i=0; i<deps.length; i++) {
            var dep = deps[i];
            if (typeof dep === "string") {
                title = '"' + dep + '" variable is not defined';
                break;
            }
            if (dep === varIndex) {
                title = 'Autoreferenced variable';
                break;
            }
            if (dep > varIndex) {
                title = 'The variable "' + VARIABLES[dep].name + '" is defined below';
                break;
            }
        }
    } catch(e) {
        title = 'Invalid expression';
    }
    return title;
}

function checkVariableLengths() {
    /** Check for errors in the variable length
     */
    for (var i=0; i<VARIABLES.length; i++) {
        var variable = VARIABLES[i];
        var element = $(variable._entry.children()[2]);
        var span = element.children(':first');
        if (!variable.type.endsWith('*')) {
            // Is a scalar variable, so it doesn't matter the length value.
            element.css("color", "#CCC");
            span.data('toggle', 'tooltip');
            span.data('placement', 'right');
            span.prop('title', 'Scalar variables length attribute is ignored');
            span.tooltip('enable');
            continue;
        }

        var title = checkVariableExpression(i, "length");
        if(title !== undefined) {
            element.css("color", "red");
            span.data('toggle', 'tooltip');
            span.data('placement', 'right');
            span.prop('title', title);
            span.tooltip('enable');
            continue;
        }

        span.prop('title', '');
        span.tooltip('disable');
        // Let's choose the color depending wether it's an editable field or
        // not
        if (variable.editables.length) {
            element.css("color", "white");
        } else {
            element.css("color", "#CCC");
        }
    }
}

function checkVariableValues() {
    /** Check for errors in the variable value
     */
    for (var i=0; i<VARIABLES.length; i++) {
        var variable = VARIABLES[i];
        var element = $(variable._entry.children()[3]);
        var span = element.children(':first');
        if (variable.type.endsWith('*')) {
            // Is an array variable, so it doesn't matter the value attribute.
            element.css("color", "#CCC");
            span.data('toggle', 'tooltip');
            span.data('placement', 'right');
            span.prop('title', 'Array variables value attribute is ignored');
            span.tooltip('enable');
            continue;
        }

        var title = checkVariableExpression(i, "value");
        if(title !== undefined) {
            element.css("color", "red");
            span.data('toggle', 'tooltip');
            span.data('placement', 'right');
            span.prop('title', title);
            span.tooltip('enable');
            continue;
        }

        var eqs = tokenizer.split(variable.value);
        var n = typeToN(variable.type);
        if (eqs.length < n) {
            element.css("color", "red");
            span.data('toggle', 'tooltip');
            span.data('placement', 'right');
            span.prop('title', 'Type "' + variable.type + '" requires at least ' + n.toString() + ' values');
            span.tooltip('enable');
            continue;            
        }

        span.prop('title', '');
        span.tooltip('disable');
        // Let's choose the color depending wether it's an editable field or
        // not
        if (variable.editables.value) {
            element.css("color", "white");
        } else {
            element.css("color", "#CCC");
        }
    }
    
}

function setMovableVariables() {
    var i;
    for (i=0; i<__FIXED_VARIABLES; i++) {
        var v = VARIABLES[i];
        var moveUp = $($(v._entry.children()[5]).children()[1]);
        var moveDown = $($(v._entry.children()[5]).children()[2]);
        moveUp.css('display', 'none');
        moveDown.css('display', 'none');
    }
    for (i=__FIXED_VARIABLES; i<VARIABLES.length; i++) {
        var v = VARIABLES[i];
        var moveUp = $($(v._entry.children()[5]).children()[1]);
        var moveDown = $($(v._entry.children()[5]).children()[2]);
        if (i === __FIXED_VARIABLES) {
            moveUp.css('display', 'none');
        } else {
            moveUp.css('display', '');
        }
        if (i === VARIABLES.length - 1) {
            moveDown.css('display', 'none');
        } else {
            moveDown.css('display', '');
        }
    }
}

function updateVariablesTable() {
    /** Look for common errors, highlighting them and adding tooltips
     */
    checkVariableNames();
    checkVariableTypes();
    checkVariableLengths();
    checkVariableValues();
    setMovableVariables();
}

// Register the table errors check as loading callback
registerTabCallback('Variables', updateVariablesTable);

$(document).ready(function(){
    var drag_index;
    $('#varsTable').sortable({
        start: function(event, ui) {
            drag_index = ui.item.index();
        },
        stop: function(evt, ui) {
            var i = ui.item.index();
            if(i === drag_index)
                return;

            function array_move(arr, old_index, new_index) {
                if (new_index >= arr.length) {
                    var k = new_index - arr.length + 1;
                    while (k--) {
                        arr.push(undefined);
                    }
                }
                arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
                return arr; // for testing
            };

            array_move(VARIABLES, drag_index, i);

            /*
            for (var i=0; i<VARIABLES.length; i++) {
                console.log(i, VARIABLES[i]._name, $('#varsTable').children()[i].firstChild.firstChild.innerHTML);
                VARIABLES[i]._entry = $($('#varsTable').children()[i]);
            }
            */

            setMovableVariables();
            // The dependencies might be broken, recheck expressions
            checkVariableLengths();
            checkVariableValues();
        },
        cancel: ':input,button,.contenteditable'
    });
});
