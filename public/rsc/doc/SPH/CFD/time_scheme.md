# Time integration schemes

Actually SPH can be conceived as a tool to compute differential operators, and on top of that, the particle fields rate of change.
However, a time integration scheme is required to integrate the position, velocity and density of each particle.
