# MLS kernel correction

MLS kernel correction is a matrix that can be applied to the kernel gradient
to assert linear consistency without any boundary condition.
Such matrix is computed as:

```latex
\mathcal{L}_i = \left[
\sum_{j \in \Omega} \left( \boldsymbol{r}_j - \boldsymbol{r}_i \right) \otimes \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j}.
\right]^{-1}
```

This module only computes that matrix, without applying it.
Moreover, its wide application all along the differential operators is strongly
not recommended, since it breaks all the intrinsic conservation properties and
might make impossible to apply some boundary conditions.
However, MLS would be required for some other modules, like the $`\delta`$-SPH
MLS correction.

## Usage

Just load this module after the "Basic Structure".

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
