# Boundary conditions

The default CFD module is able to deal with Free-Surfaces (accepting some inconsistencies as discussed by [Colagrossi et al (2016)](https://journals.aps.org/pre/abstract/10.1103/PhysRevE.79.056701)).
Any other boundary condition requires a contribution to the differential operators

# Theoretical background

When a particle is close enough to the boundary, the convolution compact support becomes suddently truncated.

![SPHConvolution](rsc/doc/SPH/CFD/BC/FS.svg)

Thus we can split the compact support in the flow subset, $`\bar{\Omega}`$, and the complementary subset $`\Omega^{*}`$.
Obviously, $`\bar{\Omega}`$ is already populated with particles, so the convolution can be still practised.
The same cannot be said on the $`\Omega^{*}`$ subset.
Thus, a generic differential operator can be written as,

```latex
\left\langle \nabla f \right\rangle_i = \sum_{j \in \bar{\Omega}} (f_j - f_i) \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) V_j + \left\langle \nabla f \right\rangle_i^{\Omega^{*}},
```

with,

```latex
\left\langle \nabla f \right\rangle_i^{\Omega^{*}} \simeq \int_{\Omega^{*}} f(\boldsymbol{y}) \nabla W(\boldsymbol{y} - \boldsymbol{r}_i; h) \mathrm{d}\boldsymbol{y}.
```

Actually, the definition above is quite ambiguous, since the field value $`f(\boldsymbol{y})`$ is unknown all along $`\Omega^{*}`$, by definition.

A number of formulations to deal with the approximation above have been described in the past.
This is still an open problem though.
