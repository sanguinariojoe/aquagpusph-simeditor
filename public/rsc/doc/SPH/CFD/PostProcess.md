# Post-process

Modules to compute several usefull magnitudes in runtime.

In contrast of post-processing the printed whole particles system snapshots,
these tools can be considered to collect information every single time step,
without a significant performance impact, neither in computational effort nor in
storage requirements.
