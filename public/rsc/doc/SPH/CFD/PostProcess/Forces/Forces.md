# Global force

This module computes the global force and moment made by the fluid on the
boundaries, based on momentum conservation,

```latex
\boldsymbol{f} = - \sum_{i \in \bar{\Omega}} m_i \left(
    \left\langle \frac{d \boldsymbol{u}}{d t} \right\rangle_i - \boldsymbol{g}
\right),
```

```latex
\boldsymbol{m} = - \sum_{i \in \bar{\Omega}} m_i 
\left(
    \boldsymbol{r}_i - \boldsymbol{r}_{ref}
\right)
\times
\left(
    \left\langle \frac{d \boldsymbol{u}}{d t} \right\rangle_i - \boldsymbol{g}
\right),
```

with $`\boldsymbol{r}_{ref}`$ the center of reference for moments computation.

This way to compute forces is very versatile, robust and efficient.
Unfortunatelly, it can be only considered for the total force computation.
Other modules are provided in case the force on different bodies shall be
computed independently.

This module is printing both the force and the moment into both the execution
terminal and the file `Forces.dat`.

## Usage

After loading this module the center of renference, `forces_r`, must be
conveniently specified ($`(0, 0, 0)`$ by default).

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
