# Pressure force

This module computes the pressure force made by the fluid onto a specific
boundary, modelled by Ghost Particles.

This module is printing both the force and the moment into both the execution
terminal and the file `GPPressureForces.dat`.

## Usage

After loading this module the affected boundary, `pressureForces_iset`, and the
center of renference, `pressureForces_r`, must be conveniently specified.

This module can be loaded several times to track different bodies.
However, all the modules execution are writing the same `pressureForces_F_GP`
and `pressureForces_M_GP` variables on the same `GPPressureForces.dat` file.
Thus, if the computed forces shall be considered for further computations on the
pipeline, remember to copy those variables in a safe place, and if the endpoint
is printing those forces to files, remember to edit `GP Pressure forces on-file`
tool to aim to a different file per boundary.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
