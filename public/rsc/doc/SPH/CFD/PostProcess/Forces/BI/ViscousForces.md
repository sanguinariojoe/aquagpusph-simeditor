# Pressure force

This module computes the viscous force made by the fluid onto a specific
boundary, modelled by *no-slip* Boundary Integrals.
In case free-slip boundary is modelled, non-null viscous forces will be
inconsistently retrieved.

This module is printing both the force and the moment into both the execution
terminal and the file `BIViscousForces.dat`.

## Usage

After loading this module the affected boundary, `viscousForces_iset`, and the
center of renference, `viscousForces_r`, must be conveniently specified.

This module can be loaded several times to track different bodies.
However, all the modules execution are writing the same `viscousForces_F_BI`
and `viscousForces_M_BI` variables on the same `BIViscousForces.dat` file.
Thus, if the computed forces shall be considered for further computations on the
pipeline, remember to copy those variables in a safe place, and if the endpoint
is printing those forces to files, remember to edit `BI Viscous forces on-file`
tool to aim to a different file per boundary.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
