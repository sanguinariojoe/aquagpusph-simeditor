# Hydrostatic correction

This simple linear correction assumes that the linear component of the pressure
field is heavily dominated by the hydrostatic pressure field, and therefore

```latex
\left\langle \nabla p \right\rangle_i^L = -\rho_i \,\, \boldsymbol{g}.
```

This model is really simple and computationally inexpensive, and in some cases
acceptable results are obtained.

## Usage

Before loading this module, you must load the "Basic delta-SPH" one.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
