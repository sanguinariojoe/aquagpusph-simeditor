# MLS correction

MLS-based linear correction,

```latex
\left\langle \nabla p \right\rangle_i^L =
\sum_{j \in \Omega} \left( p_j + p_i \right) \cdot 
\left(
    \mathcal{L}_i \cdot \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h)
\right)
\frac{m_j}{\rho_j}.
```

Although relatively expensive, this linear correction gives good results in
general, and it is strongly recommended.

## Usage

Before loading this module, you must load the "Basic delta-SPH" and the "MLS"
ones.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
