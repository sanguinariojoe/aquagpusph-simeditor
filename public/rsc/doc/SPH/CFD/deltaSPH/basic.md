# Basic $`\delta`$-SPH

This module adds the $`\delta`$-SPH diffusive term to the pipeline, considering
null linear correction,
$`\left\langle \nabla p \right\rangle_i^L = \boldsymbol{0}`$.
Thus,

```latex
\left\langle \Delta p \right\rangle_i = \sum_{j \in \Omega}
    \frac{
        p_j - p_i
    }{
        \vert \boldsymbol{r}_j - \boldsymbol{r}_i \vert^2
    } \left( \boldsymbol{r}_j - \boldsymbol{r}_i \right)
\cdot \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j}.
```

The expression above can be expected to be inconsistent close to the boundaries,
so in general a correction term shall be loaded afterwards.

## Usage

Just load this module after the CFD "Basic Structure".
Probably you want also to load the "delta-SPH MLS correction" module afterwards,
to avoid inconsistencies close to the boundaries.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
