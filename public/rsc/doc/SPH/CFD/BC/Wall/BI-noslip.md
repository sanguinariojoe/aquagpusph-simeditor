# No-slip for Boundary Integrals

The default Boundary Integrals module supports free-slip by means of a null contribution to the $`\left\langle \Delta \boldsymbol{u} \right\rangle_i`$ operator.
This module adds such contribution, modelling therefore a no-slip boundary condition.

![SPHConvolution](rsc/doc/SPH/CFD/BC/Wall/noslip.svg)

The new term depends on whether Monaghan or Morris Laplacian is considered.

## Morris term

In the Morris term just a tangential contribution is computed,

```latex
\left\langle \Delta \boldsymbol{u} \right\rangle_i^t =
2 \sum_{j \in \partial \bar{\Omega}} \frac{\boldsymbol{u}_{ij}^t}{\left( \boldsymbol{r}_j - \boldsymbol{r}_i \right) \cdot \boldsymbol{n}_j} W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) s_j,
```

with $`\boldsymbol{u}_{ij}^t`$ the tangential velocity difference,

```latex
\boldsymbol{u}_{ij}^t = \left( \boldsymbol{u}_j - \boldsymbol{u}_i \right) - \boldsymbol{n}_j \left( \boldsymbol{u}_j - \boldsymbol{u}_i \right) \cdot \boldsymbol{n}_j.
```

The term computed above is considered to compute the following Morris viscous term:

```latex
\left\langle \Delta \boldsymbol{u} \right\rangle_i =
\frac{1}{\gamma_i} \left(
    \sum_{j \in \Omega} \frac{
        \boldsymbol{u}_j - \boldsymbol{u}_i
    }{
        \vert \boldsymbol{r}_j - \boldsymbol{r}_i \vert^2
    } \left( \boldsymbol{r}_j - \boldsymbol{r}_i \right) \cdot \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j}
+
    \left\langle \Delta \boldsymbol{u} \right\rangle_i^t
\right).
```

## Monaghan term

For the Monaghan computation the same tangential term described above is considered.
However, a normal contribution is also considered,

```latex
\left\langle \Delta \boldsymbol{u} \right\rangle_i^n =
\sum_{j \in \partial \bar{\Omega}} \frac{
    \left( \boldsymbol{u}_j - \boldsymbol{u}_i \right) \cdot \left( \boldsymbol{r}_j - \boldsymbol{r}_i \right)
}{
    \vert \boldsymbol{r}_j - \boldsymbol{r}_i \vert^2
}
\boldsymbol{n}_j W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) s_j,
```

such that the final viscous term is computed as

```latex
\left\langle \Delta \boldsymbol{u} \right\rangle_i =
\frac{1}{\gamma_i} \left(
    \left(2 + 2 \left( d + 1 \right) \right) \left(
        \sum_{j \in \Omega} \frac{
            \left( \boldsymbol{u}_j - \boldsymbol{u}_i \right) \cdot \left( \boldsymbol{r}_j - \boldsymbol{r}_i \right)
        }{
            \vert \boldsymbol{r}_j - \boldsymbol{r}_i \vert^2
        } \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j}
    +
        \left\langle \Delta \boldsymbol{u} \right\rangle_i^n
    \right)
+
    \left\langle \Delta \boldsymbol{u} \right\rangle_i^t
\right).
```

## Usage

This module should be always loaded after `Boundary Integrals` one.
This module can be loaded several times, to affect different boundaries, belonging to different particle sets.
To this end, after loading this module the tool `BI no-slip set` can be edited to point to the desired particles set (0 by default).

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
