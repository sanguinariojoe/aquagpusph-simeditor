# Free-slip for Ghost Particles

The default Ghost Particles module supports no-slip by default, by means of an anti-symmetric velocity field extension.
This module modifies such velocity field extension by a symmetric one, modelling therefore a free-slip boundary condition.

![SPHConvolution](rsc/doc/SPH/CFD/BC/Wall/freeslip.svg)

## Usage

This module should be always loaded after `Ghost Particles` one.
This module can be loaded several times, to affect different boundaries, belonging to different particle sets.
To this end, after loading this module the tool `GP free-slip set` can be edited to point to the desired particles set (0 by default).

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
