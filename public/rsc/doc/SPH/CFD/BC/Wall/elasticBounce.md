# Elastic bounce

Elastic bounce shall be considered as a fallback method to avoid particles
tresspassing walls, instead of a standalone boundary.

Along this line, elastic bounce can be combined with both Boundary Integrals and
Ghost Particles.
When a fluid particle is about to cross through a boundary element (either a
proper one, `imove = -3`, or a mirror one, `imove = -2`), its velocity is
reflected.

![SPHConvolution](rsc/doc/SPH/CFD/BC/bounce.svg)

The velocity reflection is carried out to be consistent with the
Improved Euler/Heun time integrator.

## Usage

You just need to load this module, on top of Ghost Particles or Boundary
Integrals.

Optionally some parameters of the bounce can be tweaked by means of redefining
some variables.

`__ELASTIC_FACTOR__` can be used to control the amount of kinetic energy to be
preserved during the bounce.
It shall take a value between 0 (all the kinetic energy is lost) and 1 (all the
kinetic energy is preserved).
The default value is 0.

`__MIN_BOUND_DIST__` controls the distance (as a factor of $`\Delta r`$) to the
boundary at which the bounce is triggered.
If a particle is about to get closer to this distance, it will be bounced.
The default value is 0.3.

`__DR_FACTOR__` can be used to artificially enlarge the boundary elements so it
becomes less probable than a particle may find a way out between 2 boundary
elements.
The default value is 1.5.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
