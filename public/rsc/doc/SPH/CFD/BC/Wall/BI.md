# Boundary Integrals

In boundary integrals the boundary, $`\partial \bar{\Omega}`$, is populated with boundary elements.

![SPHConvolution](rsc/doc/SPH/CFD/BC/BI.svg)

Those boundary elements are the natural 2-D extension of the 3-D fuild particles, i.e. they have the same field properties than the fluid particles. On top of that, boundary elements are featured by their area, $`s`$, instead of the mass, and should transport the boundary normal information, $`\boldsymbol{n}`$.
Such boundary elements can be then applied to the convolution,

```latex
\left\langle \nabla f \right\rangle_i = \frac{1}{\gamma_i} \left(\sum_{j \in \bar{\Omega}} (f_j - f_i) \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j} + \sum_{j \in \partial \bar{\Omega}} (f_j - f_i) \boldsymbol{n}_j W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) s_j \right),
```

with,

```latex
\gamma_i = \sum_{j \in \bar{\Omega}} W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j}.
```

Those operators are considered for both the pressure gradient and velocity divergence operators:

```latex
\left\langle \nabla \cdot \boldsymbol{u} \right\rangle_i =
\frac{1}{\gamma_i} \left(
\sum_{j \in \Omega} \left( \boldsymbol{u}_j - \boldsymbol{u}_i \right) \cdot \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j} +
\sum_{j \in \partial \bar{\Omega}} \left( \boldsymbol{u}_j - \boldsymbol{u}_i \right) \boldsymbol{n}_j W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) s_j
\right),
```
```latex
\left\langle \nabla p \right\rangle_i =
\frac{1}{\gamma_i} \left(
\sum_{j \in \Omega} \left( p_j + p_i \right) \cdot \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j} +
\sum_{j \in \partial \bar{\Omega}} \left( p_j + p_i \right) \boldsymbol{n}_j W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) s_j
\right).
```

## Usage

You just need to load this module.
To setup the initial condition, i.e. the particles packing file to be loaded at the beggining of the simulation, the boundary elements should be distributed all along the boundary to be modelled.
Each $`i`$-th boundary element represents a square of dimensions $`\sqrt{s_i} \times \sqrt{s_i}`$, with $`s_i`$ its surface, to be stored in the mass array.
Also the boundary elements should be initialized with a normal pointing outwards the flow domain, and the `imove = -3` flag.
It does not matters the particles set they belongs to.

By default this module considers free-slip boundary condition, by means of a null contribution to the $`\left\langle \Delta \boldsymbol{u} \right\rangle_i`$ operator.

![SPHConvolution](rsc/doc/SPH/CFD/BC/Wall/freeslip.svg)

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
