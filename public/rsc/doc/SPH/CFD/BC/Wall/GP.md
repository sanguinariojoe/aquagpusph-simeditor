# Ghost Particles

In ghost particles methodology the flow domain is artificially extended across the boundary, $`\partial \bar{\Omega}`$, populating it with more fluid particles.

![SPHConvolution](rsc/doc/SPH/CFD/BC/GP.svg)

Those new particles are attached to the boundary, and contributes to the convolution in the same exact way the regular fluid particles,

```latex
\left\langle \nabla f \right\rangle_i = \sum_{j \in \bar{\Omega}} (f_j - f_i) \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j} + \sum_{j \in \bar{\Omega}^{*}} (f_j - f_i) \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j},
```

with $`\bar{\Omega}^{*}`$ the extended fluid domain.

The challenge is defining consitent extended fluid properties, $`f_j`$.
To this end different extension models are considered for each differential operator.

For the velocity Laplacian an antisymetric model is considered:

```latex
\boldsymbol{u}_j = 2 \boldsymbol{u}(\boldsymbol{r}_j^{*}) - \left\langle \boldsymbol{u} \right\rangle(2 \boldsymbol{r}_j^{*} - \boldsymbol{r}_j),
```

with $`\boldsymbol{r}_j^{*}`$ the mirroring point, or projection of the ghost particle in the boundary.

For the pressure, an antisymmetric model is considered as well:

```latex
p_j = \left\langle p \right\rangle(2 \boldsymbol{r}_j^{*} - \boldsymbol{r}_j) + \vert 2 \boldsymbol{r}_j^{*} - \boldsymbol{r}_j \vert \left\langle \nabla p \right\rangle(\boldsymbol{r}_j^{*}) \cdot \boldsymbol{n}(\boldsymbol{r}_j^{*}).
```

The normal derivative of the pressure field at the boundary is computed to enforce the untresspassability:

```latex
\left\langle \nabla p \right\rangle(\boldsymbol{r}_j^{*}) = -\rho(\boldsymbol{r}_j^{*}) \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t}(\boldsymbol{r}_j^{*}) -\mu \left\langle \Delta \boldsymbol{u} \right\rangle(\boldsymbol{r}_j^{*}) - \rho(\boldsymbol{r}_j^{*}) \boldsymbol{g}.
```

Finally, 2 different extensions are considered for the tangential and normal velocity components at the time of computing the divergence of the velocity:

```latex
\boldsymbol{u}_j \cdot \boldsymbol{n}(\boldsymbol{r}_j^{*}) = \boldsymbol{n}(\boldsymbol{r}_j^{*}) \cdot (2 \boldsymbol{u}(\boldsymbol{r}_j^{*}) - \left\langle \boldsymbol{u} \right\rangle(2 \boldsymbol{r}_j^{*} - \boldsymbol{r}_j)),
```
```latex
\boldsymbol{u}_j \cdot \boldsymbol{t}(\boldsymbol{r}_j^{*}) = \boldsymbol{t}(\boldsymbol{r}_j^{*}) \cdot \left\langle \boldsymbol{u} \right\rangle(2 \boldsymbol{r}_j^{*} - \boldsymbol{r}_j).
```

## Usage

You just need to load this module.
To setup the initial condition, i.e. the particles packing file to be loaded at the beggining of the simulation, the ghost particles should be distributed across the boundary to be modelled.
Each $`i`$-th ghost particle is created with the `imove = -1` flag.

To carry out the mirroring, each ghost particle must be associated with a boundary element, placed at the projection of the particle in the boundary, and marked with the `imove = -2` flag.
Thus, a set of mirroring boundary elements should be populated as well all along the boundary.
Then each ghost particle should have an entry in the `associations` array, with the index of its mirroring boundary element.
Several ghost particle may share the same boundary element, as it is the case in the figure shown above.

By default this module considers no-slip boundary condition.

![SPHConvolution](rsc/doc/SPH/CFD/BC/Wall/noslip.svg)

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
