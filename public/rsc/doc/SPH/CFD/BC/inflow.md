# Inflow

A rectangular plane where an inflow lattice of particles is injected in a
constant basis.
This boundary is featured by a constant inflow velocity and an hydrostatic
pressure field.

## Usage

This module should be loaded, after the "Basic structure" and "Computational
domain" ones.
Also, the following variables shall be defined:

 - `inflow_r`: Bottom-left corner of the inflow rectangular plane
 - `inflow_ru`: rectangular plane U direction vector, $`(0, 1, 0)`$ by default
 - `inflow_rv`: rectangular plane V direction vector, $`(0, 0, 1)`$ by default. Useless in 2D
 - `inflow_n`: Rectangular plane normal, $`(1, 0, 0)`$ by default
 - `inflow_U`: Inflow velocity
 - `inflow_rFS`: Free surface reference position, to compute the hydrostatic pressure

This module requires consuming buffer particles to constantly feed the system
with new particles.
Thus, at the time of providing the initial condition file, remember to place
some extra invalid particles out of the computational domain.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
