# Solid walls

Boundary conditions to impose both free-slip and no-slip imprenetrable wall,

The impenetrable wall is imposed by means of a normal component of the velocity at the boundary as the normal component of the boundary velocity,

```latex
\boldsymbol{u}_i \cdot \boldsymbol{n}(\boldsymbol{r}_i)  = \boldsymbol{u}(\boldsymbol{r}_i) \cdot \boldsymbol{n}(\boldsymbol{r}_i) \,\, \mathrm{if} \,\, \boldsymbol{r}_i \in \partial \bar{\Omega},
```

with $`\boldsymbol{n}`$ the outward pointing boundary normal.

Of course, due to the Lagrangian nature of the model, the boundary impenetrability is not an easy condition to impose.

Regarding the free-slip/no-slip, it is referring to the tangential component of the velocity.
In free-slip a Neumann boundary condition is imposed,

```latex
\nabla \partial \boldsymbol{u}_i \cdot \boldsymbol{n}(\boldsymbol{r}_i) = \boldsymbol{u}(\boldsymbol{r}_i) - \boldsymbol{n}(\boldsymbol{r}_i) \left(\boldsymbol{u}(\boldsymbol{r}_i) \cdot \boldsymbol{n}(\boldsymbol{r}_i)\right) \,\, \mathrm{if} \,\, \boldsymbol{r}_i \in \partial \bar{\Omega}.
```

![SPHConvolution](rsc/doc/SPH/CFD/BC/Wall/freeslip.svg)

In contrast, for no-slip boundary conditions a Dirichlet boundary condition should be imposed,

```latex
\boldsymbol{u}_i - \boldsymbol{n}(\boldsymbol{r}_i) \left(\boldsymbol{u}_i \cdot \boldsymbol{n}(\boldsymbol{r}_i)\right) = \boldsymbol{u}(\boldsymbol{r}_i) - \boldsymbol{n}(\boldsymbol{r}_i) \left(\boldsymbol{u}(\boldsymbol{r}_i) \cdot \boldsymbol{n}(\boldsymbol{r}_i)\right) \,\, \mathrm{if} \,\, \boldsymbol{r}_i \in \partial \bar{\Omega}.
```

![SPHConvolution](rsc/doc/SPH/CFD/BC/Wall/noslip.svg)
