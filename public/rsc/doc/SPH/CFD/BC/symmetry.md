# Symmetry plane

An infinite plane where the particles are reflected in.
The reflected particles are ephemeral, becoming created after the predictor and
destroyed again after the interactions computation.

Every single particle will be mirrored, including boundary particles and
elements.
The very exception are invalid and buffer particles, `imove <= -255`.

## Usage

This module should be loaded after the "Basic structure" and "Computational
domain" ones.
The following variables shall be defined aftwerwards:

 - `symmetry_r`: Infinite plane arbitrary point
 - `symmetry_n`: Infinite plane normal, $`(1, 0, 0)`$ by default

In case several symmetry planes are required, this module can be loaded several
times, adding tools to set the variables above before *both* `symmetry init` and
`symmetry clean` tools.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
