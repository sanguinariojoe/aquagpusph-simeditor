# Inflow

An infinite plane where the particles are abandoning the system with a
prefixed velocity and an hydrostatic pressure field.

## Usage

This module should be loaded, after the "Basic structure" and "Computational
domain" ones.
The following variables shall be defined aftwerwards:

 - `outflow_r`: Infinite plane arbitrary point
 - `outflow_n`: Infinite plane normal, $`(1, 0, 0)`$ by default
 - `outflow_U`: Outflow velocity
 - `outflow_rFS`: Free surface reference position, to compute the hydrostatic pressure

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
