# Explicit Euler scheme

This module can be loaded to set the explicit Euler time integration scheme.
This module is the simplest first order scheme, requiring just a single interactions stage computation per time step,

```latex
\left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n}) = \dots,
```
```latex
\left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n}) = \dots,
```
```latex
\boldsymbol{r}_i(t_{n + 1}) = \boldsymbol{r}_i(t_{n}) + \Delta t \boldsymbol{u}_i(t_{n}) + \frac{\Delta t^2}{2} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n}),
```
```latex
\boldsymbol{u}_i(t_{n + 1}) = \boldsymbol{u}_i(t_{n}) + \Delta t \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n}),
```
```latex
\rho_i(t_{n + 1}) = \rho_i(t_{n}) + \Delta t \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n}),
```
## Usage

In general you just need to load this module.
However, if you loaded some other scheme before (like Adams-Bashforth), then you probably want to remove some extra variables and tools appended by the model.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
