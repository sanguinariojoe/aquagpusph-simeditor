# Explicit Adams-Bashforth scheme

This module can be loaded to set an explicit Addams-Bashforth time integration
scheme.
Actually this model implements several Addams-Bashforth schemes, depending on
the number of required substeps (up to 5 steps are allowed).

## Expressions

At the first time steps, no enough information is available to compute the
scheme, so lower order schemes are considered until such information becomes
available.

### First step

The first step is actually a 1st order Euler scheme.
If a single step scheme is intended to be applied, it is strongly recommended to
select directly the Euler scheme instead, since this module would imply some
overheads.

```latex
\left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n}) = \dots,
```
```latex
\left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n}) = \dots,
```
```latex
\boldsymbol{r}_i(t_{n + 1}) = \boldsymbol{r}_i(t_{n}) + \Delta t \boldsymbol{u}_i(t_{n}) + \frac{\Delta t^2}{2} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n}),
```
```latex
\boldsymbol{u}_i(t_{n + 1}) = \boldsymbol{u}_i(t_{n}) + \Delta t \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n}),
```
```latex
\rho_i(t_{n + 1}) = \rho_i(t_{n}) + \Delta t \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n}),
```

### Second step

```latex
\left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n}) = \dots,
```
```latex
\left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n}) = \dots,
```
```latex
\boldsymbol{r}_i(t_{n + 1}) = \boldsymbol{r}_i(t_{n}) + \Delta t \boldsymbol{u}_i(t_{n}) + \frac{\Delta t^2}{2} \left(
    \frac{3}{2} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n}) -
    \frac{1}{2} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1})
\right),
```
```latex
\boldsymbol{u}_i(t_{n + 1}) = \boldsymbol{u}_i(t_{n}) + \Delta t \left(
    \frac{3}{2} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n}) -
    \frac{1}{2} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1})
\right),
```
```latex
\rho_i(t_{n + 1}) = \rho_i(t_{n}) + \Delta t \left(
    \frac{3}{2} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n}) -
    \frac{1}{2} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 1})
\right),
```

### Third step

```latex
\left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n}) = \dots,
```
```latex
\left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n}) = \dots,
```
```latex
\boldsymbol{r}_i(t_{n + 1}) = \boldsymbol{r}_i(t_{n}) + \Delta t \boldsymbol{u}_i(t_{n}) + \frac{\Delta t^2}{2} \left(
    \frac{23}{12} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n})
    - \frac{16}{12} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1})
    + \frac{5}{12} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 2})
\right),
```
```latex
\boldsymbol{u}_i(t_{n + 1}) = \boldsymbol{u}_i(t_{n}) + \Delta t \left(
    \frac{23}{12} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n})
    - \frac{16}{12} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1})
    + \frac{5}{12} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 2})
\right),
```
```latex
\rho_i(t_{n + 1}) = \rho_i(t_{n}) + \Delta t \left(
    \frac{23}{12} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n})
    - \frac{16}{12} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 1})
    + \frac{5}{12} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 2})
\right),
```

### Fourth step

```latex
\left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n}) = \dots,
```
```latex
\left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n}) = \dots,
```
```latex
\boldsymbol{r}_i(t_{n + 1}) = \boldsymbol{r}_i(t_{n}) + \Delta t \boldsymbol{u}_i(t_{n}) + \frac{\Delta t^2}{2} \left(
    \frac{55}{24} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n})
    - \frac{59}{24} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1})
    + \frac{37}{24} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 2})
    - \frac{9}{24} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 3})
\right),
```
```latex
\boldsymbol{u}_i(t_{n + 1}) = \boldsymbol{u}_i(t_{n}) + \Delta t \left(
    \frac{55}{24} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n})
    - \frac{59}{24} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1})
    + \frac{37}{24} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 2})
    - \frac{9}{24} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 3})
\right),
```
```latex
\rho_i(t_{n + 1}) = \rho_i(t_{n}) + \Delta t \left(
    \frac{55}{24} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n})
    - \frac{59}{24} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 1})
    + \frac{37}{24} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 2})
    - \frac{9}{24} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 3})
\right),
```

### Fifth step

```latex
\left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n}) = \dots,
```
```latex
\left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n}) = \dots,
```
```latex
\boldsymbol{r}_i(t_{n + 1}) = \boldsymbol{r}_i(t_{n}) + \Delta t \boldsymbol{u}_i(t_{n}) + \frac{\Delta t^2}{2} \left(
    \frac{1901}{720} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n})
    - \frac{2774}{720} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1})
    + \frac{2616}{720} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 2})
    - \frac{1724}{720} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 3})
    + \frac{251}{720} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 4})
\right),
```
```latex
\boldsymbol{u}_i(t_{n + 1}) = \boldsymbol{u}_i(t_{n}) + \Delta t \left(
    \frac{1901}{720} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n})
    - \frac{2774}{720} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1})
    + \frac{2616}{720} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 2})
    - \frac{1724}{720} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 3})
    + \frac{251}{720} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 4})
\right),
```
```latex
\rho_i(t_{n + 1}) = \rho_i(t_{n}) + \Delta t \left(
    \frac{1901}{720} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n})
    - \frac{2774}{720} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 1})
    + \frac{2616}{720} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 2})
    - \frac{1724}{720} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 3})
    + \frac{251}{720} \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 4})
\right),
```

## Usage

After loading this module you can select the number of steps to be considered
editing `TSCHEME_ADAMS_BASHFORTH_STEPS` at the "Definitions" tab (5 steps is the
maximum supported value).
This module needs to add several variables, as well as the tools
`sort adams-bashforth` and `backup adams-bashforth`, that you will need to
remove manually if you want to switch back to some other scheme.

If you want to make posible to stop and resume simulations, it is required to
save some extra fields in the output files, `dudt_as1_in`, `dudt_as2_in`,
`dudt_as3_in`, `dudt_as4_in`, `drhodt_as1_in`, `drhodt_as2_in`, `drhodt_as3_in`,
`drhodt_as4_in`.

![Setting AQUAgpusph folder](rsc/doc/SPH/CFD/time_scheme/saveAdamBashforth.png)

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
