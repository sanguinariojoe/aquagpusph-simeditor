# Implicit Midpoint Euler scheme

This module can be loaded to set the implicit midpoint Euler time integration
scheme.
This first order implicit module requires to solve a fixed point problem,

```latex
\left\langle
    \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t}
\right\rangle_i(t_{n + \frac{1}{2}}) =
ns_{\boldsymbol{u}}(\boldsymbol{r}(t_n),
                    \boldsymbol{u}(t_{n + \frac{1}{2}}),
                    \rho(t_{n + \frac{1}{2}})),
```
```latex
\left\langle
    \frac{\mathrm{d} \rho}{\mathrm{d} t}
\right\rangle_i(t_{n}) =
ns_{\rho}(\boldsymbol{r}(t_n),
          \boldsymbol{u}(t_{n + \frac{1}{2}}),
          \rho(t_{n + \frac{1}{2}})),
```

with $`ns_{\boldsymbol{u}}`$ and $`ns_{\rho}`$ the momentum and mass
conservation Navier-Stokes equations respectively.
With those variation rates a simple Euler integration scheme can be applied,

```latex
\boldsymbol{r}_i(t_{n + 1}) =
\boldsymbol{r}_i(t_{n}) +
\Delta t \boldsymbol{u}_i(t_{n + \frac{1}{2}}),
```
```latex
\boldsymbol{u}_i(t_{n + 1}) =
\boldsymbol{u}_i(t_{n}) +
\Delta t \left\langle
    \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t}
\right\rangle_i(t_{n + \frac{1}{2}}),
```
```latex
\rho_i(t_{n + 1}) = \rho_i(t_{n}) +
\Delta t \left\langle
    \frac{\mathrm{d} \rho}{\mathrm{d} t}
\right\rangle_i(t_{n + \frac{1}{2}}).
```

Both fixed point problems are solved by an iterative solver, i.e. in a substep,
$`m`$, a new candidate variation rates are computed,

```latex
\left\langle
    \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t}
\right\rangle_i(t_{n + \frac{1}{2}})^{*} =
ns_{\boldsymbol{u}}(\boldsymbol{r}(t_n),
                    \boldsymbol{u}(t_{n + \frac{1}{2}})^m,
                    \rho(t_{n + \frac{1}{2}})^m),
```
```latex
\left\langle
    \frac{\mathrm{d} \rho}{\mathrm{d} t}
\right\rangle_i(t_{n})^{*} =
ns_{\rho}(\boldsymbol{r}(t_n),
          \boldsymbol{u}(t_{n + \frac{1}{2}})^m,
          \rho(t_{n + \frac{1}{2}})^m),
```

which are conveniently relaxed to grant convergence and used to predict new
state variables,

```latex
\left\langle
    \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t}
\right\rangle_i(t_{n + \frac{1}{2}})^{m+1} =
f^m \left\langle
    \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t}
\right\rangle_i(t_{n + \frac{1}{2}})^{*}
+
(1 - f^m) \left\langle
    \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t}
\right\rangle_i(t_{n + \frac{1}{2}})^{m},
```
```latex
\left\langle
    \frac{\mathrm{d} \rho}{\mathrm{d} t}
\right\rangle_i(t_{n + \frac{1}{2}})^{m+1} =
f^m \left\langle
    \frac{\mathrm{d} \rho}{\mathrm{d} t}
\right\rangle_i(t_{n + \frac{1}{2}})^{*}
+
(1 - f^m) \left\langle
    \frac{\mathrm{d} \rho}{\mathrm{d} t}
\right\rangle_i(t_{n + \frac{1}{2}})^{m},
```
```latex
\boldsymbol{u}(t_{n + \frac{1}{2}})^{m+1} = 
\boldsymbol{u}(t_{n}) +
\frac{\Delta t}{2} \left\langle
    \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t}
\right\rangle_i(t_{n + \frac{1}{2}})^{m+1},
```
```latex
\rho(t_{n + \frac{1}{2}})^{m+1} = 
\rho(t_{n}) +
\frac{\Delta t}{2} \left\langle
    \frac{\mathrm{d} \rho}{\mathrm{d} t}
\right\rangle_i(t_{n + \frac{1}{2}})^{m+1},
```

where the relaxation factor, $`f^m`$, is computed considering the following
expression:

```latex
f^m = f_{max} e^(- \frac{(m - M / 2)^2}{\sigma^2}),
```

with $`M`$ the maximum number of subiterations, $`f_{max}`$ the maximum relaxation
factor during all the iterative process, and $`\sigma`$ a factor automatically
tuned to grant the relaxation factor starts with a provided initial value.

## Usage

You can load this module on top of the CFD basic structure.
Afterwards, you can tweak the following parameters:

 - `subiter_max` : Number of subiterations carried out to solve the fixed problems. Increasing this value will improve the stability of the model, but also the computational cost (default = 10).
 - `subiter_relax` : Initial relaxation factor. Increasing this value the solver may converge faster. However, values close to 1 may turn the solver unstable (default = 0.1).
 - `subiter_relax_max` : Maximum overall relaxation factor. Increasing this value the solver may converge faster. However, values close to 1 may turn the solver unstable (default = 0.75).

Unlike the explicit schemes, reverting back the changes made by this module would be a hard task.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
