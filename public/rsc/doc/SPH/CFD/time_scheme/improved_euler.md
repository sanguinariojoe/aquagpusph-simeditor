# Explicit Improved Euler scheme

This module can be loaded to set the explicit Improved Euler time integration scheme (also known as Heun scheme).
This module is a quasi-second order scheme, with the main benefit of requiring just a single interactions stage computation per time step,

```latex
\boldsymbol{r}_i(t_{n + 1/2}) = \boldsymbol{r}_i(t_{n}) + \Delta t \boldsymbol{u}_i(t_{n}) + \frac{\Delta t^2}{2} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1/2}),
```
```latex
\boldsymbol{u}_i(t_{n + 1/2}) = \boldsymbol{u}_i(t_{n}) + \Delta t \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1/2}),
```
```latex
\rho_i(t_{n + 1/2}) = \rho_i(t_{n}) + \Delta t \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 1/2}),
```
```latex
\left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n + 1/2}) = \dots,
```
```latex
\left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n + 1/2}) = \dots,
```
```latex
\boldsymbol{r}_i(t_{n + 1}) = \boldsymbol{r}_i(t_{n + 1/2}) + \frac{\Delta t^2}{4} \left(
    \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n + 1/2}) -
    \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1/2})
\right),
```
```latex
\boldsymbol{u}_i(t_{n + 1}) = \boldsymbol{u}_i(t_{n + 1/2}) + \frac{\Delta t}{2}  \left(
    \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n + 1/2}) -
    \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1/2})
\right),
```
```latex
\rho_i(t_{n + 1}) = \rho_i(t_{n + 1/2}) + \frac{\Delta t}{2}  \left(
    \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n + 1/2}) -
    \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 1/2})
\right),
```

## Usage

In general you just need to load this module.
However, if you loaded some other scheme before (like Adams-Bashforth), then you probably want to remove some extra variables and tools appended by the model.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
