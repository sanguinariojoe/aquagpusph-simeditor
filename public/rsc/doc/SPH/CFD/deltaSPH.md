# $`\delta`$-SPH

$`\delta`$-SPH is an additional term used to stabilize the simulation in
explicit time integration schemes.
Such term is a diffusion term in the continuity equation in the form:

```latex
\left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i^\delta = 
\delta \Delta t \frac{\rho_i}{\rho_0} \left\langle \Delta p \right\rangle_i,
```

with

```latex
\left\langle \Delta p \right\rangle_i = \sum_{j \in \Omega} \left(
    \frac{
        p_j - p_i
    }{
        \vert \boldsymbol{r}_j - \boldsymbol{r}_i \vert^2
    } \left( \boldsymbol{r}_j - \boldsymbol{r}_i \right)
    -
    \frac{
        \left\langle \nabla p \right\rangle_j^L
        +
        \left\langle \nabla p \right\rangle_i^L
    }{
        2
    }
\right) \cdot \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j}.
```

Thus $`\left\langle \Delta p \right\rangle`$ is the Laplacian of the pressure
field, considering the Morris expression.
Such expression is strongly inconsistent close to the free surface, so in
general a linear correction of the gradient shall be applied,
$`\left\langle \nabla p \right\rangle^L`$.
