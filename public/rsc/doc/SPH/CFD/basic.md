# CFD basic structure

This module provides the most basic CFD pipeline, with the following features.

## Features

### Stiff Equation Of State (EOS)

A weakly-compressible model based on the following EOS is considered:

```latex
p_i = c_s^2 (\rho_i - \rho_0),
```

with $`c_s`$ the speed of sound and $`\rho_0`$ the flow density of reference.

### Symmetric pressure gradient operator

While the velocity divergence is the straightforward implementation of SPH,

```latex
\left\langle \nabla \cdot \boldsymbol{u} \right\rangle_i =
\sum_{j \in \Omega} \left( \boldsymbol{u}_j - \boldsymbol{u}_i \right) \cdot \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j},
```

the pressure operator takes a symmetric form, which grants momentum and energy conservation,

```latex
\left\langle \nabla p \right\rangle_i =
\sum_{j \in \Omega} \left( p_j + p_i \right) \cdot \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j}.
```

0th order consistency is not anymore granted closed to the boundary though.

### Monaghan or Morris viscous term

Both the Monaghan viscous term,

```latex
\left\langle \Delta \boldsymbol{u} \right\rangle_i = \left(2 + 2 \left( d + 1 \right) \right)
\sum_{j \in \Omega} \frac{
    \left( \boldsymbol{u}_j - \boldsymbol{u}_i \right) \cdot \left( \boldsymbol{r}_j - \boldsymbol{r}_i \right)
}{
    \vert \boldsymbol{r}_j - \boldsymbol{r}_i \vert^2
} \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j},
```

and the Morris one,

```latex
\left\langle \Delta \boldsymbol{u} \right\rangle_i = \sum_{j \in \Omega} \frac{
    \boldsymbol{u}_j - \boldsymbol{u}_i
}{
    \vert \boldsymbol{r}_j - \boldsymbol{r}_i \vert^2
} \left( \boldsymbol{r}_j - \boldsymbol{r}_i \right) \cdot \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j},
```

are implemented.
The Monaghan one is used by default.

### Quintic Wendland kernel

The Quintic Wendland kernel, which has demonstrated good particles packing preservation properties, is considered by default,

```latex
W(\boldsymbol{y} - \boldsymbol{x}; h) = \frac{k}{h^{d} \pi} \left( 1 + q \right) \left( 2 - q \right)^4,
```
```latex
W(\boldsymbol{y} - \boldsymbol{x}; h) = \frac{10 k}{\pi} \frac{\boldsymbol{y} - \boldsymbol{x}}{h^{d+1}} \left( 2 - q \right)^3.
```

In the expressions above $`d`$ is the number of dimensions, $`k`$ is a constant which depends also on the number of dimensions, and $`q = \vert \boldsymbol{y} - \boldsymbol{x} \vert / h`$.

### Improved Euler time integrator

By default the explicit Improved Euler time integration scheme is applied,

```latex
\boldsymbol{r}_i(t_{n + 1/2}) = \boldsymbol{r}_i(t_{n}) + \Delta t \boldsymbol{u}_i(t_{n}) + \frac{\Delta t^2}{2} \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1/2}),
```
```latex
\boldsymbol{u}_i(t_{n + 1/2}) = \boldsymbol{u}_i(t_{n}) + \Delta t \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1/2}),
```
```latex
\rho_i(t_{n + 1/2}) = \rho_i(t_{n}) + \Delta t \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 1/2}),
```
```latex
\left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n + 1/2}) = \dots,
```
```latex
\left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n + 1/2}) = \dots,
```
```latex
\boldsymbol{r}_i(t_{n + 1}) = \boldsymbol{r}_i(t_{n + 1/2}) + \frac{\Delta t^2}{4} \left(
    \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n + 1/2}) -
    \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1/2})
\right),
```
```latex
\boldsymbol{u}_i(t_{n + 1}) = \boldsymbol{u}_i(t_{n + 1/2}) + \frac{\Delta t}{2}  \left(
    \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n + 1/2}) -
    \left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i(t_{n - 1/2})
\right),
```
```latex
\rho_i(t_{n + 1}) = \rho_i(t_{n + 1/2}) + \frac{\Delta t}{2}  \left(
    \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n + 1/2}) -
    \left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i(t_{n - 1/2})
\right),
```

### Sensors

Sensors are regular particles that don't contribute to the interactions.
Some useful variables are convoluted in the sensor,

```latex
\left\langle \boldsymbol{u} \right\rangle_i = \frac{1}{\gamma_i}
\sum_{j \in \Omega} \boldsymbol{u}_j W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j},
```
```latex
\left\langle \rho \right\rangle_i = \frac{1}{\gamma_i}
\sum_{j \in \Omega} \rho_j W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j},
```
```latex
\left\langle p \right\rangle_i = \frac{1}{\gamma_i}
\sum_{j \in \Omega} p_j W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j},
```

with $`\gamma_i`$ the shepard renormalization factor,

```latex
\gamma_i = \sum_{j \in \Omega} W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) \frac{m_j}{\rho_j},
```

### Particles sorting

For performance optimization purposes, the particles are sorted according to their position, and more specifically, according the cell index they belong to.
This shall be taken into account at the time of implementing/adding new particle fields.
In such case, tools to duplicate and sort the new fields shall be added between "Sort (start)" and "Sort (end)" dummy tools.
To learn more about sorting particle fields, you can check "Backup id" and "sort stage1" tools deployed by this module.

## Usage

After loading this module, you are resposible of defining the following variables:

 - `c_s` : The speed of sound, which should be of type `float`.
 - `h` : The kernel characteristic length, which should be of type `float`.

You would be interested in tweaking some other variables too:

 - `g` : The gravity acceleration, or any other volumetric force actually.
 - `p_0` : The background pressure, which can be increased *in the abscence of free surface* to improve the stability.
 - `refd` : The density of reference. This variable is defined per particles set, and therefore should be tweaked in the "Sets" tab.
 - `visc_dyn` : The dynamic viscosity. This variable is defined per particles set, and therefore should be tweaked in the "Sets" tab.

Optionally, you can also modify the considered Viscous model in the "Definitions" tab, replacing the `__LAP_FORMULATION__` value by `__LAP_MORRIS__`.

![Setting Laplacian formulation](rsc/doc/SPH/CFD/SetLap.png)

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
