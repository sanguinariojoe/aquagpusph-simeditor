# Computational domain

Defines a computational domain.
Every particle (of any kind) moving out of the computational domain will be
converted in an invalid one (`imove = -256`).
Those invalid particles are converted in buffer particles in the next time step.

## Usage

This module shall be loaded after the "Basic Structure".
After that you can set the value of the `domain_min` and `domain_max` variables,
which controls the bottom-left-back and top-right-front corners of the
computational domain respectively.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
