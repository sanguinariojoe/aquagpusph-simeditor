# Basic structure

This module implements the most basic particles packing algorithm based on
gradient descent minimization,

```latex
\Delta \boldsymbol{r}_{i, n + 1} =
- f V_i^(1 / d)
\left\langle \nabla \gamma \right\rangle_i (\boldsymbol{r}_i + \Delta \boldsymbol{r}_{i, n}),
```

To this end, a constant particles volume is considered, $`V_i = m_i / \rho_i`$,
all along the process.
When it is finished, the pressure based on an hydrostatic pressure field is
computed, and the density and mass conveniently derived,

```latex
p_i = \rho_0 \boldsymbol{g} \cdot \left(
    \boldsymbol{r}_0
    -
    \left(
        \boldsymbol{r}_i + \Delta \boldsymbol{r}_{i, n}
    \right)
\right),
```
```latex
\rho_i = \rho_0 + \frac{p_i}{c_s^2},
```
```latex
m_i = \rho_i \, V_i.
```

In expressions above $`\boldsymbol{r}_0`$ is an arbitrary point at the
free-surface.

## Usage

Load this module, which is valid also if just Ghost Particles are considered.
Some other boundary conditions may require adding some other modules.
After loading this module, you are resposible of defining the following variables:

 - `c_s` : The speed of sound, which should be of type `float`.
 - `h` : The kernel characteristic length, which should be of type `float`.

You would be interested in tweaking some other variables too:

 - `g` : The gravity acceleration, or any other volumetric force actually.
 - `r0` : An arbitrary point at the free-surface
 - `refd` : The density of reference. This variable is defined per particles set, and therefore should be tweaked in the "Sets" tab.

On top of that, if a free-surface shall be considered, a solid wall shall be
added along it.
To this end, it is strongly recommended to create such "virtual" wall in a
different particles set which will not be loaded by the main simulation
afterwards.

All the particles packing modules require the AQUAgpusph resources folder, where
the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
