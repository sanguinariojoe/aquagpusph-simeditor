# Boundary conditions

If just Ghost Particles shall be considered for the final endpoint simulation,
then no any additional module is required for the particles packing algorithm.

However, if some other boundary conditions are needed, then their modules should
be loaded to close the system.

Special treatment requires the Free-surface, since it cannot be naturally
treated, like it is happening indeed in Computational Fluid Dynamics module.
Thus, it is strongly recommneded to create a special boundary just for the
particles shifting, which is subsequently neglected when the main simulation is
executed.

# Theoretical background

When a particle is close enough to the boundary, the convolution compact support becomes suddently truncated.

![SPHConvolution](rsc/doc/SPH/CFD/BC/FS.svg)

Thus we can split the compact support in the flow subset, $`\bar{\Omega}`$, and the complementary subset $`\Omega^{*}`$.
Obviously, $`\bar{\Omega}`$ is already populated with particles, so the convolution can be still practised.
The same cannot be said on the $`\Omega^{*}`$ subset.
Thus, a generic differential operator can be written as,

```latex
\left\langle \nabla \gamma \right\rangle_i = \sum_{j \in \bar{\Omega}} \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) V_j + \left\langle \nabla \gamma \right\rangle_i^{\Omega^{*}},
```

with,

```latex
\left\langle \nabla \gamma \right\rangle_i^{\Omega^{*}} \simeq \int_{\Omega^{*}} \nabla W(\boldsymbol{y} - \boldsymbol{r}_i; h) \mathrm{d}\boldsymbol{y}.
```

A number of formulations to deal with the approximation above have been described in the past.
This is still an open problem though.
