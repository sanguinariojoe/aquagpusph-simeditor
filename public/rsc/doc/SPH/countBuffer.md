# Count buffer particles

Compute the number of available buffer particles (`imove = -255`).
The buffer particles are intended to be stacked at the end of the particles
array, so it can be more easily consumed.
To this end, this module can be used to count the remaining amount of buffer
particles, $`n_{buffer}`$.
Then they can be consumed from $`n - n_{buffer}`$ on.

*Never consume buffer particles backwards from the end of the array!* Otherwise,
the remaining buffer particles will not be stacked at the end of the array,
making harder, if not impossible, to consume them.

## Usage

Select the tool after which you want to count the number of remaining buffer
particles. Then load this module.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
