# Computational Fluid Dynamics (CFD)

Modules to setup CFD pipelines, i.e. implementations based on the following governing equations for a generic $`i`$-th particle:

```latex
\left\langle \frac{\mathrm{d} \rho}{\mathrm{d} t} \right\rangle_i = 
- \rho_i \left\langle \nabla \cdot \boldsymbol{u} \right\rangle_i,
```
```latex
\left\langle \frac{\mathrm{d} \boldsymbol{u}}{\mathrm{d} t} \right\rangle_i = 
- \frac{\left\langle \nabla p \right\rangle_i}{\rho_i}
+ \frac{\mu}{\rho_i} \left\langle \Delta \boldsymbol{u} \right\rangle_i
+ \boldsymbol{g}.
```

The governing equations shall be closed with an Equation Of State (EOS), and a time integration scheme.
Eventually some terms can be added to the governing equations above, to improve the stabilility or the conservation, e.g. $`\delta`$-SPH of symmetric pressure gradient operator.

You can find many examples of simulations carried out with these CFD modules in the [AQUAgpusph gallery](http://canal.etsin.upm.es/aquagpusph/gallery/)

## Usage

To setup a CFD pipeline you must load first the "Basic Structure" module.
Later on you would probably add a proper boundary condition, and eventually some terms to improve the model behaviour.

All the CFD modules require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
