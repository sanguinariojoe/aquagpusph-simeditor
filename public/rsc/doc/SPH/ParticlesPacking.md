# Particles Packing

Particles packing is a methodology to improve the Initial Condition.
The methodology is based on looking for the best position of the particles,
based on the hypothesis,

```latex
\exists \Delta \boldsymbol{r}_i :
\left\vert \left\langle \nabla \gamma \right\rangle_i (\boldsymbol{r}_i + \Delta \boldsymbol{r}_i) \right\vert
<
\left\vert \left\langle \nabla \gamma \right\rangle_i (\boldsymbol{r}_i) \right\vert,
```

with,

```latex
\left\langle \nabla \gamma \right\rangle_i (\boldsymbol{r}_i) =
\sum_j \nabla W(\boldsymbol{r}_j - \boldsymbol{r}_i; h) V_j
```

It is indeed a complex problem since a new position shall be computed for each
$`i`$-th particle, which is indeed modifying
$`\left\langle \nabla \gamma \right\rangle_j`$ of all the neighbours close
enough.
[Colagrossi et al (2012)](https://www.sciencedirect.com/science/article/pii/S0010465512001051)
suggests a dynamic system to find those new positions, although here a gradient
descent minimization methodology is applied,

```latex
\Delta \boldsymbol{r}_{i, n + 1} =
- f V_i^(1 / d)
\left\langle \nabla \gamma \right\rangle_i (\boldsymbol{r}_i + \Delta \boldsymbol{r}_{i, n}),
```

with $`d`$ the number of dimensions and $`f`$ a factor to grant the model
convergence.

The equations above still require to add the boundary conditions.
Along this line, it is very convenient to add a solid wall all along the
free-surface belonging to a different particles set, which is not loaded
afterwards in the main simulation.

## Usage

To setup a particles packing you must load the "Basic Structure" module,
and eventually some boundary conditions.

The particles packing in AQUAgpusph is designed to be considered as a different
pipeline/simulation from the main simulation, i.e.

  1. A first rough initial condition is generated, for instance with a Python script.
  2. A particles packing pipeline is ran to refine the particles packing.
  3. The main simulation is run on top of the particles packing saved files.

All the particles packing modules require the AQUAgpusph resources folder, where
the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
