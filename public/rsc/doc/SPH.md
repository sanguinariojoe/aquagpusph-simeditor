# Smoothed-Particle Hydrodynamics (SPH)

Smoothed-Particle Hydrodynamics (SPH) based pipelines. SPH is a meshless is a computational method used for simulating the mechanics of continuum media.

In SPH the media is discretized in a set of particles to be treated in a Lagrangian way.

![SPHConvolution](rsc/doc/SPHConvolution.svg)

To this end, a convolution is defined,

```latex
\left\langle f \right\rangle(\boldsymbol{r}) = \sum_i f_i W(\boldsymbol{r}_i - \boldsymbol{r}; h) V_i,
```

with $`V_i = m_i / \rho_i`$ the volume of a generic ***i***-th particle, which is a function of the mass and density.
$`W`$ is a known function, so-called the kernel.
The kernel plays a major role in SPH, and must fulfills some minimum properties,

```latex
W(\boldsymbol{r}_i - \boldsymbol{r}; h) = W(\boldsymbol{r} - \boldsymbol{r}_i; h),
```
```latex
\int_{\mathcal{B}(\boldsymbol{x})} W(\boldsymbol{y} - \boldsymbol{x}; h) \mathrm{d}\boldsymbol{y} = 1,
```
```latex
W(\boldsymbol{y} - \boldsymbol{x}; h) = 0 \,\, \forall \,\, \vert \boldsymbol{y} - \boldsymbol{x} \vert > s \, h.
```

Thus, the kernel is a symmetric radial function with a compact support of radius $`s \, h`$, with $`s`$ an arbitrary integer.
Hence, integrating by parts SPH allows to compute differential operators loading such differential operator to the well known kernel,

```latex
\left\langle \nabla f \right\rangle(\boldsymbol{r}) = \sum_i f_i \nabla W(\boldsymbol{r}_i - \boldsymbol{r}; h) V_i,
```

although for consistency reasons the following differential operator is usually considered,

```latex
\left\langle \nabla f \right\rangle(\boldsymbol{r}) = \sum_i (f_i - f(\boldsymbol{r})) \nabla W(\boldsymbol{r}_i - \boldsymbol{r}; h) V_i,
```

Check the [wikipedia entry](https://en.wikipedia.org/wiki/Smoothed-particle_hydrodynamics) to learn more about SPH.
