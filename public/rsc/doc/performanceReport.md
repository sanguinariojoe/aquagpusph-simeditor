# Performance report

This is kind of a special report, which is collecting the CPU/GPU time required
to compute each single simulation time step, printing the information both in
the terminal and in a file named `Performance.dat`.

The collected variables are:

 - `t` : Simulation time instant
 - `elapsed` : Total time elapsed to compute the last time step
 - `average(elapsed)` : The average value of the instantaneous elapsed times
 - `variance(elapsed)` : The variance on the instantaneous elapsed times
 - `overhead` : Time spent in tasks not strictly related with the simulation computation
 - `average(elapsed)` : The average value of the instantaneous overheads
 - `variance(elapsed)` : The variance on the instantaneous overheads

## Usage

When this module is loaded, the performance report will be added right after the
selected/active tool, or the simulation step starting port otherwise.
It is recommended to add this tool at the very end of the pipeline to avoid
biased results.
