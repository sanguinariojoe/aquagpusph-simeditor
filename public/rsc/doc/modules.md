# Modules

Pipelines can be created in a modular way, loading a basic structure module, loading afterwards more modules afterwards to add new features.

While the original modules deployed within AQUAgpusph (so-called presets) can be loaded in this editor, it is strongly recommended to use the modules provided within this tool, since compatibility issues can be expected.

## Building new modules

This editor is mainly designed to build whole pipelines.
However, it can be conveniently considered to generate modules as well, requiring however [further actions](https://gitlab.com/sanguinariojoe/aquagpusph-simeditor/-/wikis/create-modules) to clean the files.
