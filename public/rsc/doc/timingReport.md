# Timing report

This module is adding both, an on-screen and an on-file, reports about the
simulation timing, printing the following variables:

 - `t` : Simulation time instant
 - `dt` : Simulation time step
 - `iter` : Simulation step index

## Usage

When this module is loaded, both tools will be appended right after the
selected/active tool, or the simulation step starting port otherwise.
