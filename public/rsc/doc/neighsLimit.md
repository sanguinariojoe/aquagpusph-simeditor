# Limit of neighbours

In some situations the number of neighbours can be a good simulation health
assessing tool.
This module check the number of neighbours of each particle, and if it exceeds
a specified quantity, the simulation will be finished.

## Usage

This module shall be loaded after the "Basic Structure".
After loading the module, the maximum tolerated number of neighbours variable,
`neighs_limit`, shall be set.
In general such number of neighbours depends on the particle and kernel sizes
ratio.
So it is recommended to proceed as follows:

 1. Load the "Basic Structure"
 2. Define both `h` (kernel size, required) and `dr` (distance between particles)
 3. Define `hfac` as `h` / `dr`
 4. Load this module

Following those steps, a number of neighbours twice the expected one is allowed.

This module require the AQUAgpusph resources folder, where the considered scripts are located.
Remember therefore to properly set the AQUAgpusph path in "Settings" tab:

![Setting AQUAgpusph folder](rsc/doc/SPH/SetRscFolder.png)
