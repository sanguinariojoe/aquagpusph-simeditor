#!/bin/bash

rm -f AQUAgpusph.save.*.xml freeslip.*.vtu freeslip.pvd sphere.*.vtu \
      sphere.pvd fluid.*.vtu fluid.pvd inflow.*.vtu inflow.pvd log.*.html \
      outflow.*.vtu outflow.pvd Performance.dat
