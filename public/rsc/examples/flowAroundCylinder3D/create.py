import numpy as np

# Input data
maxIter = 200000
Re      = 220.0
nx = 260
ny = 90
nz = 90
cx = nx / 4
cy = ny / 2
cz = ny / 2
r  = ny / 9
uLB  = 0.04
nulb = uLB * r / Re
tau  = 3. * nulb + 0.5
print("nu = {}".format(nulb))
print("tau = {}".format(tau))

# Setup the grid
x = np.arange(0, nx)
y = np.arange(0, ny)
z = np.arange(0, nz)
xv, yv, zv = np.meshgrid(x, y, z)
xv = xv.ravel()
yv = yv.ravel()
zv = zv.ravel()

# Normal
nxv = nyv = nzv = np.zeros(xv.shape)

# Velocity
uxv = uLB * (1.0 + 1e-4 * np.sin(zv / (nz - 1.0) * 2 * np.pi))
uyv = np.zeros(xv.shape)
uzv = np.zeros(xv.shape)

# Density
rhov = np.ones(xv.shape)

# Divide by sets
iset = np.zeros(xv.shape, dtype=int)
iset[xv == 0]      = 1                                       # Inflow
iset[xv == nx - 1] = 2                                       # Outlet
iset[yv == 0]      = 3                                       # Free-slip
iset[yv == ny - 1] = 3                                       # Free-slip
iset[zv == 0]      = 3                                       # Free-slip
iset[zv == nz - 1] = 3                                       # Free-slip
iset[(xv - cx)**2 + (yv - cy)**2 + (zv - cz)**2 < r**2] = 4  # No-slip

# Normals
nxv = np.zeros(xv.shape)
nyv = np.zeros(xv.shape)
nzv = np.zeros(xv.shape)
nxv[xv == 0] = -1                             # Inflow
nxv[xv == nx - 1] = 1                         # Outlet
nyv[yv == 0] = -1                             # Free-slip
nxv[yv == 0] = 0
nyv[yv == ny - 1] = 1                         # Free-slip
nxv[yv == ny - 1] = 0
nzv[zv == 0] = -1                             # Free-slip
nxv[zv == 0] = 0
nzv[zv == nz - 1] = 1                         # Free-slip
nxv[zv == nz - 1] = 0

# Write the particle sets
for s in range(np.max(iset) + 1):
    mask = iset == s
    n = np.sum(mask.astype(np.int))
    if not n:
        continue
    print("Set {} has {} particles...".format(s, n))
    # Write the data file
    if s == np.max(iset):
        uxv[mask] = 0
        uyv[mask] = 0
    data = np.asarray([
        xv[mask] - cx,
        yv[mask] - cy,
        zv[mask] - cz,
        np.zeros(zv[mask].shape),
        nxv[mask],
        nyv[mask],
        nzv[mask],
        np.zeros(nzv[mask].shape),
        uxv[mask],
        uyv[mask],
        uzv[mask],
        np.zeros(uzv[mask].shape),
        rhov[mask],
    ]).T
    np.savetxt('set{}.dat'.format(s), data, delimiter=',', fmt='%g')
