/*
 *  This file is part of AQUAgpusph, a free CFD program based on SPH.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  AQUAgpusph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AQUAgpusph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AQUAgpusph.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @addtogroup basic
 * @{
 */

/** @file
 *  @brief Compute the approrpiate neighbours to can compute gradients of
 * magnitudes.
 */

#include "resources/Scripts/types/types.h"


#ifndef OMEGA_COMPLEMENTARY
    /** @def OMEGA_COMPLEMENTARY
     * @brief Expression to compute the "complementary" relaxation frequency.
     * 
     * In the original Two Relaxation Times (TRT) model, the following
     * expression is applied:
     *
     * \f$ f^{*}_i = f_i - \omega_s \left(f^s_i - f^{s, eq}_i \right)
     *                   - \omega_a \left(f^s_i - f^{s, eq}_i \right), \f$
     *
     * requiring the definition of 2 relaxation frequencies. However, the
     * expression can be rearranged as
     *
     * \f$ f^{*}_i = f_i - \omega \left(f_i - f^{eq}_i \right)
     *                   - \omega2 \left(\bar{f}_i - \bar{f}^{eq}_i \right) \f$
     *
     * with \f$ \bar{f}_i \f$ the probability function associated to the
     * opposite stream velocity, \f$ \bar{\mathbf{c}}_i = -\mathbf{c}_i \f$. The
     * new relaxation frequency, \f$ \omega2 \f$, is the "complementary" one.
     */
    #define OMEGA_COMPLEMENTARY 0.1f*(2.f-omega)/(8.f-omega)
#endif


/** @brief Collide the particles associated to a probability function.
 * 
 * @param f Original particles probability
 * @param f_eq Particles probability at the equilibirum state
 * @param omega Relaxation frequency, \f$ \omega = \frac{1}{\tau} \f$
 */
vec2 collide(vec2 f, vec2 f_eq, float omega)
{
    if (omega > 0.999f) {
        return f_eq;
    }
    float omega2 = OMEGA_COMPLEMENTARY;
    vec2 f_neq = f_eq - f;
    return clamp(f + omega * f_neq + omega2 * f_neq.yx, 0.f, 1.f);
}


/** @def COLLIDE_ENTRY
 * @brief Automatic way to create entry functions for all the probability
 * functions.
 *
 * A new collider function is never generated for probability function
 * \f$ f_0 \f$.
 * The finally generated functions will be named collide_fX(), with X the index
 * of the probability function to be handled.
 * 
 * @see collide()
 */
#define COLLIDE_ENTRY(idx)                                              \
    __kernel void collide_f##idx(__global vec2 *f##idx##_in,            \
                                 const __global vec2 *f##idx,           \
                                 const __global vec2* f##idx##_eq,      \
                                 const __global float* omega,           \
                                 uint N)                                \
    {                                                                   \
        uint i = get_global_id(0);                                      \
        if(i >= N)                                                      \
            return;                                                     \
        f##idx##_in[i] = collide(f##idx[i], f##idx##_eq[i], omega[i]);  \
    }

COLLIDE_ENTRY(1);
COLLIDE_ENTRY(2);
COLLIDE_ENTRY(3);
COLLIDE_ENTRY(4);
COLLIDE_ENTRY(5);
COLLIDE_ENTRY(6);
COLLIDE_ENTRY(7);
COLLIDE_ENTRY(8);
COLLIDE_ENTRY(9);

/**
 * @}
 */
