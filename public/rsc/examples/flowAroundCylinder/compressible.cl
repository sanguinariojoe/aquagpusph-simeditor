/*
 *  This file is part of AQUAgpusph, a free CFD program based on SPH.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  AQUAgpusph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AQUAgpusph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AQUAgpusph.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @addtogroup basic
 * @{
 */

/** @file
 *  @brief Makes the fluid compressible, i.e.
 * \f$ \mathbf{u} = \frac{\sum c_i f_i}{\sum f_i} \f$
 */

#include "resources/Scripts/types/types.h"

/** @brief Make the fluid compressible.
 * 
 * While in incompressible approach the velocity can be computed as
 * \f$ \mathbf{u} = \sum c_i f_i \f$, in compressible flows the velocity shall
 * be divided by the density, \f$ \mathbf{u} = \frac{\sum c_i f_i}{\sum f_i} \f$
 *
 * @param rho Density, \f$ \rho \f$
 * @param u Velocity, \f$ \mathbf{u} \f$
 * @param N Number of cells.
 */
__kernel void entry(const __global float *rho,
                    __global vec *u,
                    uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    u[i] /= rho[i];
}

/**
 * @}
 */
