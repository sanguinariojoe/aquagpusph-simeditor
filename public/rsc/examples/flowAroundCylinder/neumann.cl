/*
 *  This file is part of AQUAgpusph, a free CFD program based on SPH.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  AQUAgpusph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AQUAgpusph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AQUAgpusph.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @addtogroup basic
 * @{
 */

/** @file
 *  @brief Set fixed gradient values at the boundary (Neumann BC)
 */

#include "resources/Scripts/types/types.h"

/** @brief Enforce density at the boundary, by means of its gradient value.
 *
 * @param iset Set of particles index.
 * @param bc_gradrho Density gradient to be enforced at each cell.
 * @param bc_dr Distance to the first neighbour along the normal direction.
 * @param neigh1 1st neighbour along the normal direction
 * @param neigh2 2nd neighbour along the normal direction
 * @param rho Cell density, \f$ rho \f$
 * @param bc_iset Particles set in which density shall be enforced.
 * @param N Number of particles.
 */
__kernel void rho(const __global uint* iset,
                  const __global float* bc_gradrho,
                  const __global float* bc_dr,
                  const __global uint* neigh1,
                  const __global uint* neigh2,
                  __global float *rho,
                  uint bc_iset,
                  uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    const float r1 = rho[neigh1[i]];
    const float r2 = neigh2[i] < N ? rho[neigh2[i]] : r1;

    rho[i] = (4.f * r1 - r2 - 2.f * bc_dr[i] * bc_gradrho[i]) / 3.f;
}

/** @brief Enforce null density gradient at the boundary.
 *
 * @param iset Set of particles index.
 * @param neigh1 1st neighbour along the normal direction
 * @param neigh2 2nd neighbour along the normal direction
 * @param rho Cell density, \f$ rho \f$
 * @param bc_iset Particles set in which density shall be enforced.
 * @param N Number of particles.
 */
__kernel void rho0(const __global uint* iset,
                   const __global uint* neigh1,
                   const __global uint* neigh2,
                   __global float *rho,
                   uint bc_iset,
                   uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    const float r1 = rho[neigh1[i]];
    const float r2 = neigh2[i] < N ? rho[neigh2[i]] : r1;

    rho[i] = (4.f * r1 - r2) / 3.f;
}

/** @brief Enforce velocity at the boundary, by means of its gradient value.
 *
 * @param iset Set of particles index.
 * @param bc_gradu Velocity gradient to be enforced at each cell.
 * @param bc_dr Distance to the first neighbour along the normal direction.
 * @param neigh1 1st neighbour along the normal direction
 * @param neigh2 2nd neighbour along the normal direction
 * @param u Cell velocity, \f$ \mathbf{u} \f$
 * @param bc_iset Particles set in which velocity shall be enforced.
 * @param N Number of particles.
 */
__kernel void u(const __global uint* iset,
                const __global vec* bc_gradu,
                const __global float* bc_dr,
                const __global uint* neigh1,
                const __global uint* neigh2,
                __global vec *u,
                uint bc_iset,
                uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    const vec u1 = u[neigh1[i]];
    const vec u2 = neigh2[i] < N ? u[neigh2[i]] : u1;

    u[i] = (4.f * u1 - u2 - 2.f * bc_dr[i] * bc_gradu[i]) / 3.f;
}

/** @brief Enforce null velocity gradient at the boundary.
 *
 * @param iset Set of particles index.
 * @param neigh1 1st neighbour along the normal direction
 * @param neigh2 2nd neighbour along the normal direction
 * @param u Cell velocity, \f$ \mathbf{u} \f$
 * @param bc_iset Particles set in which velocity shall be enforced.
 * @param N Number of particles.
 */
__kernel void u0(const __global uint* iset,
                 const __global uint* neigh1,
                 const __global uint* neigh2,
                 __global vec *u,
                 uint bc_iset,
                 uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    const vec u1 = u[neigh1[i]];
    const vec u2 = neigh2[i] < N ? u[neigh2[i]] : u1;

    u[i] = (4.f * u1 - u2) / 3.f;
}

/** @brief Enforce velocity normal component at the boundary, by means of its
 * gradient value.
 *
 * @param iset Set of particles index.
 * @param bc_gradun Velocity normal component gradient to be enforced at each
 * cell
 * @param bc_dr Distance to the first neighbour along the normal direction.
 * @param normal Boundary normal, \f$ \mathbf{n} \f$
 * @param neigh1 1st neighbour along the normal direction
 * @param neigh2 2nd neighbour along the normal direction
 * @param u Cell velocity, \f$ \mathbf{u} \f$
 * @param bc_iset Particles set in which velocity shall be enforced.
 * @param N Number of particles.
 */
__kernel void un(const __global uint* iset,
                 const __global vec* bc_gradun,
                 const __global float* bc_dr,
                 const __global vec *normal,
                 const __global uint* neigh1,
                 const __global uint* neigh2,
                 __global vec *u,
                 uint bc_iset,
                 uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    const vec n = normal[i];
    const vec u1 = dot(u[neigh1[i]], n) * n;
    const vec u2 = neigh2[i] < N ? dot(u[neigh2[i]], n) * n : u1;

    vec ut = u[i] - dot(u[i], n) * n;
    u[i] = ut + (4.f * u1 - u2 - 2.f * bc_dr[i] * bc_gradun[i]) / 3.f;
}

/** @brief Enforce null velocity normal component gradient at the boundary.
 *
 * @param iset Set of particles index.
 * @param normal Boundary normal, \f$ \mathbf{n} \f$
 * @param neigh1 1st neighbour along the normal direction
 * @param neigh2 2nd neighbour along the normal direction
 * @param u Cell velocity, \f$ \mathbf{u} \f$
 * @param bc_iset Particles set in which velocity shall be enforced.
 * @param N Number of particles.
 */
__kernel void un0(const __global uint* iset,
                  const __global vec *normal,
                  const __global uint* neigh1,
                  const __global uint* neigh2,
                  __global vec *u,
                  uint bc_iset,
                  uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    const vec n = normal[i];
    const vec u1 = dot(u[neigh1[i]], n) * n;
    const vec u2 = neigh2[i] < N ? dot(u[neigh2[i]], n) * n : u1;

    vec ut = u[i] - dot(u[i], n) * n;
    u[i] = ut + (4.f * u1 - u2) / 3.f;
}

/** @brief Enforce velocity tangent component at the boundary, by means of its
 * gradient value.
 *
 * @param iset Set of particles index.
 * @param bc_gradut Velocity tangent component gradient to be enforced at each
 * cell
 * @param bc_dr Distance to the first neighbour along the normal direction.
 * @param normal Boundary normal, \f$ \mathbf{n} \f$
 * @param neigh1 1st neighbour along the normal direction
 * @param neigh2 2nd neighbour along the normal direction
 * @param u Cell velocity, \f$ \mathbf{u} \f$
 * @param bc_iset Particles set in which velocity shall be enforced.
 * @param N Number of particles.
 */
__kernel void ut(const __global uint* iset,
                 const __global vec* bc_gradut,
                 const __global float* bc_dr,
                 const __global vec *normal,
                 const __global uint* neigh1,
                 const __global uint* neigh2,
                 __global vec *u,
                 uint bc_iset,
                 uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    const vec n = normal[i];
    const vec u1 = u[neigh1[i]] - dot(u[neigh1[i]], n) * n;
    const vec u2 = neigh2[i] < N ? u[neigh2[i]] - dot(u[neigh2[i]], n) * n : u1;

    vec un = dot(u[i], n) * n;
    u[i] = un + (4.f * u1 - u2 - 2.f * bc_dr[i] * bc_gradut[i]) / 3.f;
}

/** @brief Enforce null velocity tangent component gradient at the boundary.
 *
 * @param iset Set of particles index.
 * @param normal Boundary normal, \f$ \mathbf{n} \f$
 * @param neigh1 1st neighbour along the normal direction
 * @param neigh2 2nd neighbour along the normal direction
 * @param u Cell velocity, \f$ \mathbf{u} \f$
 * @param bc_iset Particles set in which velocity shall be enforced.
 * @param N Number of particles.
 */
__kernel void ut0(const __global uint* iset,
                  const __global vec *normal,
                  const __global uint* neigh1,
                  const __global uint* neigh2,
                  __global vec *u,
                  uint bc_iset,
                  uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    const vec n = normal[i];
    const vec u1 = u[neigh1[i]] - dot(u[neigh1[i]], n) * n;
    const vec u2 = neigh2[i] < N ? u[neigh2[i]] - dot(u[neigh2[i]], n) * n : u1;

    vec un = dot(u[i], n) * n;
    u[i] = un + (4.f * u1 - u2) / 3.f;
}

/**
 * @}
 */
