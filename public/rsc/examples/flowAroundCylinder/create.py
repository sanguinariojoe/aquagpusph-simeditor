import numpy as np

# Input data
maxIter = 200000
Re      = 220.0
nx = 520
ny = 180
cx = nx / 4
cy = ny / 2
r  = ny / 9
uLB  = 0.04
nulb = uLB * r / Re
tau  = 3. * nulb + 0.5
print("nu = {}".format(nulb))
print("tau = {}".format(tau))

# Setup the grid
x = np.arange(0, nx)
y = np.arange(0, ny)
xv, yv = np.meshgrid(x, y)
xv = xv.ravel()
yv = yv.ravel()

# Normal
nxv = np.zeros(xv.shape)
nyv = np.zeros(xv.shape)

# Velocity
uxv = uLB * (1.0 + 1e-4 * np.sin(yv / (ny - 1.0) * 2 * np.pi))
uyv = np.zeros(xv.shape)

# Density
rhov = np.ones(xv.shape)

# Divide by sets
iset = np.zeros(xv.shape, dtype=int)
iset[xv == 0]      = 1                        # Inflow
iset[xv == nx - 1] = 2                        # Outlet
iset[yv == 0]      = 3                        # Free-slip
iset[yv == ny - 1] = 4                        # Free-slip
iset[(xv - cx)**2 + (yv - cy)**2 < r**2] = 5  # No-slip

# Normal per set
nxs = [0, -1, 1,  0, 0, 0]
nys = [0,  0, 0, -1, 1, 0]

# Read the particles set template


# Write the particle sets
for s in range(np.max(iset) + 1):
    mask = iset == s
    n = np.sum(mask.astype(np.int))
    if not n:
        continue
    print("Set {} has {} particles...".format(s, n))
    # Write the data file
    nxv[mask] = nxs[s]
    nyv[mask] = nys[s]
    if s == np.max(iset):
        uxv[mask] = 0
        uyv[mask] = 0
    data = np.asarray([
        xv[mask] - cx,
        yv[mask] - cy,
        nxv[mask],
        nyv[mask],
        uxv[mask],
        uyv[mask],
        rhov[mask],
    ]).T
    np.savetxt('set{}.dat'.format(s), data, delimiter=',', fmt='%g')
    # And the XML file
    
