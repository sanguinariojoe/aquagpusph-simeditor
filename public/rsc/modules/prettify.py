import argparse
import xml.dom.minidom


parser = argparse.ArgumentParser(description='Prettify XML file')
parser.add_argument('input', metavar='input', type=str,
                    help='Input XML file')
parser.add_argument('output', metavar='output', type=str,
                    help='Output XML file')
args = parser.parse_args()

xml_in = xml.dom.minidom.parse(args.input)
with open(args.output, 'w') as xml_out:
    xml_out.write(xml_in.toprettyxml())
