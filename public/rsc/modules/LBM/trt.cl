/*
 *  This file is part of AQUAgpusph, a free CFD program based on SPH.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  AQUAgpusph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AQUAgpusph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AQUAgpusph.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @addtogroup basic
 * @{
 */

/** @file
 *  @brief Compute the approrpiate neighbours to can compute gradients of
 * magnitudes.
 */

#include "resources/Scripts/types/types.h"


#ifndef OMEGA_COMPLEMENTARY
    /** @def OMEGA_COMPLEMENTARY
     * @brief Expression to compute the "complementary" relaxation frequency.
     * 
     * In the original Two Relaxation Times (TRT) model, the following
     * expression is applied:
     *
     * \f$ f^{*}_i = f_i - \omega_s \left(f^s_i - f^{s, eq}_i \right)
     *                   - \omega_a \left(f^s_i - f^{s, eq}_i \right), \f$
     *
     * requiring the definition of 2 relaxation frequencies. However, the
     * expression can be rearranged as
     *
     * \f$ f^{*}_i = f_i - \omega \left(f_i - f^{eq}_i \right)
     *                   - \omega_2 \left(\bar{f}_i - \bar{f}^{eq}_i \right) \f$
     *
     * with \f$ \bar{f}_i \f$ the probability function associated to the
     * opposite stream velocity, \f$ \bar{\mathbf{c}}_i = -\mathbf{c}_i \f$. The
     * new relaxation frequency, \f$ \omega_2 \f$, is the "complementary" one.
     *
     * In order to get \f$ \omega = \frac{1}{2} (\omega_s + \omega_a) \f$ we set
     * \f$ \omega_2 = \omega - \omega_a \f$. However, in "Servan‐Camas, B., and
     * F. T.‐C. Tsai (2008), Lattice Boltzmann method with two relaxation times
     * for advection‐diffusion equation: Third order analysis and stability
     * analysis, Adv. Water Resour., 31(8), 1113–1126,
     * doi:10.1016/j.advwatres.2008.05.001" they found \f$ \omega_a = 1 \f$
     * gives the best results, so \f$ \omega_2 = \omega - 1 \f$ would be chosen.
     * But such a value may be big enough to turn the simulation unstable again
     */
    #define OMEGA_COMPLEMENTARY 1.E-2f*(omega - 1.f)
#endif


/** @brief Collide the particles associated to a probability function.
 * 
 * @param f Original particles probability
 * @param f_eq Particles probability at the equilibirum state
 * @param omega Relaxation frequency, \f$ \omega = \frac{1}{\tau} \f$
 */
vec2 collide(vec2 f, vec2 f_eq, float omega)
{
    if (omega > 0.999f) {
        return f_eq;
    }
    float omega2 = OMEGA_COMPLEMENTARY;
    vec2 f_neq = f_eq - f;
    return clamp(f + omega * f_neq + omega2 * f_neq.yx, 0.f, 1.f);
}


/** @def COLLIDE_ENTRY
 * @brief Automatic way to create entry functions for all the probability
 * functions.
 *
 * A new collider function is never generated for probability function
 * \f$ f_0 \f$.
 * The finally generated functions will be named collide_fX(), with X the index
 * of the probability function to be handled.
 * 
 * @see collide()
 */
#define COLLIDE_ENTRY(idx)                                              \
    __kernel void collide_f##idx(__global vec2 *f##idx##_in,            \
                                 const __global vec2 *f##idx,           \
                                 const __global vec2* f##idx##_eq,      \
                                 const __global float* omega,           \
                                 uint N)                                \
    {                                                                   \
        uint i = get_global_id(0);                                      \
        if(i >= N)                                                      \
            return;                                                     \
        f##idx##_in[i] = collide(f##idx[i], f##idx##_eq[i], omega[i]);  \
    }

COLLIDE_ENTRY(1);
COLLIDE_ENTRY(2);
COLLIDE_ENTRY(3);
COLLIDE_ENTRY(4);
COLLIDE_ENTRY(5);
COLLIDE_ENTRY(6);
COLLIDE_ENTRY(7);
COLLIDE_ENTRY(8);
COLLIDE_ENTRY(9);

/**
 * @}
 */
