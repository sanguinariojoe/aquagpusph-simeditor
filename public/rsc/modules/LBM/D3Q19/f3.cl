/*
 *  This file is part of AQUAgpusph, a free CFD program based on SPH.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  AQUAgpusph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AQUAgpusph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AQUAgpusph.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @addtogroup basic
 * @{
 */

/** @file
 * @brief Handles the probability functions, \f$ f_3 \f$.
 * 
 * In AQUAgpusph, the probability functions are paired in such a way that
 * \f$ f_3 \f$ contains the probability functions associated to the velocities
 * \f$ c_3 \f$ and \f$ -c_3 \f$.
 */

#include "resources/Scripts/types/types.h"


/** @brief Helper function to know if 2 vectors are the same one, within a
 * provided tolerance.
 *
 * In 3D vectors just x,y,z components are considered (w one is neglected)
 * 
 * @param a First vector to compare
 * @param b Second vector to compare
 * @param tol Acceptable squared tolerance
 */
bool is_same_vec(vec_xyz a, vec_xyz b, float tol)
{
    vec_xyz v = a - b;
    float d2 = dot(v, v);
    if (d2 <= tol * tol)
        return true;
    return false;
}

/** @brief Initialize the probability functions, \f$ f_3 \f$, as their values at
 * the equilibirum state.
 * 
 * Using this you can start a simulation providing just the density and velocity
 * at each cell, instead of a value for each probability function
 *
 * @param f3 Particles probability, \f$ f_3 \f$
 * @param f3_eq Particles probability, \f$ f_3 \f$, at the equilibirum state
 * @param N Number of cells.
 */
__kernel void init(__global vec2* f3,
                   const __global vec2* f3_eq,
                   uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    f3[i] = f3_eq[i];
}

/** @brief Compute the particles probability, \f$ f_3 \f$, at the equilibirum
 * state.
 *
 * @param f3_eq Particles probability, \f$ f_3 \f$, at the equilibirum state
 * @param rho Cell density, \f$ \rho \f$
 * @param u Cell average velocity, \f$ \mathbf{u} \f$
 * @param c3 Particles velocity associated to the probability \f$ f_3 \f$
 * @param w3 Weigth of the probability \f$ f_3 \f$ to the equilibrium
 * @param N Number of cells.
 */
__kernel void equilibrium(__global vec2* f3_eq,
                          const __global float* rho,
                          const __global vec* u,
                          vec c3,
                          float w3,
                          float cs,
                          uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    float cs2 = 1.f / (cs * cs);
    float uc = cs2 * dot(c3.XYZ, u[i].XYZ);
    float u2 = 0.5f * cs2 * dot(u[i].XYZ, u[i].XYZ);

    f3_eq[i].x = w3 * rho[i] * (1.f + uc + 0.5f * uc * uc - u2);
    f3_eq[i].y = w3 * rho[i] * (1.f - uc + 0.5f * uc * uc - u2);
}

/** @brief Sort the probability function, \f$ f_3 \f$, by the cell indexes
 *
 * @param f3_in Unsorted particles probability, \f$ f_3 \f$
 * @param f3 Sorted particles probability, \f$ f_3 \f$
 * @param id_sorted Permutations list from the unsorted space to the sorted
 * one.
 * @param N Number of particles.
 */
__kernel void sort(const __global vec2 *f3_in, __global vec2 *f3,
                   const __global unit *id_sorted,
                   uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    f3[id_sorted[i]] = f3_in[i];
}

/** @brief Find the appropriate neighbours for the probability function,
 * \f$ f_3 \f$.
 *
 * @param to3 Neighbour cell where the particles associated to \f$ f_3 \f$ are
 * moving to.
 * @param from3 Neighbour cell from which the particles associated to
 * \f$ f_3 \f$ are coming.
 * @param r Position \f$ \mathbf{r} \f$.
 * @param c3 Particles velocity associated to the probability \f$ f_3 \f$
 * @param icell Cell where each particle is located.
 * @param ihoc Head of chain for each cell (first particle found).
 * @param N Number of particles.
 * @param n_cells Number of cells in each direction
 */
__kernel void neighbours(__global uivec2 *to3,
                         __global uivec2 *from3,
                         const __global vec* r,
                         vec c3,
                         const __global uint *icell,
                         const __global uint *ihoc,
                         uint N,
                         uivec4 n_cells)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    // Mark them as not found
    to3[i] = (uivec2)(N, N);
    from3[i] = (uivec2)(N, N);

    // Locate a candidate
    const vec_xyz r_i = r[i].XYZ;
    BEGIN_LOOP_OVER_NEIGHS(){
        if(i == j){
            j++;
            continue;
        }
        const vec_xyz r_ij = r[j].XYZ - r_i;
        if (is_same_vec(r_ij, c3.XYZ, 0.1f)) {
            to3[i].x = j;
            from3[i].y = j;
        }
        if (is_same_vec(r_ij, -c3.XYZ, 0.1f)) {
            from3[i].x = j;
            to3[i].y = j;
        }
    }END_LOOP_OVER_NEIGHS()
}

/** @brief Collide the particles associated to the probability function,
 * \f$ f_3 \f$.
 * 
 * @param f3_eq Particles probability, \f$ f_3 \f$, at the equilibirum state
 * @param f3_in Collided particles probability, \f$ f_3 \f$
 * @param f3 Original particles probability, \f$ f_3 \f$
 * @param omega Relaxation frequency, \f$ \omega = \frac{1}{\tau} \f$
 * @param N Number of particles.
 */
__kernel void collide(__global vec2 *f3_in,
                      const __global vec2 *f3,
                      const __global vec2* f3_eq,
                      const __global float* omega,
                      uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    f3_in[i] = clamp(f3[i] + omega[i] * (f3_eq[i] - f3[i]), 0.f, 1.f);
}

/** @brief Stream the particles associated to the probability function,
 * \f$ f_3 \f$.
 *
 * @param f3 Collided and Streamed particles probability, \f$ f_3 \f$
 * @param f3_in Collided particles probability, \f$ f_3 \f$
 * @param to3 Neighbour cell where the particles associated to \f$ f_3 \f$ are
 * moving to.
 * @param N Number of particles.
 */
__kernel void stream(__global vec2 *f3,
                     const __global vec2 *f3_in,
                     const __global uivec2 *to3,
                     uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    if (to3[i].x < N) {
        f3[to3[i].x].x = f3_in[i].x;
    }
    if (to3[i].y < N) {
        f3[to3[i].y].y = f3_in[i].y;
    }
}

/** @brief Add the contribution of the probability function, \f$ f_3 \f$, to
 * the macroscopic density and velocity.
 * 
 * \f$ \rho = \sum_i f_i \f$
 * \f$ \mathbf{u} = \sum_i \mathbf{c}_i f_i \f$
 *
 * @param f3 Collided and Streamed particles probability, \f$ f_3 \f$
 * @param rho Density, \f$ \rho \f$
 * @param u Velocity, \f$ \mathbf{u} \f$
 * @param c3 Particles velocity associated to the probability \f$ f_3 \f$
 * @param N Number of particles.
 */
__kernel void rhou(const __global vec2 *f3,
                   __global float *rho,
                   __global vec *u,
                   vec c3,
                   uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    rho[i] += f3[i].x + f3[i].y;
    u[i].XYZ += c3.XYZ * (f3[i].x - f3[i].y);
}

/**
 * @}
 */
