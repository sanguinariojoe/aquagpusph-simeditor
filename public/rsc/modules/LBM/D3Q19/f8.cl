/*
 *  This file is part of AQUAgpusph, a free CFD program based on SPH.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  AQUAgpusph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AQUAgpusph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AQUAgpusph.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @addtogroup basic
 * @{
 */

/** @file
 * @brief Handles the probability functions, \f$ f_8 \f$.
 * 
 * In AQUAgpusph, the probability functions are paired in such a way that
 * \f$ f_8 \f$ contains the probability functions associated to the velocities
 * \f$ c_8 \f$ and \f$ -c_8 \f$.
 */

#include "resources/Scripts/types/types.h"


/** @brief Helper function to know if 2 vectors are the same one, within a
 * provided tolerance.
 *
 * In 3D vectors just x,y,z components are considered (w one is neglected)
 * 
 * @param a First vector to compare
 * @param b Second vector to compare
 * @param tol Acceptable squared tolerance
 */
bool is_same_vec(vec_xyz a, vec_xyz b, float tol)
{
    vec_xyz v = a - b;
    float d2 = dot(v, v);
    if (d2 <= tol * tol)
        return true;
    return false;
}

/** @brief Initialize the probability functions, \f$ f_8 \f$, as their values at
 * the equilibirum state.
 * 
 * Using this you can start a simulation providing just the density and velocity
 * at each cell, instead of a value for each probability function
 *
 * @param f8 Particles probability, \f$ f_8 \f$
 * @param f8_eq Particles probability, \f$ f_8 \f$, at the equilibirum state
 * @param N Number of cells.
 */
__kernel void init(__global vec2* f8,
                   const __global vec2* f8_eq,
                   uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    f8[i] = f8_eq[i];
}

/** @brief Compute the particles probability, \f$ f_8 \f$, at the equilibirum
 * state.
 *
 * @param f8_eq Particles probability, \f$ f_8 \f$, at the equilibirum state
 * @param rho Cell density, \f$ \rho \f$
 * @param u Cell average velocity, \f$ \mathbf{u} \f$
 * @param c8 Particles velocity associated to the probability \f$ f_8 \f$
 * @param w8 Weigth of the probability \f$ f_8 \f$ to the equilibrium
 * @param N Number of cells.
 */
__kernel void equilibrium(__global vec2* f8_eq,
                          const __global float* rho,
                          const __global vec* u,
                          vec c8,
                          float w8,
                          float cs,
                          uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    float cs2 = 1.f / (cs * cs);
    float uc = cs2 * dot(c8.XYZ, u[i].XYZ);
    float u2 = 0.5f * cs2 * dot(u[i].XYZ, u[i].XYZ);

    f8_eq[i].x = w8 * rho[i] * (1.f + uc + 0.5f * uc * uc - u2);
    f8_eq[i].y = w8 * rho[i] * (1.f - uc + 0.5f * uc * uc - u2);
}

/** @brief Sort the probability function, \f$ f_8 \f$, by the cell indexes
 *
 * @param f8_in Unsorted particles probability, \f$ f_8 \f$
 * @param f8 Sorted particles probability, \f$ f_8 \f$
 * @param id_sorted Permutations list from the unsorted space to the sorted
 * one.
 * @param N Number of particles.
 */
__kernel void sort(const __global vec2 *f8_in, __global vec2 *f8,
                   const __global unit *id_sorted,
                   uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    f8[id_sorted[i]] = f8_in[i];
}

/** @brief Find the appropriate neighbours for the probability function,
 * \f$ f_8 \f$.
 *
 * @param to8 Neighbour cell where the particles associated to \f$ f_8 \f$ are
 * moving to.
 * @param from8 Neighbour cell from which the particles associated to
 * \f$ f_8 \f$ are coming.
 * @param r Position \f$ \mathbf{r} \f$.
 * @param c8 Particles velocity associated to the probability \f$ f_8 \f$
 * @param icell Cell where each particle is located.
 * @param ihoc Head of chain for each cell (first particle found).
 * @param N Number of particles.
 * @param n_cells Number of cells in each direction
 */
__kernel void neighbours(__global uivec2 *to8,
                         __global uivec2 *from8,
                         const __global vec* r,
                         vec c8,
                         const __global uint *icell,
                         const __global uint *ihoc,
                         uint N,
                         uivec4 n_cells)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    // Mark them as not found
    to8[i] = (uivec2)(N, N);
    from8[i] = (uivec2)(N, N);

    // Locate a candidate
    const vec_xyz r_i = r[i].XYZ;
    BEGIN_LOOP_OVER_NEIGHS(){
        if(i == j){
            j++;
            continue;
        }
        const vec_xyz r_ij = r[j].XYZ - r_i;
        if (is_same_vec(r_ij, c8.XYZ, 0.1f)) {
            to8[i].x = j;
            from8[i].y = j;
        }
        if (is_same_vec(r_ij, -c8.XYZ, 0.1f)) {
            from8[i].x = j;
            to8[i].y = j;
        }
    }END_LOOP_OVER_NEIGHS()
}

/** @brief Collide the particles associated to the probability function,
 * \f$ f_8 \f$.
 * 
 * @param f8_eq Particles probability, \f$ f_8 \f$, at the equilibirum state
 * @param f8_in Collided particles probability, \f$ f_8 \f$
 * @param f8 Original particles probability, \f$ f_8 \f$
 * @param omega Relaxation frequency, \f$ \omega = \frac{1}{\tau} \f$
 * @param N Number of particles.
 */
__kernel void collide(__global vec2 *f8_in,
                      const __global vec2 *f8,
                      const __global vec2* f8_eq,
                      const __global float* omega,
                      uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    f8_in[i] = clamp(f8[i] + omega[i] * (f8_eq[i] - f8[i]), 0.f, 1.f);
}

/** @brief Stream the particles associated to the probability function,
 * \f$ f_8 \f$.
 *
 * @param f8 Collided and Streamed particles probability, \f$ f_8 \f$
 * @param f8_in Collided particles probability, \f$ f_8 \f$
 * @param to8 Neighbour cell where the particles associated to \f$ f_8 \f$ are
 * moving to.
 * @param N Number of particles.
 */
__kernel void stream(__global vec2 *f8,
                     const __global vec2 *f8_in,
                     const __global uivec2 *to8,
                     uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    if (to8[i].x < N) {
        f8[to8[i].x].x = f8_in[i].x;
    }
    if (to8[i].y < N) {
        f8[to8[i].y].y = f8_in[i].y;
    }
}

/** @brief Add the contribution of the probability function, \f$ f_8 \f$, to
 * the macroscopic density and velocity.
 * 
 * \f$ \rho = \sum_i f_i \f$
 * \f$ \mathbf{u} = \sum_i \mathbf{c}_i f_i \f$
 *
 * @param f8 Collided and Streamed particles probability, \f$ f_8 \f$
 * @param rho Density, \f$ \rho \f$
 * @param u Velocity, \f$ \mathbf{u} \f$
 * @param c8 Particles velocity associated to the probability \f$ f_8 \f$
 * @param N Number of particles.
 */
__kernel void rhou(const __global vec2 *f8,
                   __global float *rho,
                   __global vec *u,
                   vec c8,
                   uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    rho[i] += f8[i].x + f8[i].y;
    u[i].XYZ += c8.XYZ * (f8[i].x - f8[i].y);
}

/**
 * @}
 */
