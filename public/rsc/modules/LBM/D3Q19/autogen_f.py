ci = [" 0,  0,  0,  0",
      " 1,  0,  0,  0",
      " 0,  1,  0,  0",
      " 0,  0,  1,  0",
      " 1,  1,  0,  0",
      " 1, -1,  0,  0",
      " 1,  0,  1,  0",
      " 1,  0, -1,  0",
      " 0,  1,  1,  0",
      " 0,  1, -1,  0"]
ci_l = [" 0.0 ",
        " 1.0 ",
        " 1.0 ",
        " 1.0 ",
        " 1.4142135623730951 ",
        " 1.4142135623730951 ",
        " 1.4142135623730951 ",
        " 1.4142135623730951 ",
        " 1.4142135623730951 ",
        " 1.4142135623730951 "]
wi = ["1 / 3"] + ["1 / 18"] * 3 + ["1 / 36"] * 6

changes = {"_1":"_{}",
           "f1":"f{}",
           "w1":"w{}",
           "c1":"c{}",
           "to1":"to{}",
           "from1":"from{}"}


def replacements(txt, i):
    for k in changes.keys():
        txt = txt.replace(k, changes[k].format(i))
    txt = txt.replace('value="' + ci[1] + '"', 'value="' + ci[i] + '"')
    txt = txt.replace('value="' + ci_l[1] + '"', 'value="' + ci_l[i] + '"')
    txt = txt.replace('value="' + wi[1] + '"', 'value="' + wi[i] + '"')
    return txt


with open("f1.xml", "r") as f:
    xml = f.read()
with open("f1.cl", "r") as f:
    cl = f.read()

for i in range(2, len(ci)):
    with open("f{}.xml".format(i), "w") as f:
        f.write(replacements(xml, i))
    with open("f{}.cl".format(i), "w") as f:
        f.write(replacements(cl, i))
