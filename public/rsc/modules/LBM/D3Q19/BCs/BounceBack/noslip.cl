/*
 *  This file is part of AQUAgpusph, a free CFD program based on SPH.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  AQUAgpusph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AQUAgpusph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AQUAgpusph.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @addtogroup basic
 * @{
 */

/** @file
 *  @brief Enforces no-slip boundary condition for a specific set of particles
 */

#include "resources/Scripts/types/types.h"

/** @brief Enforce no-slip boundary condition.
 *
 * No-slip boundary condition is enforced by a flip of all the probability
 * functions, reverting in fact the velocity of the cell.
 *
 * To maximizime compatibility, the job is done in several stages
 *
 * @param iset Set of particles index.
 * @param f1_in Collided particles probability, \f$ f_1 \f$
 * @param f1 Original particles probability, \f$ f_1 \f$
 * @param f2_in Collided particles probability, \f$ f_2 \f$
 * @param f2 Original particles probability, \f$ f_2 \f$
 * @param f3_in Collided particles probability, \f$ f_3 \f$
 * @param f3 Original particles probability, \f$ f_3 \f$
 * @param f4_in Collided particles probability, \f$ f_4 \f$
 * @param f4 Original particles probability, \f$ f_4 \f$
 * @param noslip_iset Particles set in which no-slip boundary condition shall
 * be enforced.
 * @param N Number of particles.
 */
__kernel void f_1_4(const __global uint* iset,
                    __global vec2 *f1_in, const __global vec2 *f1,
                    __global vec2 *f2_in, const __global vec2 *f2,
                    __global vec2 *f3_in, const __global vec2 *f3,
                    __global vec2 *f4_in, const __global vec2 *f4,
                    uint noslip_iset,
                    uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != noslip_iset)
        return;

    f1_in[i] = f1[i].yx;
    f2_in[i] = f2[i].yx;
    f3_in[i] = f3[i].yx;
    f4_in[i] = f4[i].yx;
}

/** @brief Enforce no-slip boundary condition.
 *
 * No-slip boundary condition is enforced by a flip of all the probability
 * functions, reverting in fact the velocity of the cell.
 *
 * To maximizime compatibility, the job is done in several stages
 *
 * @param iset Set of particles index.
 * @param f5_in Collided particles probability, \f$ f_5 \f$
 * @param f5 Original particles probability, \f$ f_5 \f$
 * @param f6_in Collided particles probability, \f$ f_6 \f$
 * @param f6 Original particles probability, \f$ f_6 \f$
 * @param f7_in Collided particles probability, \f$ f_7 \f$
 * @param f7 Original particles probability, \f$ f_7 \f$
 * @param f8_in Collided particles probability, \f$ f_8 \f$
 * @param f8 Original particles probability, \f$ f_8 \f$
 * @param f9_in Collided particles probability, \f$ f_9 \f$
 * @param f9 Original particles probability, \f$ f_9 \f$
 * @param noslip_iset Particles set in which no-slip boundary condition shall
 * be enforced.
 * @param N Number of particles.
 */
__kernel void f_5_9(const __global uint* iset,
                    __global vec2 *f5_in, const __global vec2 *f5,
                    __global vec2 *f6_in, const __global vec2 *f6,
                    __global vec2 *f7_in, const __global vec2 *f7,
                    __global vec2 *f8_in, const __global vec2 *f8,
                    __global vec2 *f9_in, const __global vec2 *f9,
                    uint noslip_iset,
                    uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != noslip_iset)
        return;

    f5_in[i] = f5[i].yx;
    f6_in[i] = f6[i].yx;
    f7_in[i] = f7[i].yx;
    f8_in[i] = f8[i].yx;
    f9_in[i] = f9[i].yx;
}

/**
 * @}
 */
