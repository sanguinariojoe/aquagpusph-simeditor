/*
 *  This file is part of AQUAgpusph, a free CFD program based on SPH.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  AQUAgpusph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AQUAgpusph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AQUAgpusph.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @addtogroup basic
 * @{
 */

/** @file
 *  @brief Set fixed values at the boundary (Dirichlet BC)
 */

#include "resources/Scripts/types/types.h"

/** @brief Enforce density at the boundary.
 *
 * @param iset Set of particles index.
 * @param bc_rho Density to be enforced at each cell.
 * @param rho Cell density, \f$ rho \f$
 * @param bc_iset Particles set in which density shall be enforced.
 * @param N Number of particles.
 */
__kernel void rho(const __global uint* iset,
                  const __global float* bc_rho,
                  __global float *rho,
                  uint bc_iset,
                  uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    rho[i] = bc_rho[i];
}

/** @brief Enforce velocity at the boundary.
 *
 * @param iset Set of particles index.
 * @param bc_u Velocity to be enforced at each cell.
 * @param u Cell velocity, \f$ \mathbf{u} \f$
 * @param bc_iset Particles set in which velocity shall be enforced.
 * @param N Number of particles.
 */
__kernel void u(const __global uint* iset,
                const __global vec* bc_u,
                __global vec *u,
                uint bc_iset,
                uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    u[i] = bc_u[i];
}

/** @brief Enforce velocity normal component at the boundary.
 *
 * @param iset Set of particles index.
 * @param bc_un Velocity normal component to be enforced at each cell
 * @param normal Boundary normal, \f$ \mathbf{n} \f$
 * @param u Cell velocity, \f$ \mathbf{u} \f$
 * @param bc_iset Particles set in which velocity shall be enforced.
 * @param N Number of particles.
 */
__kernel void un(const __global uint* iset,
                 const __global vec* bc_un,
                 const __global vec *normal,
                 __global vec *u,
                 uint bc_iset,
                 uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    vec ut = u[i] - dot(u[i], normal[i]) * normal[i];
    u[i] = ut + bc_un[i];
}

/** @brief Enforce velocity tangent component at the boundary.
 *
 * @param iset Set of particles index.
 * @param bc_ut Velocity tangent component to be enforced at each cell
 * @param normal Boundary normal, \f$ \mathbf{n} \f$
 * @param u Cell velocity, \f$ \mathbf{u} \f$
 * @param bc_iset Particles set in which velocity shall be enforced.
 * @param N Number of particles.
 */
__kernel void ut(const __global uint* iset,
                 const __global vec* bc_ut,
                 const __global vec *normal,
                 __global vec *u,
                 uint bc_iset,
                 uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    vec un = dot(u[i], normal[i]) * normal[i];
    u[i] = un + bc_ut[i];
}

/**
 * @}
 */
