/*
 *  This file is part of AQUAgpusph, a free CFD program based on SPH.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  AQUAgpusph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AQUAgpusph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AQUAgpusph.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @addtogroup basic
 * @{
 */

/** @file
 *  @brief Handles the probability function \f$ f_0 \f$
 */

#include "resources/Scripts/types/types.h"

/** @brief Initialize the probability function, \f$ f_0 \f$, as its value at the
 * equilibirum state.
 * 
 * Using this you can start a simulation providing just the density and velocity
 * at each cell, instead of a value for each probability function
 *
 * @param f0 Particles probability, \f$ f_0 \f$
 * @param f0_eq Particles probability, \f$ f_0 \f$, at the equilibirum state
 * @param N Number of cells.
 */
__kernel void init(__global float* f0,
                   const __global float* f0_eq,
                   uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    f0[i] = f0_eq[i];
}

/** @brief Compute the particles probability, \f$ f_0 \f$, at the equilibirum
 * state.
 *
 * This is a particualr case, since the velocity associated to the probability
 * function is null.
 *
 * @param f0_eq Particles probability, \f$ f_0 \f$, at the equilibirum state
 * @param rho Cell density, \f$ \rho \f$
 * @param u Cell average velocity, \f$ \mathbf{u} \f$
 * @param w0 Weigth of the probability \f$ f_0 \f$ to the equilibrium
 * @param N Number of cells.
 */
__kernel void equilibrium(__global float* f0_eq,
                          const __global float* rho,
                          const __global vec* u,
                          float w0,
                          float cs,
                          uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    float u2 = 0.5f / (cs * cs) * dot(u[i].XYZ, u[i].XYZ);

    f0_eq[i] = w0 * rho[i] * (1.f - u2);
}

/** @brief Sort the probability function, \f$ f_0 \f$, by the cell indexes
 *
 * @param f0_in Unsorted particles probability, \f$ f_0 \f$
 * @param f0 Sorted particles probability, \f$ f_0 \f$
 * @param id_sorted Permutations list from the unsorted space to the sorted
 * one.
 * @param N Number of particles.
 */
__kernel void sort(const __global float *f0_in, __global float *f0,
                   const __global unit *id_sorted,
                   uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    f0[id_sorted[i]] = f0_in[i];
}

/** @brief Find the appropriate neighbours for the probability function,
 * \f$ f_0 \f$.
 * 
 * This is a particualr case, because both neighbours are always the cell itself
 *
 * @param to0 Neighbour cell where the particles associated to \f$ f_0 \f$ are
 * moving to.
 * @param from0 Neighbour cell from which the particles associated to
 * \f$ f_0 \f$ are coming.
 * @param N Number of particles.
 */
__kernel void neighbours(__global uint *to0,
                         __global uint *from0,
                         uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    to0[i] = i;
    from0[i] = i;
}

/** @brief Collide the particles associated to the probability function,
 * \f$ f_0 \f$.
 * 
 * @param f0_eq Particles probability, \f$ f_0 \f$, at the equilibirum state
 * @param f0_in Collided particles probability, \f$ f_0 \f$
 * @param f0 Original particles probability, \f$ f_0 \f$
 * @param omega Relaxation frequency, \f$ \omega = \frac{1}{\tau} \f$
 * @param N Number of particles.
 */
__kernel void collide(__global float *f0_in,
                      const __global float *f0,
                      const __global float* f0_eq,
                      const __global float* omega,
                      uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    f0_in[i] = clamp(f0[i] + omega[i] * (f0_eq[i] - f0[i]), 0.f, 1.f);
}

/** @brief Stream the particles associated to the probability function,
 * \f$ f_0 \f$.
 *
 * This is a special case, because the particles are always streamed to the
 * cell itself
 *
 * @param f0 Collided and Streamed particles probability, \f$ f_0 \f$
 * @param f0_in Collided particles probability, \f$ f_0 \f$
 * @param N Number of particles.
 */
__kernel void stream(__global float *f0,
                     const __global float *f0_in,
                     uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    f0[i] = f0_in[i];
}

/** @brief Add the contribution of the probability function, \f$ f_0 \f$, to
 * the macroscopic density and velocity.
 * 
 * \f$ \rho = \sum_i f_i \f$
 * \f$ \mathbf{u} = \sum_i \mathbf{c}_i f_i \f$
 *
 * This is a special case, since the velocity associated to the probability
 * function is null.
 *
 * @param f0 Collided and Streamed particles probability, \f$ f_0 \f$
 * @param rho Density, \f$ \rho \f$
 * @param u Velocity, \f$ \mathbf{u} \f$
 * @param c0 Particles velocity associated to the probability \f$ f_0 \f$
 * @param N Number of particles.
 */
__kernel void rhou(const __global float *f0,
                   __global float *rho,
                   uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;

    rho[i] += f0[i];
}

/**
 * @}
 */
