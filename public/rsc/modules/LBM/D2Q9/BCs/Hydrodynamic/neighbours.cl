/*
 *  This file is part of AQUAgpusph, a free CFD program based on SPH.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  AQUAgpusph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AQUAgpusph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AQUAgpusph.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @addtogroup basic
 * @{
 */

/** @file
 *  @brief Compute the approrpiate neighbours to can compute gradients of
 * magnitudes.
 */

#include "resources/Scripts/types/types.h"

/** @brief Compute the approrpiate neighbours to can compute gradients of
 * magnitudes.
 *
 * 2-deep neighbours are computed to can carry out 1st and 2nd order Taylor
 * expansions.
 *
 * In order to maximize the compatibility, this job is done in 2 steps. In this
 * step we are just storing the direction index (0 to 8). It is also storing the
 * distance between nodes.
 *
 * @param iset Set of particles index.
 * @param neigh1 Direction index
 * @param bc_dr Distance between cell centers
 * @param normal Boundary normal, \f$ \mathbf{n} \f$
 * @param c1_norm Normalized velocity associated to probability function
 * \f$ f_1 \f$
 * @param c2_norm Normalized velocity associated to probability function
 * \f$ f_2 \f$
 * @param c3_norm Normalized velocity associated to probability function
 * \f$ f_3 \f$
 * @param c4_norm Normalized velocity associated to probability function
 * \f$ f_4 \f$
 * @param bc_iset Particles set in which neighbours shall be computed.
 * @param N Number of particles.
 */
__kernel void direction(const __global uint* iset,
                        __global uint *neigh1,
                        __global float *bc_dr,
                        const __global vec *normal,
                        const vec c1_norm,
                        const vec c2_norm,
                        const vec c3_norm,
                        const vec c4_norm,
                        uint bc_iset,
                        uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    const vec_xyz n = normal[i].XYZ;
    float d = 0.f;
    neigh1[i] = 0;
    bc_dr[i] = 0.f;

    if (dot(n, c1_norm.XYZ) > d) {
        neigh1[i] = 1;
        bc_dr[i] = 1.f;
        d = dot(n, c1_norm.XYZ);
    }
    if (dot(n, c2_norm.XYZ) > d) {
        neigh1[i] = 2;
        bc_dr[i] = 1.f;
        d = dot(n, c2_norm.XYZ);
    }
    if (dot(n, c3_norm.XYZ) > d) {
        neigh1[i] = 3;
        bc_dr[i] = sqrt(2.f);
        d = dot(n, c3_norm.XYZ);
    }
    if (dot(n, c4_norm.XYZ) > d) {
        neigh1[i] = 4;
        bc_dr[i] = sqrt(2.f);
        d = dot(n, c4_norm.XYZ);
    }
    if (dot(n, -c1_norm.XYZ) > d) {
        neigh1[i] = 5;
        bc_dr[i] = 1.f;
        d = dot(n, -c1_norm.XYZ);
    }
    if (dot(n, -c2_norm.XYZ) > d) {
        neigh1[i] = 6;
        bc_dr[i] = 1.f;
        d = dot(n, -c2_norm.XYZ);
    }
    if (dot(n, -c3_norm.XYZ) > d) {
        neigh1[i] = 7;
        bc_dr[i] = sqrt(2.f);
        d = dot(n, -c3_norm.XYZ);
    }
    if (dot(n, -c4_norm.XYZ) > d) {
        neigh1[i] = 8;
        bc_dr[i] = sqrt(2.f);
        d = dot(n, -c4_norm.XYZ);
    }
}

#define NEIGHS_CASE_X(idx)                            \
    case idx:                                         \
        neigh1[i] = from##idx[i].x;                   \
        if(from##idx[i].x < N)                        \
            neigh2[i] = from##idx[from##idx[i].x].x;  \
        else                                          \
            neigh2[i] = N;                            \
        break;

#define NEIGHS_CASE_Y(idy)                            \
    case idy + 4:                                     \
        neigh1[i] = from##idy[i].y;                   \
        if(from##idy[i].y < N)                        \
            neigh2[i] = from##idy[from##idy[i].y].y;  \
        else                                          \
            neigh2[i] = N;                            \
        break;

/** @brief Compute the approrpiate neighbours to can compute gradients of
 * magnitudes.
 *
 * 2-deep neighbours are computed to can carry out 1st and 2nd order Taylor
 * expansions.
 *
 * In order to maximize the compatibility, this job is done in 2 steps. In this
 * step we are getting the neighbours to the direction index (0 to 8).
 *
 * @param iset Set of particles index.
 * @param neigh1 1st neighbour along the normal direction
 * @param neigh2 2nd neighbour along the normal direction
 * @param from1 Neighbour associated to probability function \f$ f_1 \f$
 * @param from2 Neighbour associated to probability function \f$ f_2 \f$
 * @param from3 Neighbour associated to probability function \f$ f_3 \f$
 * @param from4 Neighbour associated to probability function \f$ f_4 \f$
 * @param bc_iset Particles set in which neighbours shall be computed.
 * @param N Number of particles.
 */
__kernel void neighbour(const __global uint* iset,
                        __global uint *neigh1,
                        __global uint *neigh2,
                        const __global uivec2 *from1,
                        const __global uivec2 *from2,
                        const __global uivec2 *from3,
                        const __global uivec2 *from4,
                        uint bc_iset,
                        uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    const uint d = neigh1[i];

    switch (d) {
        case 0:
            neigh1[i] = i;
            neigh2[i] = i;
            break;
        NEIGHS_CASE_X(1);
        NEIGHS_CASE_X(2);
        NEIGHS_CASE_X(3);
        NEIGHS_CASE_X(4);
        NEIGHS_CASE_Y(1);
        NEIGHS_CASE_Y(2);
        NEIGHS_CASE_Y(3);
        NEIGHS_CASE_Y(4);
        default:
            neigh1[i] = N;
            neigh2[i] = N;
            break;
    }
}

/**
 * @}
 */
