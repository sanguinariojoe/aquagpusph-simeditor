/*
 *  This file is part of AQUAgpusph, a free CFD program based on SPH.
 *  Copyright (C) 2012  Jose Luis Cercos Pita <jl.cercos@upm.es>
 *
 *  AQUAgpusph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AQUAgpusph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AQUAgpusph.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @addtogroup basic
 * @{
 */

/** @file
 *  @brief Set the relaxation time of the boundary elements to 1, in order to
 * assert they are permanently at equilibrium
 */

#include "resources/Scripts/types/types.h"

/** @brief Enforce equilibrium at the boundary.
 *
 * To do that the relaxation time is set to 1.
 *
 * @param iset Set of particles index.
 * @param omega Cell relaxation time, \f$ \omega \f$
 * @param bc_iset Particles set in which density shall be enforced.
 * @param N Number of particles.
 */
__kernel void entry(const __global uint* iset,
                    __global float *omega,
                    uint bc_iset,
                    uint N)
{
    uint i = get_global_id(0);
    if(i >= N)
        return;
    if(iset[i] != bc_iset)
        return;

    omega[i] = 1.f;
}

/**
 * @}
 */
