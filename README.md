# AQUAgpusph pipelines generator

[AQUAgpusph](http://canal.etsin.upm.es/aquagpusph/) is a SPH solver featured by
its extensibility, which makes it ideal to implement new formulations, at a
point that even Lattice-Boltzmann Method pipelines have been deployed.

However, the legacy text/xml input files make the pipelines development a bit
cumbersome, so this visual
[web pipeline editor](https://sanguinariojoe.gitlab.io/aquagpusph-simeditor/)
has been conveniently created.

![Pipeline snapshot](https://sanguinariojoe.gitlab.io/aquagpusph-simeditor/imgs/snapshot.png)

This editor can be considered to develop modules, whole pipelines or even
simulation setups.
The editor does not provide any solution to generate the initial condition, i.e.
the initial particles packing to be loaded.

Take a look to the
[wiki](https://gitlab.com/sanguinariojoe/aquagpusph-simeditor/-/wikis/home) to
learn more on the pipelines editor usage.
